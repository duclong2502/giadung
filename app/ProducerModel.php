<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 10:19 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProducerModel extends Model
{
    protected $table = 'producer';
    protected $fillable = ['name', 'status', 'description', 'image', 'category_id', 'slug'];
}