<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateModel extends Model
{
    protected $table = 'rate';
    protected $fillable = ['fullname', 'email', 'star', 'comment','product_id'];
}
