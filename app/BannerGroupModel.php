<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerGroupModel extends Model
{
    protected $table = 'banner_group';
    protected $fillable = ['key','name','status'];
}
