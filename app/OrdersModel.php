<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersModel extends Model
{
    protected $table = 'orders';
    protected $fillable = ['customer_name', 'email', 'phone','address', 'message', 'payment_method','status'];
}
