<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailModel extends Model
{
    protected $table = 'orders_detail';
    protected $fillable = ['product_name', 'image', 'order_id', 'product_id', 'amount', 'price', 'status'];
}
