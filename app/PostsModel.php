<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 9:24 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsModel extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title', 'slug', 'description', 'image', 'content', 'status', 'category_id', 'title_seo', 'description_seo', 'primary_key_seo', 'sub_key_seo', 'link_seo'];
}