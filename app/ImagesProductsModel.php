<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagesProductsModel extends Model
{
    protected $table = "image_product";
    protected $fillable = ['product_id', 'image'];

    public function products(){
        return $this->belongsTo(ProductsModel::class,'product_id','id');
    }
}
