<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'slug', 'description', 'image',
        'thong_so', 'bao_hanh', 'thong_tin', 'price', 'price_sale',
        'category_id', 'producer_id', 'status', 'title_seo', 'description_seo', 'primary_key_seo', 'sub_key_seo', 'link_seo'
    ];

    public function category(){
        return $this->belongsTo(CategoryTermModel::class,'category_id','id');
    }
}
