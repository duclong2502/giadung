<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected $table = 'menu';
    protected $fillable = ['name', 'link', 'type', 'parent_id', 'status', 'menu_group_id'];

    public function childs()
    {
        return $this->hasMany('App\MenuModel', 'parent_id', 'id');
    }

    public function getLinkByID(){
          return $this->belongsTo(CategoryTermModel::class, 'link', 'id');
    }

    public function getSlugByID(){
        return $this->belongsTo(PostsModel::class, 'link', 'id');
    }
}
