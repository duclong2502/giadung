<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model
{
    protected $table = 'cart';
    protected $fillable = ['product_id', 'name', 'price', 'total', 'cookies', 'image', 'status'];
}
