<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/1/2018
 * Time: 11:01 AM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;


class ConfigModel extends Model
{
    protected $table = 'config';
    protected $fillable = ['key', 'content', 'status'];
}