<?php

namespace App\Http\Requests;

use App\PostsModel;
use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'title' => 'required|max:255',
            'description' => 'required',
        ];
        if ($this->is("admin/post/add")) {
            $rule['image'] = 'image|mimes:jpg,png,jpeg';
        }
//        if ($this->is("admin/ads/update/*")){
//            $rule['slug'] = Rule::unique('ads', 'slug')->ignore($this->id);
//        }

        return $rule;
    }
    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề bài viết không được để trống',
            'title.max' => 'Tiêu đề bài viết không được dài quá 255 ký tự',
            'description.required' => 'Mô tả bài viết không được để trống',
        ];
    }
    public function save($id = null){
        $post = PostsModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/post'), $imageName);
            if ($post->image1 != null){
                unlink(public_path($post->image1));
            }
            $this->merge(['image' => '/images/post/' . $imageName]);
        }

        $post->fill($this->all())->save();
        return $post;
    }
}
