<?php

namespace App\Http\Requests;

use App\BannerModel;
use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
            'banner_group_id' => 'required',
        ];
        if ($this->is("admin/producer/add")) {
            $rule['image'] = 'required|image|mimes:jpg,png,jpeg';
        }

        return $rule;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên banner không được để trống',
            'name.max' => 'Tên banner không được dài quá 255 ký tự',
            'banner_group_id.required' => 'Loại banner không được để trống',
            'image1.required' => 'Ảnh banner không được để trống',
        ];
    }

    public function save($id = null)
    {
        $banner = BannerModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/banner'), $imageName);
            if ($banner->image1 != null) {
                unlink(public_path($banner->image1));
            }
            $this->merge(['image' => '/images/banner/' . $imageName]);
//            $category->image = '/images/category/' . $imageName;
        }

        $banner->fill($this->all())->save();
        return $banner;
    }
}
