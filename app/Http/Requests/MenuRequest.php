<?php

namespace App\Http\Requests;

use App\MenuModel;
use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required',
//            'link' => 'required',
            'menu_group_id' => 'required',
            'type' => 'required',
            'parent_id' => 'required',
        ];

        return $rule;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
//            'link.required' => 'Đường dẫn không được để trống',
            'menu_group_id.required' => 'Loại menu không được để trống',
            'parent_id.required' => 'Danh mục cha không được để trống',
        ];
    }

    public function save($id = null)
    {
        $menu = MenuModel::findOrNew($id);
        $menu->fill($this->all())->save();
        return $menu;
    }
}
