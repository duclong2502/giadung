<?php

namespace App\Http\Requests;

use App\WarrantyModel;
use Illuminate\Foundation\Http\FormRequest;

class WarrantyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
//            'link1' => 'required',
        ];
        if ($this->is("admin/warranty/add")) {
            $rule['image'] = 'image|mimes:jpg,png,jpeg';
        }

        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề bài viết không được để trống',
            'name.max' => 'Tiêu đề bài viết không được dài quá 255 ký tự',
//            'link1.required' => 'Đường dẫn không được để trống',
        ];
    }
    public function save($id = null){
        $warranty = WarrantyModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/warranty'), $imageName);
            if ($warranty->image1 != null){
                unlink(public_path($warranty->image1));
            }
            $this->merge(['image' => '/images/warranty/' . $imageName]);
        }
        if ($this->hasFile('link1')) {
            $pdf = time() . "." . $this->link1->getClientOriginalName();
            $this->link1->move(public_path('doc/warranty'), $pdf);
            if ($warranty->link != null){
                unlink(public_path($warranty->link));
            }
            $this->merge(['link' => '/doc/warranty/' . $pdf]);
        }
//        dd($this->link);
        $warranty->fill($this->all())->save();
        return $warranty;
    }
}
