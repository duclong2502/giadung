<?php

namespace App\Http\Requests;

use App\ConfigModel;
use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'content' => 'required',
        ];

        if ($this->is("user/config/edit/*")){
            $rule['key'] = Rule::unique('config', 'key')->ignore($this->id);
        }

        return $rule;
    }
    public function messages()
    {
        return [
            'content.required' => 'Tiêu đề bài viết không được để trống',
        ];
    }
    public function save($id = null){
        $config = ConfigModel::findOrNew($id);

        $config->fill($this->all())->save();
        return $config;
    }
}
