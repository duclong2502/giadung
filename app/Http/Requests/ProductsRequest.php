<?php

namespace App\Http\Requests;

use App\ImagesProductsModel;
use App\ProductsModel;
use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
            'description' => 'required',
//            'price' => 'required',
//            'price_sale' => 'required',
            'category_id' => 'required',
            'producer_id' => 'required',
        ];
        if ($this->is("admin/producer/add")) {
            $rule['image'] = 'image|mimes:jpg,png,jpeg';
        }
//        if ($this->is("admin/ads/update/*")){
//            $rule['slug'] = Rule::unique('ads', 'slug')->ignore($this->id);
//        }

        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên nhà sản xuất không được để trống',
            'name.max' => 'Tên nhà sản xuất không được dài quá 255 ký tự',
            'description.required' => 'Mô tả bài viết không được để trống',
//            'price.required' => 'Giá gốc không được để trống',
//            'price_sale.required' => 'Giá sale không được để trống',
            'category_id.required' => 'Danh mục không được để trống',
            'producer_id.required' => 'Hãng sản xuất không được để trống',
        ];
    }
    public function save($id = null){
        $products = ProductsModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/product'), $imageName);
            if ($products->image1 != null){
                unlink(public_path($products->image1));
            }
            $this->merge(['image' => '/images/product/' . $imageName]);
        }
        $this->merge(['price' => (int)str_replace(',', '', $this->get('price'))]);
        $this->merge(['price_sale' => (int)str_replace(',', '', $this->get('price_sale'))]);

        $products->fill($this->all())->save();
        $lastInsertId = $products->id;
//        dd(123);
        if ($this->has('nameImageSub')) {
//            dd($this->get('nameImageSub'));
            foreach ($this->get('nameImageSub') as $vd){
//                dd($vd);
//                $product_img = ImagesProductsModel::findOrNew($id);
                $product_img = new ImagesProductsModel();
                $urlImg = 'upload/products/subs/' . $vd;
                $product_img->product_id = $lastInsertId;
                $product_img->image = $urlImg;
                $product_img->save();
            }
        }
        return $products;
    }
}
