<?php

namespace App\Http\Requests;

use App\RateModel;
use Illuminate\Foundation\Http\FormRequest;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'fullname' => 'required|max:255',
            'email' => 'required',
        ];
        return $rule;
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Họ tên không được để trống',
            'fullname.max' => 'Họ tên bài viết không được dài quá 255 ký tự',
            'email.required' => 'Email không được để trống',
        ];
    }

    public function save($id)
    {

    }
}
