<?php

namespace App\Http\Requests;

use App\CategoryTermModel;
use Illuminate\Foundation\Http\FormRequest;

class CategoryTermRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
            'link' => 'required',
            'menu_group_id' => 'required',
            'description' => 'required',
        ];

        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên nhà danh mục không được để trống',
            'name.max' => 'Tên nhà danh mục không được dài quá 255 ký tự',
            'link.required' => 'Đường dẫn không được để trống',
            'menu_group_id.required' => 'Loại menu không được để trống',
            'description.required' => 'Mô tả không được để trống',
        ];
    }
    public function save($id = null){
        $category = CategoryTermModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/category'), $imageName);
            if ($category->image1 != null){
                unlink(public_path($category->image1));
            }
//            $this->merge(['image' => '/images/category/' . $imageName]);
            $category->image = '/images/category/' . $imageName;
        }

        $category->type = 0;
        $category->name = $this->get('name');
        $category->link = $this->get('link');
        $category->parent_id = $this->get('parent_id');
        $category->status = $this->get('status');
        $category->menu_group_id = $this->get('menu_group_id');
        $category->description = $this->get('description');
        if($this->get('parent_id') == 0){
            $category->level = 1;
        }else{
            $category->level = 2;
        }
        $category->save();
        return $category;
    }
}
