<?php

namespace App\Http\Requests;

use App\ProducerModel;
use Illuminate\Foundation\Http\FormRequest;

class ProducerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
            'description' => 'required',
        ];
        if ($this->is("admin/producer/add")) {
            $rule['image'] = 'image|mimes:jpg,png,jpeg';
        }
//        if ($this->is("admin/ads/update/*")){
//            $rule['slug'] = Rule::unique('ads', 'slug')->ignore($this->id);
//        }

        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên nhà sản xuất không được để trống',
            'name.max' => 'Tên nhà sản xuất không được dài quá 255 ký tự',
            'description.required' => 'Mô tả bài viết không được để trống',
        ];
    }
    public function save($id = null){
        $producer = ProducerModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/producer'), $imageName);
            if ($producer->image1 != null){
                unlink(public_path($producer->image1));
            }
            $this->merge(['image' => '/images/producer/' . $imageName]);
        }

        $producer->fill($this->all())->save();
        return $producer;
    }
}
