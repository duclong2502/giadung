<?php

namespace App\Http\Requests;

use App\SlidersModel;
use Illuminate\Foundation\Http\FormRequest;

class SlidersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|max:255',
            'link' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ];
        if ($this->is("admin/slider/add")) {
            $rule['image'] = 'image|mimes:jpg,png,jpeg';
        }
//        if ($this->is("admin/ads/update/*")){
//            $rule['slug'] = Rule::unique('ads', 'slug')->ignore($this->id);
//        }

        return $rule;
    }
    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề bài viết không được để trống',
            'name.max' => 'Tiêu đề bài viết không được dài quá 255 ký tự',
            'link.required' => 'Đường dẫn không được để trống',
            'link.regex' => 'Đường dẫn không đúng định dạng',
        ];
    }
    public function save($id = null){
        $slider = SlidersModel::findOrNew($id);
        if ($this->hasFile('image1')) {
            $imageName = time() . "." . $this->image1->getClientOriginalName();
            $this->image1->move(public_path('images/slider'), $imageName);
            if ($slider->image1 != null){
                unlink(public_path($slider->image1));
            }
            $this->merge(['image' => '/images/slider/' . $imageName]);
        }

        $slider->fill($this->all())->save();
        return $slider;
    }
}
