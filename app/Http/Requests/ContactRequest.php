<?php

namespace App\Http\Requests;

use App\ContactModel;
use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'customer_name' => 'required|max:255',
            'email' => 'required',
        ];

        return $rule;
    }
    public function messages()
    {
        return [
            'customer_name.required' => 'Họ tên không được để trống',
            'customer_name.max' => 'Họ tên không được dài quá 255 ký tự',
            'email.required' => 'Email không được để trống',
        ];
    }
    public function save($id = null){
//        dd(123);
        $contact = ContactModel::findOrNew($id);

        $contact->fill($this->all())->save();
        return $contact;
    }
}
