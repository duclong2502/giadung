<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->level == 99) {
            return $next($request);
        } else {
            return redirect(route('dashboard'))->withErrors('Bạn không đủ quyền truy cập');
//            return redirect(route('dashboard'))->with('error','Bạn không đủ quyền truy cập');
        }
    }
}
