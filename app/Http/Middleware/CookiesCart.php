<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class CookiesCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->makeMyCookie();
        return $next($request);
    }

    protected function makeMyCookie()
    {
        if (!$this->hasCookie("a_p")) {
            $value = time() . rand(1, 10);
            return Cookie::queue(Cookie::make('a_p', $value, 999999));
        }
        return false;
    }

    protected function hasCookie($cookie_name)
    {
        $cookie_exist = Cookie::get($cookie_name);
        return ($cookie_exist) ? true : false;
    }
}
