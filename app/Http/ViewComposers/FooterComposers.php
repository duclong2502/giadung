<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/11/2018
 * Time: 10:14 AM
 */

namespace App\Http\ViewComposers;

use App\BannerModel;
use App\CategoryTermModel;
use App\ConfigModel;
use App\ProductsModel;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Session;

class FooterComposers
{
    protected $fanpage_facebook = "";
    protected $footer = "";
    protected $categories_footer = [];

    protected $product_new = [];
    protected $product_hot = [];
    protected $category = [];

    public function __construct(Request $request)
    {
        $this->fanpage_facebook = ConfigModel::select('content')->where('key', 'fanpage_facebook')->first();
        $this->footer = ConfigModel::select('content')->where('key', 'footer')->first();
        $this->categories_footer = CategoryTermModel::where('parent_id', '=', 0)->where('menu_group_id', 2)->get();

        $this->product_new = ProductsModel::where('status', 1)->orderBy('id', 'DESC')->take(20)->get();
        $this->product_hot = ProductsModel::where('status', 1)->orderByRaw('price - price_sale DESC')->take(20)->get();
        $this->category = CategoryTermModel::where('status', 1)->where('parent_id', '<>',0)->take(5)->get();

    }

    public function compose(View $view)
    {
        $view->with('fanpage_facebook', $this->fanpage_facebook->content);
        $view->with('footer', $this->footer->content);
        $view->with('categories_footer', $this->categories_footer);
        $view->with('product_new', $this->product_new);
        $view->with('product_hot', $this->product_hot);
        $view->with('category', $this->category);
    }
}