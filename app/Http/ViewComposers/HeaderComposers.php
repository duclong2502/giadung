<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/10/2018
 * Time: 2:19 PM
 */

namespace App\Http\ViewComposers;


use App\BannerModel;
use App\CartModel;
use App\CategoryTermModel;
use App\ConfigModel;
use App\MenuModel;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Session;

class HeaderComposers
{
    protected $carts = [];
    protected $categories = [];
    protected $categories_sidebar = [];
    protected $allCategories = [];
    protected $cookie = "";
    protected $sub_money = 0;
    protected $facebook = "";
    protected $youtube = "";
    protected $google_plus = "";
    protected $ads = "";
    protected $ads_footer = "";

    public function __construct(Request $request)
    {
        $this->cookie = $request->cookie('a_p');
        $this->carts = CartModel::where('cookies', $this->cookie)->get();
        $this->categories = MenuModel::where('menu_group_id', 1)->where('parent_id', '=', 0)->get();
        $this->categories_sidebar = CategoryTermModel::where('menu_group_id', 1)->where('parent_id', '=', 0)->get();
        $this->allCategories = CategoryTermModel::pluck('name', 'id')->all();
        $this->youtube = ConfigModel::where('key', 'youtube')->first();
        $this->facebook = ConfigModel::where('key', 'facebook')->first();
        $this->google_plus = ConfigModel::where('key', 'google_plus')->first();

        $this->ads = BannerModel::where('banner_group_id',2)->orderBy('id','DESC')->take(3)->get();
        $this->ads_footer = BannerModel::where('banner_group_id',1)->orderBy('id','DESC')->take(3)->get();


        foreach ($this->carts as $value) {
            $tt = $value->price * $value->total;
            $this->sub_money = $this->sub_money + $tt;
        }
    }

    public function compose(View $view)
    {
        $view->with('carts', $this->carts);
        $view->with('sub_money', $this->sub_money);
        $view->with('categories', $this->categories);
        $view->with('categories_sidebar', $this->categories_sidebar);
        $view->with('allCategories', $this->allCategories);
        $view->with('facebook', $this->facebook->content);
        $view->with('youtube', $this->youtube->content);
        $view->with('google_plus', $this->google_plus->content);
        $view->with('ads', $this->ads);
        $view->with('ads_footer', $this->ads_footer);

    }
}