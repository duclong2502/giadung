<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 09/04/2018
 * Time: 10:57 AM
 */

namespace App\Http\Controllers\Cms;

use App\ContactModel;
use App\Http\Controllers\Controller;
use App\OrdersModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
//    function __construct(Request $request)
//    {
//        $this->request = $request;
//    }

    function dashboard()
    {
//       dd(Auth::user());
        $contact  = ContactModel::all();
        $products  = ContactModel::all();
        $order = OrdersModel::all();
        $new_order = OrdersModel::where('status',0)->orderBy('id','DESC');
//        $level = User::all()->value('level');
        $response = [
            "title" => "DASHBOARD",
            "title_description" => "",
            "contact" => $contact,
            "products" => $products,
            "order" => $order,
            "new_order" => $new_order->paginate(10),
//            "level" => $level
        ];
//        dd(Auth::user()->level);
        return view('admin.dashboard',$response);
    }
}