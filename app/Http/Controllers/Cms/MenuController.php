<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/21/2018
 * Time: 8:57 AM
 */

namespace App\Http\Controllers\Cms;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use App\MenuGroup;
use App\MenuModel;
use App\PostsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->name = ' quản lý menu';
    }

    public function index()
    {
        $menu = MenuModel::orderBy('id', 'DESC');
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "menu" => $menu->paginate(10),

        ];
        return view('admin.menu.index', $response);
    }

    public function add()
    {
        $menu_group = MenuGroup::all();
        $menu = MenuModel::where('parent_id', 0)->where('status', 1)->get();
        $post = PostsModel::where('status', 1)->get();
        $category = CategoryTermModel::where('status', 1)->get();
        $response = [
            "title" => "Thêm mới $this->name",
            "title_description" => "",
            "menu_group" => $menu_group,
            "menu" => $menu,
            "post" => $post,
            "category" => $category,
        ];
        return view('admin.menu.add', $response);
    }

    public function doAdd(MenuRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id)
    {
        $item = MenuModel::where('id', $id)->first();
        $menu_group = MenuGroup::all();
        $menu = MenuModel::where('parent_id', 0)->where('status', 1)->where('id', '<>', $id)->get();
        $post = PostsModel::where('status', 1)->get();
        $category = CategoryTermModel::where('status', 1)->get();

        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
            "menu_group" => $menu_group,
            "menu" => $menu,
            "item" => $item,
            "post" => $post,
            "category" => $category,
        ];
        return view('admin.menu.edit', $response);
    }

    public function doEdit(MenuRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

    public function delete($id)
    {
        $item = MenuModel::find($id);
        if (!$item) abort(404);

        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}