<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 9:16 PM
 */

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRequest;
use App\PostsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    function __construct()
    {
        $this->name = " bài viết";
    }

    public function index(){
        $posts = PostsModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "posts" => $posts->paginate(10),

        ];
        return view('admin.posts.index', $response);
    }

    public function add(){
        $response = [
            "title" => "Thêm $this->name",
            "title_description" => "",

        ];
        return view('admin.posts.add', $response);
    }

    public function doAdd(PostsRequest $request){
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id){
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
        ];
        $post = PostsModel::where("id", $id)->first();
        if (!$post) about(404);
        $response["post"] = $post;
        return view('admin.posts.edit', $response);
    }

    public  function doEdit(PostsRequest $request,$id){
        $request->save($id);
        return Redirect::back()->withSuccess('Sửa thành công');
    }

    public function delete($id){
        $post = PostsModel::find($id);
        if (!$post) abort(404);

        $post->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }

}