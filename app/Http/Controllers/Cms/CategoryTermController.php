<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/9/2018
 * Time: 3:54 PM
 */

namespace App\Http\Controllers\Cms;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryTermRequest;
use App\MenuGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CategoryTermController extends Controller
{
    function __construct()
    {
        $this->name = " danh mục sản phẩm";
    }

    public function index()
    {
        $category_term = CategoryTermModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "category_term" => $category_term->paginate(10),

        ];
        return view('admin.category.index', $response);
    }

    public function add()
    {
        $menu_group = MenuGroup::all();
        $category_term = CategoryTermModel::where('level',1)->get();
        $response = [
            "title" => "Thêm mới $this->name",
            "title_description" => "",
            "menu_group" => $menu_group,
            "category_term" => $category_term,
        ];
        return view('admin.category.add', $response);

    }

    public function doAdd(CategoryTermRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id)
    {
        $item = CategoryTermModel::where('id', $id)->first();
        $menu_group = MenuGroup::all();
        $category_term =  CategoryTermModel::where('level',1)->get();
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
            "menu_group" => $menu_group,
            "category_term" => $category_term,
            "item" => $item,
        ];
        return view('admin.category.edit', $response);
    }

    public function doEdit(CategoryTermRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

    public function delete($id){
        $item = CategoryTermModel::find($id);
        if (!$item) abort(404);

        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}