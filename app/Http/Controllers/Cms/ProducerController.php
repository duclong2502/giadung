<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 10:10 PM
 */

namespace App\Http\Controllers\Cms;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProducerRequest;
use App\ProducerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProducerController extends Controller
{

    public function __construct()
    {
        $this->name = ' nhà sản xuất';
    }

    public  function index(){
        $producers = ProducerModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "producers" => $producers->paginate(10),

        ];
        return view('admin.producer.index', $response);
    }

    public function add(){
        $category = CategoryTermModel::where('level',1)->get();
        $response = [
            "title" => "Thêm $this->name",
            "title_description" => "",
            "category" => $category,

        ];
        return view('admin.producer.add', $response);

    }

    public  function doAdd(ProducerRequest $request){
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id){
        $category = CategoryTermModel::where('level',1)->get();
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
            "category" => $category,
        ];
        $producer = ProducerModel::where("id", $id)->first();
        if (!$producer) about(404);
        $response["producer"] = $producer;
        return view('admin.producer.edit', $response);
    }

    public function doEdit(ProducerRequest $request,$id){
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

    public function delete($id){
        $item = ProducerModel::find($id);
        if (!$item) abort(404);

        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }

}