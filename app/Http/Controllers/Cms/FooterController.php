<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/11/2018
 * Time: 10:35 AM
 */

namespace App\Http\Controllers\Cms;

use App\ConfigModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class FooterController extends Controller
{
    function __construct(Request $request)
    {
        $this->request = $request;
        $this->name = " footer";
    }

    function index()
    {
        $footer = ConfigModel::where('key','footer')->get();
        $response = [
            "title" => "Nội dung $this->name",
            "title_description" => "",
            "footer" => $footer,

        ];
        return view('admin.footer.index', $response);
    }

    function edit($id)
    {
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
        ];
        $footer = ConfigModel::where('id',$id)->first();
        if (!$footer) about(404);
        $response["footer"] = $footer;
        return view('admin.footer.edit', $response);

    }
    public function doEdit(ConfigRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

}