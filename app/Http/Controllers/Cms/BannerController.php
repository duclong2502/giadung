<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/18/2018
 * Time: 10:11 AM
 */

namespace App\Http\Controllers\Cms;


use App\BannerGroupModel;
use App\BannerModel;
use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\CategoryTermRequest;
use App\MenuGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class BannerController extends Controller
{
    function __construct()
    {
        $this->name = " banner quảng cáo";
    }

    public function index()
    {
        $banners = BannerModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "banners" => $banners->paginate(10),

        ];
        return view('admin.banner.index', $response);
    }

    public function add()
    {
        $banner_group = BannerGroupModel::all();
        $response = [
            "title" => "Thêm mới $this->name",
            "title_description" => "",
            "banner_group" => $banner_group,
        ];
        return view('admin.banner.add', $response);
    }

    public function doAdd(BannerRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id)
    {
        $banner_group = BannerGroupModel::all();
        $banner = BannerModel::where('id', $id)->first();
        $response = [
            "title" => "Thêm mới $this->name",
            "title_description" => "",
            "banner_group" => $banner_group,
            "banner" => $banner,
        ];
        return view('admin.banner.edit', $response);
    }

    public function doEdit(BannerRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

    public function delete($id)
    {
        $item = BannerModel::find($id);
        if (!$item) abort(404);

        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}