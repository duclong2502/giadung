<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/3/2018
 * Time: 4:51 PM
 */

namespace App\Http\Controllers\Cms;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsRequest;
use App\ImagesProductsModel;
use App\ProducerModel;
use App\ProductsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{

    public function __construct()
    {
        $this->name = ' sản phẩm';
    }

    public function index()
    {
        $products = ProductsModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "products" => $products->paginate(10),
        ];
        return view('admin.products.index', $response);
    }

    public function add()
    {
        $producer = ProducerModel::all();
        $category = CategoryTermModel::all();
        $response = [
            "title" => "Thêm mới $this->name",
            "title_description" => "",
            "producer" => $producer,
            "category" => $category,
        ];
        return view('admin.products.add', $response);
    }

    public function doAdd(ProductsRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");

    }

    public function edit($id)
    {
        $producer = ProducerModel::all();
        $category = CategoryTermModel::all();
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
            "producer" => $producer,
            "category" => $category,
        ];
        $product = ProductsModel::where("id", $id)->first();
        if (!$product) about(404);
        $response["product"] = $product;
        return view('admin.products.edit', $response);

    }

    public function doEdit(ProductsRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }

    public function delete($id)
    {
        $product = ProductsModel::find($id);
//        dd($product);
        if (!$product) abort(404);
        unlink(public_path($product->image));
        $product->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }


    //THEM ANH CHI TIET
    public function imageDetail($id){
        $list = ImagesProductsModel::where('product_id', $id)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách ảnh chi tiết",
            "title_description" => "",
            "list" => $list->paginate(10),
        ];
        return view('admin.products.image_detail', $response);

    }

    public function postImages(Request $request)
    {
        $arr = [];
        if ($request->ajax()) {
            if ($request->hasFile('nameImageSub')) {
                $imageFiles = $request->file('nameImageSub');
                foreach ($imageFiles as $item) {
                    if ($item->isValid()) {
                        $getimageName = time() . '.' . $item->getClientOriginalName();
                        $item->move(public_path('/upload/products/subs'), $getimageName);
                        array_push($arr, $getimageName);
                    }
                }
            }
        }
        return response()->json($arr);

    }

    public function postDeleteImage($value)
    {
        unlink(public_path('upload/products/subs/' . $value));
    }

    public function getDeleteSub($id)
    {
        $imgSub = ImagesProductsModel::find($id);
        unlink(public_path($imgSub->image));
        $imgSub->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}