<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/1/2018
 * Time: 2:27 PM
 */

namespace App\Http\Controllers\Cms;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;

class AdminController extends Controller
{
    function __construct()
    {
//        $this->request = $request;
        $this->name = " tài khoản";
    }

    public function index()
    {
        $admin = User::whereNotIn('level', [99])->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "admin" => $admin->paginate(10),

        ];
        return view('admin.user.index', $response);
    }

    public function add()
    {
        $response = [
            "title" => "Thêm $this->name",
            "title_description" => "",
        ];
        return view('admin.user.add', $response);
    }

    public function doAdd(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "username" => "required|unique:users",
            "email" => "required|unique:users",
            'password' => 'required|min:6|confirmed',
            "password_confirmation" => "required|min:6",
        ], [
            "name.required" => 'Chưa nhập tên',
            "username.required" => 'Chưa nhập tên tài khoản',
            "username.unique" => "Username đã tồn tại trong hệ thống",
            "email.required" => 'Chưa nhập email',
            "email.unique" => 'Email đã tồn tại trong hệ thống',
            "password.required" => 'Chưa nhập mật khẩu',
            "password_confirmation.required" => 'Chưa nhập mật khẩu',
            "password.min" => 'Mật khẩu phải lớn hơn 6 ký tự',
            "password_confirmation.min" => 'Mật khẩu phải lớn hơn 6 ký tự',
            'confirmed' => 'Mật khẩu không khớp'
        ]);

        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'username' => $request->get('username'),
            'level' => 0,
            'password' => bcrypt($request->get('password')),
        ]);


        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

//    public function edit($id){
//        $response = [
//            "title" => "Sửa $this->name",
//            "title_description" => "",
//        ];
//        $admin = User::where("id", $id)->first();
//        if (!$admin) about(404);
//        $response["admin"] = $admin;
//        return view('admin.user.edit', $response);
//    }
//
//    public function doEdit($id){
//
//    }


    function delete($id)
    {
        $admin = User::find($id);
        if (!$admin) abort(404);

        $admin->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}