<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/6/2018
 * Time: 9:54 AM
 */

namespace App\Http\Controllers\Cms;

use App\ContactModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    function __construct()
    {
        $this->name = " đăng ký liên hệ";
    }

    function index()
    {
        $contact = ContactModel::orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "contact" => $contact->paginate(10),

        ];
        return view('admin.contact.index', $response);
    }
    function delete($id){
        $item = ContactModel::find($id);
        if (!$item) abort(404);
        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}