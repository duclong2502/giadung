<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 7/31/2018
 * Time: 2:46 PM
 */

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function getLogin()
    {
        if (Auth::check()) {
            return Redirect::to("/dashboard");
        } else {
            return view('admin.login');
        }
    }

    public function postLogin(Request $request)
    {
        $data = [
            'username' => $request->username,
            'password' => $request->password,
        ];
        $u = $request -> username;
        $p = $request -> password;
        $user = User::where('username',$u)->where('password', $p)->first();
        if ($user != null) {
            Auth::login($user);
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('loginAdmin')->withErrors('Sai tài khoản hoặc mật khẩu');
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to("/admin");
//        return view('user.login');
//        return redirect(route('loginAdmin'))->withErrors('Đăng nhập để tiếp tục');
    }
}