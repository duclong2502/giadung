<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 8:58 AM
 */

namespace App\Http\Controllers\Cms;

use App\Http\Requests\SlidersRequest;
use App\SlidersModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SlidersController extends Controller
{
    function __construct()
    {
        $this->name = " slider";
    }

    public function index()
    {
        $sliders = SlidersModel::where('status', 1)->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "sliders" => $sliders->paginate(10),

        ];
        return view('admin.sliders.index', $response);
    }

    public function add()
    {
        $response = [
            "title" => "Thêm $this->name",
            "title_description" => "",

        ];
        return view('admin.sliders.add', $response);
    }

    public function doAdd(SlidersRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }


    function edit($id)
    {
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
        ];
        $slider = SlidersModel::where("id", $id)->first();
        if (!$slider) about(404);
        $response["slider"] = $slider;
        return view('admin.sliders.edit', $response);

    }
    public function doEdit(SlidersRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }




    function delete($id)
    {
        $sliders = SlidersModel::find($id);
        if (!$sliders) abort(404);

        $sliders->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}