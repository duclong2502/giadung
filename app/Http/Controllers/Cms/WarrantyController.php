<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/10/2018
 * Time: 4:23 PM
 */

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\WarrantyRequest;
use App\WarrantyModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class WarrantyController extends Controller
{
    function __construct()
    {
        $this->name = " chế độ bảo hành";
    }

    public function index()
    {
        $warranty = WarrantyModel::orderBy('id', 'DESC');
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "warranty" => $warranty->paginate(10),

        ];
        return view('admin.warranty.index', $response);
    }

    public function add()
    {
        $response = [
            "title" => "Thêm $this->name",
            "title_description" => "",

        ];
        return view('admin.warranty.add', $response);
    }

    public function doAdd(WarrantyRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Thêm mới thành công");
    }

    public function edit($id)
    {
        $item = WarrantyModel::where('id', $id)->first();
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
            "item" => $item

        ];
        return view('admin.warranty.edit', $response);
    }

    public function doEdit(WarrantyRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }
    function delete($id)
    {
        $item = WarrantyModel::find($id);
        if (!$item) abort(404);
        unlink(public_path($item->image));
        unlink(public_path($item->link));
        $item->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }
}