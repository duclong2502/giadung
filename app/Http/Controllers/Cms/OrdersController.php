<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/6/2018
 * Time: 10:11 AM
 */

namespace App\Http\Controllers\Cms;

use App\OrderDetailModel;
use App\OrdersModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{

    function __construct()
    {
        $this->name = " đơn hàng";
    }

    function index(Request $request)
    {
        $orders = OrdersModel::orderBy('id', 'desc');
        if($request->has('name')){
            $orders = $orders->where('customer_name', "like", "%" . $request->get('name') . "%");
        }
        if($request->has('phone')){
            $orders = $orders->where('phone', "like", "%" . $request->get('phone') . "%");
        }

        if($request->has('start_date')){
            $date = str_replace('/', '-', $request->input("start_date"));
            $datepicker = date('Y-m-d', strtotime($date));
            $orders = $orders->where("created_at", ">=", $datepicker);
        }
        if ($request->has("end_date")) {
            $date = str_replace('/', '-', $request->input("end_date"));
            $datepicker = date('Y-m-d', strtotime($date));
            $orders = $orders->where("created_at", "<=", $datepicker);
        }


        $request->flash();
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "orders" => $orders->paginate(10),

        ];
        return view('admin.orders.index', $response);
    }

    function detail($id)
    {
        $detail = OrderDetailModel::where('order_id', $id)->orderBy('id', 'desc');
        $response = [
            "title" => "Chi tiết đơn hàng",
            "title_description" => "",
            "detail" => $detail->paginate(10),

        ];
        return view('admin.orders.detail', $response);
    }

    function updateStatus(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('optradio');
        $order = OrdersModel::where('id', $id)->first();

        $now = date("Y-m-d H:i:s");
        if ($status) {
            $order->status = $status;
            $order->updated_at = $now;
            $order->save();
            return Redirect::back()->withSuccess("Thay đổi thành công");
        }
        return Redirect::back();

    }

    function delete($id)
    {
        $order = OrdersModel::find($id);
        $order_detail = OrderDetailModel::where('order_id', $id)->get();
        foreach ($order_detail as $item){
            $item->delete();
        }

        if (!$order) abort(404);

        $order->delete();
        return Redirect::back()->withSuccess("Xóa thành công");
    }

}