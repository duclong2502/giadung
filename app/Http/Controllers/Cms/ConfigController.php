<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/1/2018
 * Time: 10:54 AM
 */

namespace App\Http\Controllers\Cms;

use App\ConfigModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ConfigController extends Controller
{
    function __construct(Request $request)
    {
        $this->request = $request;
        $this->name = " config hệ thống";
    }

    function index()
    {
        $config = ConfigModel::where('key', '<>', 'footer')->orderBy("id", "desc");
        $response = [
            "title" => "Danh sách $this->name",
            "title_description" => "",
            "config" => $config->paginate(10),

        ];
        return view('admin.config.index', $response);
    }

    function edit($id)
    {
        $response = [
            "title" => "Sửa $this->name",
            "title_description" => "",
        ];
        $config = ConfigModel::where("id", $id)->first();
        if (!$config) about(404);
        $response["config"] = $config;
        return view('admin.config.edit', $response);

    }

    public function doEdit(ConfigRequest $request, $id)
    {
        $request->save($id);
        return Redirect::back()->withSuccess("Sửa thành công");
    }
}