<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/7/2018
 * Time: 4:02 PM
 */

namespace App\Http\Controllers\FontEnd_Controller;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\ImagesProductsModel;
use App\PostsModel;
use App\ProducerModel;
use App\ProductsModel;
use App\RateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function getProductCategory(Request $request, $slug, $hang = null)
    {

        if ($slug) {
            $category_child = CategoryTermModel::where('link', $slug)->first();
            $products = ProductsModel::where('category_id', $category_child->id);
        }

        if ($hang) {
            $producer_child = ProducerModel::where('slug', $hang)->first();
            $products = $products->where('producer_id', (int)$producer_child->id);
        } else {
            $producer_child = null;
        }
        if ($request->has('p-min') || $request->has('p-max')) {
//            dd($request->has('p-max'));

            if($request->has('p-min') && $request->has('p-max') == false){
                $p_min = str_replace(",", "", $request->get('p-min'));
                $products = $products->where('price_sale', ">" ,(int)$p_min);
//                dd($products->get());
            }else{
                $p_min = str_replace(",", "", $request->get('p-min'));
                $p_max = str_replace(",", "", $request->get('p-max'));
                $products = $products->whereBetween('price_sale', [(int)$p_min, (int)$p_max]);
            }


        }

        if ($request->has('sort')) {
            if ($request->get('sort') == 'new') {
                $products = $products->orderBy('id', 'DESC');
            }
            if ($request->get('sort') == 'min-max') {
                $products = $products->orderBy('price_sale', 'DESC');
            }
            if ($request->get('sort') == 'max-min') {
                $products = $products->orderBy('price_sale', 'ASC');
            }
        }
//        dd($products->get());
        $products = $products;
        $request->flash();

        //KLQ
        $category = CategoryTermModel::where('level', '<>', 1)->get();
        $producer = ProducerModel::all();
        $news_lq = PostsModel::where('status', 1)->orderBy('id', 'DESC')->take(3)->get();


        $response = [
            "title" => "Danh mục sản phẩm - MAG",
            "products" => $products->paginate(10),
            "category" => $category,
            "producer" => $producer,
            "category_child" => $category_child,
            "slug" => $slug,
            "producer_child" => $producer_child,
            "news_lq" => $news_lq,
        ];

        return view('font-end.product_category', $response);
    }


    public function getProductDetail(Request $request, $cat, $slug)
    {
        $category_seo = CategoryTermModel::where('link', $cat)->first();
        $product_detail = ProductsModel::where('slug', $slug)->first();
        $product_highlight = ProductsModel::where('status', 1)->take(10)->get();
        $img_product = ImagesProductsModel::where('product_id', $product_detail->id)->get();
        $comment = RateModel::where('product_id', $product_detail->id)->get();
//        dd($product_highlight);

        //KLQ
        $categoryAll = CategoryTermModel::where('level', '<>', 1)->get();
        $producer = ProducerModel::all();

        $response = [
            "title" => "Chi tiết sản phẩm - MAG",
            "product_detail" => $product_detail,
            "product_highlight" => $product_highlight,
            "img_product" => $img_product,
            "category" => $categoryAll,
            "category_seo" => $category_seo,
            "producer" => $producer,
            "comment" => $comment,
        ];

        return view('font-end.product_detail', $response);
    }


}