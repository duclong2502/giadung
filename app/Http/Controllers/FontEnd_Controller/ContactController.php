<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/7/2018
 * Time: 3:28 PM
 */

namespace App\Http\Controllers\FontEnd_Controller;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function getContact()
    {
        $response = [
            "title" => "Liên hệ - MAG",
        ];

        return view('font-end.contact', $response);
    }

    public function postContact(ContactRequest $request)
    {
        $request->save();
        return Redirect::back()->withSuccess("Đăng ký liên hệ thành công");
    }
}