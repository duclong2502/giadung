<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/8/2018
 * Time: 10:38 AM
 */

namespace App\Http\Controllers\FontEnd_Controller;

use App\OrderDetailModel;
use App\OrdersModel;
use App\RateModel;
use Cookie;
use App\CartModel;
use App\Http\Controllers\Controller;
use App\ProductsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class CartController extends Controller
{

    public function getCart(Request $request)
    {
        $cookie = $request->cookie('a_p');
        $item = CartModel::where('cookies', $cookie)->get();
        $response = [
            "title" => "Chi tiết giỏ hàng - MAG",
            "item" => $item,
            "cookie" => $cookie,
        ];

        return view('font-end.cart_detail', $response);
    }

    public function addCart(Request $request)
    {
        $id = $request->get('pid');
        $product = ProductsModel::where('id', $id)->first();
        $total = $request->get('count');
        $value = $request->cookie('a_p');

        //check-exist product in cart
        $exist = CartModel::where('product_id', $id)->first();

        //check 1 nguoi dat cung 1 sp
        if ($exist && $exist->cookies == $value) {
            try {
                $now = date("Y-m-d H:i:s");
                $cart = CartModel::where("id", $exist->id)->first();
                $cart->total += $total;
                $cart->updated_at = $now;

                $cart->save();
                $request->session()->flash('success', 'Thêm vào giỏ hàng thành công');
                $return = [
                    "stt" => 1,
                    "mess" => 'Đã thêm vào giỏ hàng'
                ];
            } catch (\Exception $e) {
                $return = [
                    "stt" => 0,
                    "mess" => 'Thêm vào giỏ hàng không thành công'
                ];
            }
        } //them vao gio lan dau
        else {
            try {
                $cart = new CartModel();
                $now = date("Y-m-d H:i:s");

                $cart->product_id = (int)$id;
                $cart->name = $product->name;
                $cart->price = $product->price_sale;
                $cart->total = $total;
                $cart->cookies = $value;
                $cart->image = $product->image;
                $cart->status = 1;

//                $cart->created_at = $now;
//                $cart->updated_at = $now;
                $cart->save();
                $request->session()->flash('success', 'Thêm vào giỏ hàng thành công!');
                $return = [
                    "stt" => 1,
                    "mess" => 'Đã thêm vào giỏ hàng!'
                ];

            } catch (\Exception $e) {
                $return = [
                    "stt" => 0,
                    "mess" => 'Thêm vào giỏ hàng không thành công!'
                ];
            }

        }

        return response()->json($return);
    }

    public function removeProduct(Request $request)
    {

        $id = $request->id;
        $cookie = $request->cookie('a_p');
        $product_cart = CartModel::where("id", $id)->first();

        if ($product_cart->cookies == $cookie) {
            CartModel::where("id", $id)->delete();
            $return = [
                "stt" => 1,
                "mess" => 'Đã xóa'
            ];
            return response()->json($return);
        }
    }


    public function successOrder(Request $request, $cookie)
    {
//        dd($cookie);
        $list = CartModel::where('cookies', $cookie)->get();

        $order = new OrdersModel();
        $now = date("Y-m-d H:i:s");
        $code = 'MAG_' . $request->get('name') . '_' . time() . rand(0, 9999);
//        dd($code);

        $order->customer_name = $request->get('name');
        $order->email = $request->get('email');
        $order->phone = $request->get('phone');
        $order->payment_method = 0;
        $order->address = $request->get('address');
        $order->message = $request->get('message');
        $order->status = 0;
        $order->created_at = $now;
        $order->updated_at = $now;
        $order->code = $code;
        $order->save();
        foreach ($list as $key => $item) {
            $order_details = new OrderDetailModel();
            $order_details->product_name = $item->name;
            $order_details->image = $item->image;
            $order_details->order_id = $order->id;
            $order_details->product_id = $item->product_id;
            $order_details->amount = $item->total;
            $order_details->price = $item->price;
            $order_details->status = 1;
            $order_details->created_at = $now;
            $order_details->updated_at = $now;
            CartModel::where('cookies', $item->cookies)->delete();
            $order_details->save();
        }
        $mail = $request->input('email');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $date = $now;

        Mail::send('email.order', ["list" => $list, 'code' => $code], function ($message) use ($mail) {
            $message->to($mail, 'Gia dung MAG')->subject('Đặt hàng thành công!');
        });

        $email_admin1 = 'testmail.tuan@gmail.com';

        Mail::send('email.admin1', ['mail' => $mail, 'name' => $name, 'phone' => $phone, 'date' => $date, 'address' => $address, "list" => $list], function ($message) use ($email_admin1) {
            $message->to($email_admin1, 'Gia dung MAG')->subject('THÔNG BÁO CÓ ĐƠN HÀNG!');
        });

        $email_admin = 'mocanhxnk@gmail.com';

        Mail::send('email.admin1', ['mail' => $mail, 'name' => $name, 'phone' => $phone, 'date' => $date, 'address' => $address, "list" => $list], function ($message) use ($email_admin) {
            $message->to($email_admin, 'Gia dung MAG')->subject('THÔNG BÁO CÓ ĐƠN HÀNG!');
        });
        return Redirect::back()->withSuccess("Đặt hàng thành công");
    }


//    ADD RATE
    public function addRate(Request $request, $id)
    {
        $rate = new RateModel();

        $rate->fullname = $request->get('fullname');
        $rate->email = $request->get('email');
        $rate->comment = $request->get('comment');
//        if($request->has('star')){
        $rate->star = $request->get('star');
//        }else{
//            $rate->star = 5;
//        }
        $rate->product_id = $id;
        $rate->save();
        return Redirect::back()->withSuccess("Cảm ơn bạn đã để lại đánh giá!");
    }

    public function plus(Request $request)
    {

        $id = $request->id;
        $quality = $request->quality;
        $cookie = $request->cookie('a_p');
        $product_cart = CartModel::where("id", $id)->first();

        if ($product_cart->cookies == $cookie) {
            $product_cart->total = $quality;
            $product_cart->save();

            $return = [
                "stt" => 1,
                "mess" => 'đã thay đổi số lượng' . $id . 'thành công',
            ];
            return response()->json($return);
        }
    }

    public function minus(Request $request)
    {

        $id = $request->id;
        $quality = $request->quality;
        $cookie = $request->cookie('a_p');
        $product_cart = CartModel::where("id", $id)->first();

        if ($product_cart->cookies == $cookie) {
            $product_cart->total = $quality;
            $product_cart->save();

            $return = [
                "stt" => 1,
                "mess" => 'đã thay đổi số lượng' . $id . 'thành công!',
            ];
            return response()->json($return);
        }
    }

    public function thanhtoan(Request $request){
        $cookie = $request->cookie('a_p');
        $item = CartModel::where('cookies', $cookie)->get();
        $response = [
            "title" => "Thanh toán đơn hàng",
            "item" => $item,
            "cookie" => $cookie,
        ];
        return view('font-end.thanhtoan', $response);
    }
}