<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/7/2018
 * Time: 1:55 PM
 */

namespace App\Http\Controllers\FontEnd_Controller;

use App\CategoryTermModel;
use App\Http\Controllers\Controller;
use App\PostsModel;
use App\ProductsModel;
use App\SlidersModel;
use App\WarrantyModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    public function getHome(Request $request)
    {
        $products = ProductsModel::where('status', 1);
        $products_new = ProductsModel::where('status', 1)->orderBy('id','DESC')->take(16)->get();
        if ($request->has('home-search')) {
            $products = $products->where('name', "like", "%" . $request->get('home-search') . "%");
        }


        $category_parent = CategoryTermModel::where('level', '<>', 1)->get();
        $sliders = SlidersModel::where('status', 1)->get();
        $news = PostsModel::where('status', 1)->take(6)->get();
        $products = $products->orderBy('id', "DESC")->take(12);

        $products = $products->get();

        $response = [
            "title" => "Home - MAG",
            "sliders" => $sliders,
            "news" => $news,
            "products" => $products,
            "products_new" => $products_new,
            "category_parent" => $category_parent,
        ];

        return view('font-end.home', $response);
    }


//    TIN TỨC
    public function getNews()
    {
        $news = PostsModel::where('status', 1)->orderBy('id', 'DESC')->take(5);
        $products = ProductsModel::where('status', 1)->orderBy('id', 'DESC')->take(5)->get();
        $news_lq = PostsModel::where('status', 1)->orderBy('id', 'DESC')->take(3)->get();

        $response = [
            "title" => "Tin tức - MAG",
            'news' => $news->paginate(5),
            'products' => $products,
            'news_lq' => $news_lq,
        ];

        return view('font-end.news', $response);
    }

    public function getNewsDetail($slug)
    {
        $new_detail = PostsModel::where('slug', $slug)->first();
        $population_news = PostsModel::where('status', 1)->orderBy('id', "DESC")->take(3)->get();
        $products = ProductsModel::where('status', 1)->orderBy('id', 'DESC')->take(5)->get();

        $response = [
            "title" => "Chi tiết tin tức - MAG",
            "new_detail" => $new_detail,
            "population_news" => $population_news,
            "products" => $products,
        ];

        return view('font-end.news_detail', $response);
    }

//    CHẾ ĐỘ BẢO HÀNH
    public function getWarranty()
    {
        $warranty = WarrantyModel::where('status', 1)->orderBy('id', 'DESC')->take(5);
        $products = ProductsModel::where('status', 1)->orderBy('id', 'DESC')->take(5)->get();
        $news_lq = PostsModel::where('status', 1)->orderBy('id', 'ASC')->take(3)->get();
        $response = [
            "title" => "Chế độ bảo hành - MAG",
            'warranty' => $warranty->paginate(5),
            'products' => $products,
            'news_lq' => $news_lq,
        ];

        return view('font-end.warranty', $response);
    }


    public function btnFilter(Request $request)
    {
        $link = $request->get('link');
        $category_child = CategoryTermModel::where('link', $link)->first();
        $products = ProductsModel::where('category_id', $category_child->id)->get();

        return view('font-end.filter_home',compact('products'));
    }
}