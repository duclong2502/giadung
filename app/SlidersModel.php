<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2018
 * Time: 8:53 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlidersModel extends Model
{
    protected $table = 'sliders';
    protected $fillable = ['name', 'link', 'image', 'status', 'title_seo', 'description_seo', 'primary_key_seo', 'sub_key_seo', 'link_seo'];
}