<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    protected $table = 'contact';
    protected $fillable = ['customer_name', 'email', 'phone', 'message', 'status'];
}
