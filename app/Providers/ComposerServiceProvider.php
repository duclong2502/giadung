<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/10/2018
 * Time: 2:17 PM
 */

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider  extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...

        View::composer(
            'font-end.*', 'App\Http\ViewComposers\HeaderComposers'
        );
        View::composer(
            'font-end.layout.*', 'App\Http\ViewComposers\FooterComposers'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}