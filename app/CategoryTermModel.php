<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTermModel extends Model
{
    protected $table = 'category_term';
    protected $fillable = ['name', 'link', 'type', 'parent_id', 'level', 'status', 'menu_group_id', 'image', 'description'];

    public function childs()
    {
        return $this->hasMany('App\CategoryTermModel', 'parent_id', 'id');
    }
}
