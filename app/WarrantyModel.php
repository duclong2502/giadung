<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarrantyModel extends Model
{
    protected $table = 'warranty';
    protected $fillable = ['name', 'link', 'image', 'status', 'title_seo', 'description_seo', 'primary_key_seo', 'sub_key_seo', 'link_seo'];
}
