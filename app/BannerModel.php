<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerModel extends Model
{
    protected $table = 'banner';
    protected $fillable = ['name', 'image', 'banner_group_id', 'link', 'status'];
}
