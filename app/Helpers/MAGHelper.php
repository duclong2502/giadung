<?php

class MAGHelper
{
    public static function getSlugCategory($id)
    {
        $slug = \App\CategoryTermModel::select('link')->where('id', $id)->first();
        return $slug['link'];
    }

    public static function getTitleSeoConfig($key = "")
    {
        $title = \App\ConfigModel::where('key', $key)->first();
        return $title['content'];
    }

    public static function getTotalProduct($id)
    {
        $products = \App\ProductsModel::where('category_id', $id)->get();
        return count($products);
    }

    public static function getTotalProductByParent($id)
    {
        $child = \App\CategoryTermModel::where('parent_id', $id)->get();
//        dd($id);
        $products = \App\ProductsModel::whereIn('category_id', $child)->get();
        return count($products);
    }

    public static function getSlugById($id)
    {
        $slug = \App\CategoryTermModel::select('link')->where('id', $id)->first();
        dd($slug);
    }
}
