<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/admin', 'Cms\LoginController@getLogin')->name('loginAdmin');
Route::post('/admin', 'Cms\LoginController@postLogin')->name('postLogin');
Route::get('/logout', 'Cms\LoginController@logout');


Route::group(['middleware' => 'admin_auth'], function () {
    Route::get('/dashboard', "Cms\DashboardController@dashboard")->name('dashboard');

    Route::group(['prefix' => 'config', 'middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\ConfigController@index");
        Route::get('/edit/{id}', "Cms\ConfigController@edit");
        Route::post('/edit/{id}', "Cms\ConfigController@doEdit");
    });
    Route::group(['prefix' => 'user', 'middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\AdminController@index");
        Route::get('/add', "Cms\AdminController@add");
        Route::post('/add', "Cms\AdminController@doAdd");
//        Route::get('/edit/{id}', "Cms\AdminController@edit");
//        Route::post('/edit/{id}', "Cms\AdminController@doEdit");
        Route::get('/delete/{id}', "Cms\AdminController@delete");

    });

    Route::group(['prefix' => 'slider','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\SlidersController@index");
        Route::get('/add', "Cms\SlidersController@add");
        Route::post('/add', "Cms\SlidersController@doAdd");
        Route::get('/edit/{id}', "Cms\SlidersController@edit");
        Route::post('/edit/{id}', "Cms\SlidersController@doEdit");
        Route::get('/delete/{id}', "Cms\SlidersController@delete");
    });

    Route::group(['prefix' => 'post'], function () {
        Route::get('/', "Cms\PostsController@index");
        Route::get('/add', "Cms\PostsController@add");
        Route::post('/add', "Cms\PostsController@doAdd");
        Route::get('/edit{id}', "Cms\PostsController@edit");
        Route::post('/edit{id}', "Cms\PostsController@doEdit");
        Route::get('/delete/{id}', "Cms\PostsController@delete");
    });

    Route::group(['prefix' => 'warranty','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\WarrantyController@index");
        Route::get('/add', "Cms\WarrantyController@add");
        Route::post('/add', "Cms\WarrantyController@doAdd");
        Route::get('/edit/{id}', "Cms\WarrantyController@edit");
        Route::post('/edit/{id}', "Cms\WarrantyController@doEdit");
        Route::get('/delete/{id}', "Cms\WarrantyController@delete");

    });
    Route::group(['prefix' => 'producer'], function () {
        Route::get('/', "Cms\ProducerController@index");
        Route::get('/add', "Cms\ProducerController@add");
        Route::post('/add', "Cms\ProducerController@doAdd");
        Route::get('/edit{id}', "Cms\ProducerController@edit");
        Route::post('/edit{id}', "Cms\ProducerController@doEdit");
        Route::get('/delete/{id}', "Cms\ProducerController@delete");
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', "Cms\ProductsController@index");
        Route::get('/add', "Cms\ProductsController@add");
        Route::post('/add', "Cms\ProductsController@doAdd");
        Route::get('/edit/{id}', "Cms\ProductsController@edit");
        Route::post('/edit/{id}', "Cms\ProductsController@doEdit");
        Route::get('/delete/{id}', "Cms\ProductsController@delete");

        //anh chi tiet
        Route::get('/image-detail/{id}', "Cms\ProductsController@imageDetail");
        Route::post('/image-detail', 'Cms\ProductsController@postImages')->name('uploadProImg');
        Route::post('deleteImg/{value}', 'Cms\ProductsController@postDeleteImage');
        Route::get('deleteSub/{id}', 'Cms\ProductsController@getDeleteSub');
    });

    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', "Cms\ContactController@index");
        Route::get('/delete/{id}', "Cms\ContactController@delete");
    });

    Route::group(['prefix' => 'category-term'], function () {
        Route::get('/', "Cms\CategoryTermController@index");
        Route::get('/add', "Cms\CategoryTermController@add");
        Route::post('/add', "Cms\CategoryTermController@doAdd");
        Route::get('/edit/{id}', "Cms\CategoryTermController@edit");
        Route::post('/edit/{id}', "Cms\CategoryTermController@doEdit");
        Route::get('/delete/{id}', "Cms\CategoryTermController@delete");
    });

    Route::group(['prefix' => 'orders','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\OrdersController@index");
        Route::get('/detail/{id}', "Cms\OrdersController@detail");
        Route::post('/change-status-order', "Cms\OrdersController@updateStatus");
        Route::get('/delete/{id}', "Cms\OrdersController@delete");
    });

    Route::group(['prefix' => 'footer','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\FooterController@index");
        Route::get('/edit/{id}', "Cms\FooterController@edit");
    });

    Route::group(['prefix' => 'banner','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\BannerController@index");
        Route::get('/add', "Cms\BannerController@add");
        Route::post('/add', "Cms\BannerController@doAdd");
        Route::get('/edit/{id}', "Cms\BannerController@edit");
        Route::post('/edit/{id}', "Cms\BannerController@doEdit");
        Route::get('/delete/{id}', "Cms\BannerController@delete");
    });

    Route::group(['prefix' => 'menu','middleware'=> 'check_role'], function () {
        Route::get('/', "Cms\MenuController@index");
        Route::get('/add', "Cms\MenuController@add");
        Route::post('/add', "Cms\MenuController@doAdd");
        Route::get('/edit/{id}', "Cms\MenuController@edit");
        Route::post('/edit/{id}', "Cms\MenuController@doEdit");
        Route::get('/delete/{id}', "Cms\MenuController@delete");
    });

});





// START FONT_END
Route::get('/', 'FontEnd_Controller\HomeController@getHome')->name('/');
Route::get('/abc', 'FontEnd_Controller\HomeController@btnFilter');

//liên hệ
Route::get('/lien-he', 'FontEnd_Controller\ContactController@getContact');
Route::post('/lien-he', 'FontEnd_Controller\ContactController@postContact');
//TIN TỨC
Route::get('/tin-tuc', 'FontEnd_Controller\HomeController@getNews');
Route::get('/tin-tuc/{slug}', 'FontEnd_Controller\HomeController@getNewsDetail')->name('newsDetail');

Route::get('/danh-muc/{slug}/{hang?}', 'FontEnd_Controller\ProductController@getProductCategory')->name('getProductCategory');
//Route::get('/chi-tiet-san-pham/{slug}/{cat?}', 'FontEnd_Controller\ProductController@getProductDetail')->name('productDetail');
Route::get('/chi-tiet-san-pham/{cat}/{slug}', 'FontEnd_Controller\ProductController@getProductDetail')->name('productDetail');


//BẢO HÀNH
Route::get('/che-do-bao-hanh', 'FontEnd_Controller\HomeController@getWarranty');

Route::get('/gio-hang', 'FontEnd_Controller\CartController@getCart');
Route::get('/them-vao-gio', 'FontEnd_Controller\CartController@addCart');
Route::post('/xoa-san-pham-trong-gio', 'FontEnd_Controller\CartController@removeProduct');
Route::post('/dat-hang/{cookie}', 'FontEnd_Controller\CartController@successOrder')->name('dathang');
Route::post('/danh-gia-san-pham/{id}', 'FontEnd_Controller\CartController@addRate');
Route::post('/plus', 'FontEnd_Controller\CartController@plus');
Route::post('/minus', 'FontEnd_Controller\CartController@minus');
Route::get('/thanh-toan-don-hang', 'FontEnd_Controller\CartController@thanhtoan')->name('thanhtoandonhang');


Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
