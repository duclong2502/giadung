/*
 Navicat Premium Data Transfer

 Source Server         : giadung
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : 123.31.41.31:3306
 Source Schema         : giadung

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 17/08/2018 15:39:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cookies` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES (20, 15, 'Máy hút mùi Bosch DWW09W851B', '11780000', '1', '15344730905', '/images/product/1534319509.5.jpg', 1, '2018-08-17 02:31:36', '2018-08-17 02:31:36');
INSERT INTO `cart` VALUES (21, 14, 'Bếp ga hồng ngoại', '18360000', '1', '15344730905', '/images/product/1534319274.4.jpg', 1, '2018-08-17 02:31:42', '2018-08-17 02:31:42');
INSERT INTO `cart` VALUES (22, 13, 'Bếp điện từ Bosch PIC645F17E', '18490000', '1', '15344730905', '/images/product/1534318914.3.jpg', 1, '2018-08-17 02:31:44', '2018-08-17 02:31:44');
INSERT INTO `cart` VALUES (27, 15, 'Máy hút mùi Bosch DWW09W851B', '11780000', '1', '15341250572', '/images/product/1534319509.5.jpg', 1, '2018-08-17 02:47:30', '2018-08-17 02:47:30');
INSERT INTO `cart` VALUES (28, 14, 'Bếp ga hồng ngoại', '18360000', '1', '15341250572', '/images/product/1534319274.4.jpg', 1, '2018-08-17 02:47:33', '2018-08-17 02:47:33');

-- ----------------------------
-- Table structure for category_term
-- ----------------------------
DROP TABLE IF EXISTS `category_term`;
CREATE TABLE `category_term`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `menu_group_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category_term
-- ----------------------------
INSERT INTO `category_term` VALUES (2, 'Đồ gia dụng', 'do-gia-dung', '0', '0', 1, 1, 1, '2018-08-13 09:05:20', '2018-08-14 08:06:07', '');
INSERT INTO `category_term` VALUES (3, 'Hút mùi', 'hut-mui', '0', '0', 1, 1, 1, '2018-08-14 07:16:41', '2018-08-14 08:37:17', '');
INSERT INTO `category_term` VALUES (4, 'Bếp', 'bep', '0', '0', 1, 1, 1, '2018-08-14 07:38:37', '2018-08-14 10:43:49', '');
INSERT INTO `category_term` VALUES (5, 'Bếp điện từ', 'bep-dien-tu', '0', '4', 2, 1, 1, '2018-08-14 08:03:36', '2018-08-14 10:44:16', '');
INSERT INTO `category_term` VALUES (6, 'Bếp ga', 'bep-ga', '0', '4', 2, 1, 1, '2018-08-14 08:03:51', '2018-08-14 10:44:27', '');
INSERT INTO `category_term` VALUES (9, 'Nồi chiên rán không dầu', 'noi-chien-ran-khong-dau', '0', '2', 2, 1, 1, '2018-08-14 08:07:59', '2018-08-14 08:07:59', '');
INSERT INTO `category_term` VALUES (10, 'Thiết bị sưởi', 'thiet-bi-suoi', '0', '2', 2, 1, 1, '2018-08-14 08:08:20', '2018-08-14 08:08:20', '');
INSERT INTO `category_term` VALUES (11, 'Nồi cơm đa năng', 'noi-com-da-nang', '0', '2', 2, 1, 0, '2018-08-14 08:08:37', '2018-08-14 08:08:37', '');
INSERT INTO `category_term` VALUES (12, 'Máy xay đa năng', 'may-xay-da-nang', '0', '2', 2, 1, 0, '2018-08-14 08:08:50', '2018-08-14 08:08:50', '');
INSERT INTO `category_term` VALUES (13, 'Máy hút bụi', 'may-hut-bui', '0', '2', 2, 1, 0, '2018-08-14 08:09:06', '2018-08-16 08:20:24', '/images/category/1534407624.banner.png');
INSERT INTO `category_term` VALUES (17, 'Máy hút mùi', 'may-hut-mui', '0', '3', 2, 1, 2, '2018-08-17 07:32:37', '2018-08-17 07:32:37', '/images/category/1534491157.1.jpg');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 'title', 'Đồ gia dụng MAG', 1, '2018-08-01 08:50:03', '2018-08-08 10:30:51');
INSERT INTO `config` VALUES (2, 'facebook', 'https://www.facebook.com/', 1, '2018-08-11 08:52:57', '2018-08-11 08:53:01');
INSERT INTO `config` VALUES (3, 'youtube', 'https://www.youtube.com/', 1, NULL, NULL);
INSERT INTO `config` VALUES (4, 'instagram', 'https://www.instagram.com/', 1, NULL, NULL);
INSERT INTO `config` VALUES (5, 'fanpage_facebook', 'https://www.facebook.com/giadungeus/', 1, NULL, '2018-08-16 08:38:22');
INSERT INTO `config` VALUES (6, 'footer', '<div class=\"footer-contacts-info\">\r\n<h5>Gia dụng EUS</h5>\r\n</div>\r\n\r\n<div class=\"footer-contacts-list\">\r\n<ul class=\"list-unstyled\">\r\n	<li><i class=\"fa fa-map-pin fa-fw\"></i><span>29 Khuất Duy Tiến, Thanh Xu&acirc;n, H&agrave; Nội.</span></li>\r\n	<li><i class=\"fa fa-phone fa-fw\"></i><span>086-89-26-335; 098-91-65-335</span></li>\r\n	<li><i class=\"fa fa-envelope-o fa-fw\"></i><span>manager.mag@gmail.com</span></li>\r\n</ul>\r\n</div>', 1, NULL, '2018-08-17 07:06:34');
INSERT INTO `config` VALUES (7, 'title_seo', 'title_seo', 1, NULL, NULL);
INSERT INTO `config` VALUES (8, 'link_seo', 'link_seo', 1, NULL, NULL);
INSERT INTO `config` VALUES (9, 'primary_key_seo', 'primary_key_seo', 1, NULL, NULL);
INSERT INTO `config` VALUES (10, 'sub_key_seo', 'sub_key_seo', 1, NULL, NULL);
INSERT INTO `config` VALUES (11, 'description_seo', 'description_seo', 1, NULL, NULL);
INSERT INTO `config` VALUES (12, 'google_plus', 'https://plus.google.com/?hl=vi', 1, NULL, NULL);

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `message` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES (1, 'HuyTD', 'huy@gmail.com', '0986911094', 'A huy', 1, NULL, NULL);
INSERT INTO `contact` VALUES (2, 'Đặng Thu', 'thuhubt96@gmail.com', '0975082614', 'Chất lượng sản phẩm tốt nhưng mãu mã chưa thực sự ấn tượng', 1, '2018-08-10 08:17:26', '2018-08-10 08:17:26');
INSERT INTO `contact` VALUES (3, 'Tuntin', 'dangthu.th1905@gmail.com', '67628762592', 'abc', 0, '2018-08-10 08:33:52', '2018-08-10 08:33:52');
INSERT INTO `contact` VALUES (4, 'Đặng Thu', 'thuhubt96@gmail.com', '0975082614', 'Chất lượng sản phẩm tốt nhưng mẫu mã chưa thật sự ấn tượng', 0, '2018-08-10 08:42:21', '2018-08-10 08:42:21');
INSERT INTO `contact` VALUES (5, 'Trần Huy', 'huycrazy@gmail', 'a', 'Nội dung', 0, '2018-08-10 08:53:03', '2018-08-10 08:53:03');
INSERT INTO `contact` VALUES (7, 'Tun Tin', 'thuhubt96@gmail.com', NULL, NULL, 0, '2018-08-14 08:20:35', '2018-08-14 08:20:35');
INSERT INTO `contact` VALUES (8, 'Đặng Thu', 'thuhubt96@gmail.com', '558765798749', 'abc', 0, '2018-08-16 07:58:15', '2018-08-16 07:58:15');

-- ----------------------------
-- Table structure for image_product
-- ----------------------------
DROP TABLE IF EXISTS `image_product`;
CREATE TABLE `image_product`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of image_product
-- ----------------------------
INSERT INTO `image_product` VALUES (1, 'upload/products/subs/1534143593.ictc-comm_1.jpg', 7, '2018-08-13 07:00:00', '2018-08-13 07:00:00');
INSERT INTO `image_product` VALUES (2, 'upload/products/subs/1534143593.khach-tham-quan.jpg', 7, '2018-08-13 07:00:00', '2018-08-13 07:00:00');
INSERT INTO `image_product` VALUES (3, 'upload/products/subs/1534146805.nt18.png', 3, '2018-08-13 07:50:11', '2018-08-13 07:53:27');
INSERT INTO `image_product` VALUES (4, 'upload/products/subs/1534146772.nt3.png', 7, '2018-08-13 07:53:03', '2018-08-13 07:53:03');
INSERT INTO `image_product` VALUES (5, 'upload/products/subs/1534154596.a.jpg', 5, '2018-08-13 07:53:03', '2018-08-13 10:03:20');
INSERT INTO `image_product` VALUES (6, 'upload/products/subs/1534146872.photo1521191257878-15211912578782063306127.jpg', 7, '2018-08-13 07:54:34', '2018-08-13 07:54:34');
INSERT INTO `image_product` VALUES (7, 'upload/products/subs/1534147006.1.jpg', 8, '2018-08-13 07:58:24', '2018-08-13 07:58:24');
INSERT INTO `image_product` VALUES (8, 'upload/products/subs/1534147007.2.jpg', 8, '2018-08-13 07:58:24', '2018-08-13 07:58:24');
INSERT INTO `image_product` VALUES (9, 'upload/products/subs/1534147010.3.jpg', 8, '2018-08-13 07:58:24', '2018-08-13 07:58:24');
INSERT INTO `image_product` VALUES (10, 'upload/products/subs/1534154497.11.jpg', 9, '2018-08-13 10:01:55', '2018-08-13 10:01:55');
INSERT INTO `image_product` VALUES (11, 'upload/products/subs/1534154498.12.jpg', 9, '2018-08-13 10:01:55', '2018-08-13 10:01:55');
INSERT INTO `image_product` VALUES (12, 'upload/products/subs/1534154502.13.jpg', 9, '2018-08-13 10:01:55', '2018-08-13 10:01:55');
INSERT INTO `image_product` VALUES (13, 'upload/products/subs/1534316837.images (1).jpg', 10, '2018-08-15 07:11:31', '2018-08-15 07:11:31');
INSERT INTO `image_product` VALUES (14, 'upload/products/subs/1534316839.images.jpg', 10, '2018-08-15 07:11:31', '2018-08-15 07:11:31');
INSERT INTO `image_product` VALUES (15, 'upload/products/subs/1534316843.tải xuống (2).jpg', 10, '2018-08-15 07:11:31', '2018-08-15 07:11:31');
INSERT INTO `image_product` VALUES (16, 'upload/products/subs/1534317412.11.jpg', 11, '2018-08-15 07:24:01', '2018-08-15 07:24:01');
INSERT INTO `image_product` VALUES (17, 'upload/products/subs/1534317414.111.jpg', 11, '2018-08-15 07:24:01', '2018-08-15 07:24:01');
INSERT INTO `image_product` VALUES (18, 'upload/products/subs/1534317416.1.jpg', 11, '2018-08-15 07:24:01', '2018-08-15 07:24:01');
INSERT INTO `image_product` VALUES (19, 'upload/products/subs/1534317419.1111.jpg', 11, '2018-08-15 07:24:01', '2018-08-15 07:24:01');
INSERT INTO `image_product` VALUES (20, 'upload/products/subs/1534318036.22.jpg', 12, '2018-08-15 07:29:12', '2018-08-15 07:29:12');
INSERT INTO `image_product` VALUES (21, 'upload/products/subs/1534318038.222.jpg', 12, '2018-08-15 07:29:12', '2018-08-15 07:29:12');
INSERT INTO `image_product` VALUES (22, 'upload/products/subs/1534318040.2222.jpg', 12, '2018-08-15 07:29:12', '2018-08-15 07:29:12');
INSERT INTO `image_product` VALUES (23, 'upload/products/subs/1534318905.33.jpg', 13, '2018-08-15 07:41:54', '2018-08-15 07:41:54');
INSERT INTO `image_product` VALUES (24, 'upload/products/subs/1534318908.333.png', 13, '2018-08-15 07:41:54', '2018-08-15 07:41:54');
INSERT INTO `image_product` VALUES (25, 'upload/products/subs/1534318910.3333.jpg', 13, '2018-08-15 07:41:54', '2018-08-15 07:41:54');
INSERT INTO `image_product` VALUES (26, 'upload/products/subs/1534319235.44.jpg', 14, '2018-08-15 07:47:54', '2018-08-15 07:47:54');
INSERT INTO `image_product` VALUES (27, 'upload/products/subs/1534319237.444.jpg', 14, '2018-08-15 07:47:54', '2018-08-15 07:47:54');
INSERT INTO `image_product` VALUES (28, 'upload/products/subs/1534319241.4444.jpg', 14, '2018-08-15 07:47:55', '2018-08-15 07:47:55');
INSERT INTO `image_product` VALUES (30, 'upload/products/subs/1534319478.555.jpg', 15, '2018-08-15 07:51:49', '2018-08-15 07:51:49');
INSERT INTO `image_product` VALUES (31, 'upload/products/subs/1534319481.5555.jpg', 15, '2018-08-15 07:51:49', '2018-08-15 07:51:49');
INSERT INTO `image_product` VALUES (32, 'upload/products/subs/1534411914.39273572_1796443610439419_4776744875787812864_n.jpg', 16, '2018-08-16 09:32:01', '2018-08-16 09:32:01');

-- ----------------------------
-- Table structure for menu_group
-- ----------------------------
DROP TABLE IF EXISTS `menu_group`;
CREATE TABLE `menu_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_group
-- ----------------------------
INSERT INTO `menu_group` VALUES (1, 'menu_header', 'Menu Header', 1, NULL, NULL);
INSERT INTO `menu_group` VALUES (2, 'menu_footer', 'Menu Footer', 1, NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_08_01_033242_create_config_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_08_01_034557_create_sliders_table', 2);
INSERT INTO `migrations` VALUES (5, '2018_08_02_141810_create_posts_table', 3);
INSERT INTO `migrations` VALUES (6, '2018_08_02_150846_create_producer_table', 4);
INSERT INTO `migrations` VALUES (7, '2018_08_02_152329_add_description_to_producer', 5);
INSERT INTO `migrations` VALUES (8, '2018_08_02_153013_add_image_to_producer', 6);
INSERT INTO `migrations` VALUES (9, '2018_08_02_154906_create_menu_group_table', 7);
INSERT INTO `migrations` VALUES (10, '2018_08_03_094015_create_product_table', 8);
INSERT INTO `migrations` VALUES (11, '2018_08_06_025017_create_contact_table', 9);
INSERT INTO `migrations` VALUES (12, '2018_08_06_030230_create_orders_table', 10);
INSERT INTO `migrations` VALUES (13, '2018_08_06_032449_create_orders_detail_table', 11);
INSERT INTO `migrations` VALUES (14, '2018_08_08_082544_update_posts_table', 12);
INSERT INTO `migrations` VALUES (15, '2018_08_09_042941_create_image_product', 12);
INSERT INTO `migrations` VALUES (16, '2018_08_09_084801_create_category_term_table', 12);
INSERT INTO `migrations` VALUES (17, '2018_08_10_030314_create_cart_table', 13);
INSERT INTO `migrations` VALUES (18, '2018_08_10_093236_create_warranty_table', 13);
INSERT INTO `migrations` VALUES (19, '2018_08_10_094824_update_warranty_table', 13);
INSERT INTO `migrations` VALUES (20, '2018_08_11_033930_update_config_table', 13);
INSERT INTO `migrations` VALUES (21, '2018_08_11_034930_update2_config_table', 14);
INSERT INTO `migrations` VALUES (22, '2018_08_13_035532_update_orders_table', 15);
INSERT INTO `migrations` VALUES (23, '2018_08_13_094759_update_contact_table', 16);
INSERT INTO `migrations` VALUES (24, '2018_08_14_042601_update_producer_table', 17);
INSERT INTO `migrations` VALUES (25, '2018_08_14_042820_update2_producer_table', 17);
INSERT INTO `migrations` VALUES (26, '2018_08_14_042938_update3_producer_table', 17);
INSERT INTO `migrations` VALUES (27, '2018_08_15_041104_update2_posts_table', 17);
INSERT INTO `migrations` VALUES (28, '2018_08_15_044336_update_product_table', 18);
INSERT INTO `migrations` VALUES (29, '2018_08_15_045034_update2_warranty_table', 18);
INSERT INTO `migrations` VALUES (30, '2018_08_15_064104_update_sliders_table', 18);
INSERT INTO `migrations` VALUES (31, '2018_08_15_070026_update3_config_table', 18);
INSERT INTO `migrations` VALUES (32, '2018_08_16_075414_update_products_table', 19);
INSERT INTO `migrations` VALUES (33, '2018_08_16_080331_update_category_table', 20);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (3, 'Trần Duy Huy', 'Trần Duy Huy', 'Trần Duy Huy', '0', 'Trần Duy Huy', 'Trần Duy Huy', 0, '2018-08-13 07:02:08', '2018-08-13 07:02:08', 'MAG_Trần Duy Huy_15341437282660');
INSERT INTO `orders` VALUES (4, 'Trần Duy Huy', 'Trần Duy Huy', 'Trần Duy Huy', '0', 'Trần Duy Huy', 'Trần Duy Huy', 0, '2018-08-13 07:03:27', '2018-08-13 07:03:27', 'MAG_Trần Duy Huy_15341438074098');
INSERT INTO `orders` VALUES (5, 'Đặng Thu', 'Đặng Thu', 'Đặng Thu', '0', 'Đặng Thu', 'Đặng Thu', 0, '2018-08-13 08:38:13', '2018-08-13 08:38:13', 'MAG_Đặng Thu_15341494933050');
INSERT INTO `orders` VALUES (6, 'Trần Duy Huy', 'Trần Duy Huy', 'Trần Duy Huy', '0', 'Trần Duy Huy', 'Trần Duy Huy', 0, '2018-08-13 09:36:03', '2018-08-13 09:36:03', 'MAG_Trần Duy Huy_15341529632626');
INSERT INTO `orders` VALUES (7, 'Tuntin', 'Tuntin', 'Tuntin', '0', 'Tuntin', 'Tuntin', 0, '2018-08-14 03:51:17', '2018-08-14 03:51:17', 'MAG_Tuntin_1534218677863');
INSERT INTO `orders` VALUES (10, 'Vũ Thanh Bình', 'Vũ Thanh Bình', 'Vũ Thanh Bình', '0', 'Vũ Thanh Bình', 'Vũ Thanh Bình', 1, '2018-08-16 07:42:36', '2018-08-16 07:44:30', 'MAG_Vũ Thanh Bình_15344053564865');
INSERT INTO `orders` VALUES (11, 'Đặng Thu', 'thuhubt96@gmail.com', '0975082614', '0', 'Hà Nội', 'abc', 0, '2018-08-16 07:49:59', '2018-08-16 07:49:59', 'MAG_Đặng Thu_15344057997828');
INSERT INTO `orders` VALUES (12, 'qewe', 'eqweqwe', 'eqweqwe', '0', 'qưeqwe', 'qưeqwewe', 0, '2018-08-17 02:59:17', '2018-08-17 02:59:17', 'MAG_qewe_15344747579671');
INSERT INTO `orders` VALUES (13, 'nguyen linh', 'linhdesign12081994@gmail.com', '965313647', '0', '167 trung kinh cau giay ha noi', 'qưeqwewe', 0, '2018-08-17 02:59:43', '2018-08-17 02:59:43', 'MAG_nguyen linh_15344747837121');
INSERT INTO `orders` VALUES (14, 'nguyen linh', 'linhdesign12081994@gmail.com', '965313647', '0', '167 trung kinh cau giay ha noi', 'qưeqwewe', 0, '2018-08-17 02:59:46', '2018-08-17 02:59:46', 'MAG_nguyen linh_15344747867542');

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders_detail
-- ----------------------------
INSERT INTO `orders_detail` VALUES (4, 'NN', '/images/product/1534143600.banner.jpg', '3', '7', '1', '140000', 1, '2018-08-13 07:02:08', '2018-08-13 07:02:08');
INSERT INTO `orders_detail` VALUES (5, 'NN', '/images/product/1534143600.banner.jpg', '4', '7', '1', '140000', 1, '2018-08-13 07:03:27', '2018-08-13 07:03:27');
INSERT INTO `orders_detail` VALUES (6, 'Nồi cơm đa năng', '/images/product/1534147104.4.jpg', '5', '8', '1', '5000000', 1, '2018-08-13 08:38:13', '2018-08-13 08:38:13');
INSERT INTO `orders_detail` VALUES (7, 'NN', '/images/product/1534143600.banner.jpg', '6', '7', '1', '1200000', 1, '2018-08-13 09:36:03', '2018-08-13 09:36:03');
INSERT INTO `orders_detail` VALUES (8, 'Nồi cơm đa năng', '/images/product/1534147104.4.jpg', '6', '8', '1', '4750000', 1, '2018-08-13 09:36:03', '2018-08-13 09:36:03');
INSERT INTO `orders_detail` VALUES (9, 'Robot hút bụi lau nhà', '/images/product/1534154515.14.jpg', '7', '9', '1', '7500000', 1, '2018-08-14 03:51:17', '2018-08-14 03:51:17');
INSERT INTO `orders_detail` VALUES (11, 'Máy sưởi ấm panasonic', '/images/product/1534318152.2.jpg', '10', '12', '1', '5500000', 1, '2018-08-16 07:42:36', '2018-08-16 07:42:36');
INSERT INTO `orders_detail` VALUES (12, 'Máy xay đa năng', '/images/product/1534317091.images.jpg', '10', '10', '1', '4590000', 1, '2018-08-16 07:42:36', '2018-08-16 07:42:36');
INSERT INTO `orders_detail` VALUES (13, 'Bếp điện từ Bosch PIC645F17E', '/images/product/1534318914.3.jpg', '11', '13', '1', '18490000', 1, '2018-08-16 07:49:59', '2018-08-16 07:49:59');
INSERT INTO `orders_detail` VALUES (14, 'Máy hút mùi Bosch DWW09W851B', '/images/product/1534319509.5.jpg', '12', '15', '1', '11780000', 1, '2018-08-17 02:59:17', '2018-08-17 02:59:17');
INSERT INTO `orders_detail` VALUES (15, 'Bếp ga hồng ngoại', '/images/product/1534319274.4.jpg', '12', '14', '1', '18360000', 1, '2018-08-17 02:59:17', '2018-08-17 02:59:17');
INSERT INTO `orders_detail` VALUES (16, 'Bếp điện từ Bosch PIC645F17E', '/images/product/1534318914.3.jpg', '12', '13', '1', '18490000', 1, '2018-08-17 02:59:17', '2018-08-17 02:59:17');
INSERT INTO `orders_detail` VALUES (17, 'Máy sưởi ấm panasonic', '/images/product/1534318152.2.jpg', '12', '12', '1', '5500000', 1, '2018-08-17 02:59:17', '2018-08-17 02:59:17');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE,
  INDEX `password_resets_token_index`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `title_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `primary_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sub_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `link_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (3, 'Đồ gia dụng Bosch giúp cuộc sống trở nên dễ dàng hơn', 'do-gia-dung-bosch-giup-cuoc-song-tro-nen-de-dang-hon', 'Trong xã hội hiện nay, mỗi người đều phải chung tay giải quyết các công việc nhà.', '/images/post/1533716019.httpchannelvcmediavnprupload270201807img201807301525355347-1533032105970333993230.jpg', '<p>Bosch đại diện cho sự tin cậy, t&iacute;nh bền vững v&agrave; c&ocirc;ng nghệ kỹ thuật cao. Kh&ocirc;ng đặt mục ti&ecirc;u về lợi nhuận như c&aacute;c tập đo&agrave;n kinh tế kh&aacute;c, Bosch lu&ocirc;n đặt c&aacute;c ti&ecirc;u ch&iacute; về độ bền v&agrave; c&ocirc;ng nghệ cao l&ecirc;n h&agrave;ng đầu trong c&aacute;c sản phẩm của m&igrave;nh. Robert Bosch - người s&aacute;ng lập ra thương hiệu Bosch đ&atilde; từng n&oacute;i rằng: &ldquo;T&ocirc;i th&agrave; mất tiền c&ograve;n hơn l&agrave; mất sự tin tưởng. Danh dự, niềm tin v&agrave;o gi&aacute; trị của sản phẩm lu&ocirc;n được ưu ti&ecirc;n hơn những lợi nhuận tạm thời&rdquo;. Nguy&ecirc;n tắc đ&oacute; vẫn l&agrave; nền tảng m&agrave; Bosch theo đuổi để ph&aacute;t triển thương hiệu v&agrave; cho ra đời c&aacute;c sản phẩm chất lượng cao ng&agrave;y nay.</p>', 0, NULL, '2018-08-08 08:13:39', '2018-08-08 08:13:39', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (4, 'Đồ gia dụng Bosch giúp cuộc sống trở nên dễ dàng hơn', 'do-gia-dung-bosch-giup-cuoc-song-tro-nen-de-dang-hon', 'Trong xã hội hiện nay, mỗi người đều phải chung tay giải quyết các công việc nhà. Đó là điều không thể tránh khỏi!', '/images/post/1533716137.httpchannelvcmediavnprupload270201807img201807301525355347-1533032105970333993230.jpg', '<p>Bosch đại diện cho sự tin cậy, t&iacute;nh bền vững v&agrave; c&ocirc;ng nghệ kỹ thuật cao. Kh&ocirc;ng đặt mục ti&ecirc;u về lợi nhuận như c&aacute;c tập đo&agrave;n kinh tế kh&aacute;c, Bosch lu&ocirc;n đặt c&aacute;c ti&ecirc;u ch&iacute; về độ bền v&agrave; c&ocirc;ng nghệ cao l&ecirc;n h&agrave;ng đầu trong c&aacute;c sản phẩm của m&igrave;nh. Robert Bosch - người s&aacute;ng lập ra thương hiệu Bosch đ&atilde; từng n&oacute;i rằng: &ldquo;T&ocirc;i th&agrave; mất tiền c&ograve;n hơn l&agrave; mất sự tin tưởng. Danh dự, niềm tin v&agrave;o gi&aacute; trị của sản phẩm lu&ocirc;n được ưu ti&ecirc;n hơn những lợi nhuận tạm thời&rdquo;. Nguy&ecirc;n tắc đ&oacute; vẫn l&agrave; nền tảng m&agrave; Bosch theo đuổi để ph&aacute;t triển thương hiệu v&agrave; cho ra đời c&aacute;c sản phẩm chất lượng cao ng&agrave;y nay.</p>\r\n\r\n<p>C&aacute;c thiết bị đồ gia dụng Bosch với c&ocirc;ng năng mạnh mẽ c&oacute; thể gi&uacute;p cuộc sống h&agrave;ng ng&agrave;y của ch&uacute;ng ta trở n&ecirc;n dễ d&agrave;ng hơn. Y&ecirc;u cầu về thiết bị gia dụng ng&agrave;y c&agrave;ng cao, kh&ocirc;ng chỉ đơn giản tủ lạnh chỉ để l&agrave;m lạnh, l&ograve; nướng chỉ để nướng thức ăn v&agrave; m&aacute;y giặt để giặt tẩy c&aacute;c vết bẩn tr&ecirc;n quần &aacute;o. N&oacute; c&ograve;n l&agrave; c&aacute;c y&ecirc;u cầu về thiết kế, vật liệu, v&agrave; cả khả năng tiết kiệm năng lượng.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; c&aacute;c yếu tố đều c&oacute; trong c&aacute;c sản phẩm đồ gia dụng Bosch, với mục ti&ecirc;u lu&ocirc;n cập nhật v&agrave; đ&aacute;p ứng c&aacute;c nhu cầu của người ti&ecirc;u d&ugrave;ng. . Tại c&aacute;c trung t&acirc;m nghi&ecirc;n cứu trải nghiệm kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n cầu, người ti&ecirc;u d&ugrave;ng sẽ thử nghiệm c&aacute;c sản phẩm mới, c&ugrave;ng cho &yacute; kiến về c&aacute;c &yacute; tưởng ph&aacute;t triển của Bosch.</p>\r\n\r\n<p>Trong qu&aacute; tr&igrave;nh ph&aacute;t triển sản phẩm, mỗi chức năng đều được kiểm tra kĩ c&agrave;ng do đ&oacute; lu&ocirc;n đ&aacute;p ứng cao nhất nhu cầu của người d&ugrave;ng.</p>', 0, NULL, '2018-08-08 08:15:37', '2018-08-08 08:15:37', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (5, 'Đồ gia dụng Bosch giúp cuộc sống trở nên dễ dàng hơn', 'do-gia-dung-bosch-giup-cuoc-song-tro-nen-de-dang-hon', 'Trong xã hội hiện nay, mỗi người đều phải chung tay giải quyết các công việc nhà.', '/images/post/1533716557.httpchannelvcmediavnprupload270201807img201807301525355347-1533032105970333993230.jpg', '<p>Bosch đại diện cho sự tin cậy, t&iacute;nh bền vững v&agrave; c&ocirc;ng nghệ kỹ thuật cao. Kh&ocirc;ng đặt mục ti&ecirc;u về lợi nhuận như c&aacute;c tập đo&agrave;n kinh tế kh&aacute;c, Bosch lu&ocirc;n đặt c&aacute;c ti&ecirc;u ch&iacute; về độ bền v&agrave; c&ocirc;ng nghệ cao l&ecirc;n h&agrave;ng đầu trong c&aacute;c sản phẩm của m&igrave;nh. Robert Bosch - người s&aacute;ng lập ra thương hiệu Bosch đ&atilde; từng n&oacute;i rằng: &ldquo;T&ocirc;i th&agrave; mất tiền c&ograve;n hơn l&agrave; mất sự tin tưởng. Danh dự, niềm tin v&agrave;o gi&aacute; trị của sản phẩm lu&ocirc;n được ưu ti&ecirc;n hơn những lợi nhuận tạm thời&rdquo;. Nguy&ecirc;n tắc đ&oacute; vẫn l&agrave; nền tảng m&agrave; Bosch theo đuổi để ph&aacute;t triển thương hiệu v&agrave; cho ra đời c&aacute;c sản phẩm chất lượng cao ng&agrave;y nay.</p>\r\n\r\n<p>C&aacute;c thiết bị đồ gia dụng Bosch với c&ocirc;ng năng mạnh mẽ c&oacute; thể gi&uacute;p cuộc sống h&agrave;ng ng&agrave;y của ch&uacute;ng ta trở n&ecirc;n dễ d&agrave;ng hơn. Y&ecirc;u cầu về thiết bị gia dụng ng&agrave;y c&agrave;ng cao, kh&ocirc;ng chỉ đơn giản tủ lạnh chỉ để l&agrave;m lạnh, l&ograve; nướng chỉ để nướng thức ăn v&agrave; m&aacute;y giặt để giặt tẩy c&aacute;c vết bẩn tr&ecirc;n quần &aacute;o. N&oacute; c&ograve;n l&agrave; c&aacute;c y&ecirc;u cầu về thiết kế, vật liệu, v&agrave; cả khả năng tiết kiệm năng lượng.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; c&aacute;c yếu tố đều c&oacute; trong c&aacute;c sản phẩm đồ gia dụng Bosch, với mục ti&ecirc;u lu&ocirc;n cập nhật v&agrave; đ&aacute;p ứng c&aacute;c nhu cầu của người ti&ecirc;u d&ugrave;ng. . Tại c&aacute;c trung t&acirc;m nghi&ecirc;n cứu trải nghiệm kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n cầu, người ti&ecirc;u d&ugrave;ng sẽ thử nghiệm c&aacute;c sản phẩm mới, c&ugrave;ng cho &yacute; kiến về c&aacute;c &yacute; tưởng ph&aacute;t triển của Bosch.</p>\r\n\r\n<p>Trong qu&aacute; tr&igrave;nh ph&aacute;t triển sản phẩm, mỗi chức năng đều được kiểm tra kĩ c&agrave;ng do đ&oacute; lu&ocirc;n đ&aacute;p ứng cao nhất nhu cầu của người d&ugrave;ng.</p>', 1, NULL, '2018-08-08 08:22:37', '2018-08-08 08:22:37', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (6, 'Chọn mua máy hút mùi tốt cho bếp gia đình', 'chon-mua-may-hut-mui-tot-cho-bep-gia-dinh', 'Máy hút mùi là thiết bị không thể thiếu trong căn bếp hiện đại. Vậy máy hút mùi loại nào tốt? Thương hiệu máy hút mùi nào nên mua? Hãy tham khảo kinh nghiệm mua máy hút mùi.', '/images/post/1534319682.5.jpg', '<p>Chức năng m&aacute;y h&uacute;t m&ugrave;i</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chức năng ch&iacute;nh của m&aacute;y h&uacute;t m&ugrave;i l&agrave; h&uacute;t m&ugrave;i thức ăn sinh ra trong qu&aacute; tr&igrave;nh nấu nướng. Một chiếc m&aacute;y h&uacute;t m&ugrave;i tối sẽ h&uacute;t m&ugrave;i v&agrave; cả kh&oacute;i (khi nấu ăn bằng gas) ở khu vực bếp. Việc nấu nướng c&oacute; thể sinh ra nhiều loại kh&iacute; độc hại như CO, CO2.</p>\r\n\r\n<p>Với những căn bếp c&oacute; kh&ocirc;ng gian chật hẹp, c&aacute;c kh&iacute; sinh ra trong qu&aacute; tr&igrave;nh nấu ăn (với gas) trở n&ecirc;n nguy hiểm. H&iacute;t c&aacute;c loại kh&iacute; độc n&agrave;y với nồng độ cao v&agrave; li&ecirc;n tục sẽ nguy hại cho hệ h&ocirc; hấp cũng như sức khỏe của bạn v&agrave; cả gia đ&igrave;nh.<br />\r\n<br />\r\nVề cơ bản, một chiếc m&aacute;y h&uacute;t m&ugrave;i th&ocirc;ng thường kh&ocirc;ng phải l&agrave; hệ thống lọc kh&ocirc;ng kh&iacute; ho&agrave;n hảo. Chức năng ch&iacute;nh của n&oacute; l&agrave; h&uacute;t kh&ocirc;ng kh&iacute; v&agrave; c&aacute;c loại kh&iacute; độc xung quanh khu vực bếp v&agrave; thải ra m&ocirc;i trường b&ecirc;n ngo&agrave;i. Với c&aacute;c loại m&aacute;y h&uacute;t m&ugrave;i cao cấp, n&oacute; c&oacute; thể c&oacute; th&ecirc;m chức năng cảm biến hoạt động tự động (tự động điều chỉnh c&ocirc;ng suất hoạt động), chức năng hẹn giờ, chức năng cảnh b&aacute;o phin lọc mỡ cần l&agrave;m sạch hoặc thậm ch&iacute; c&oacute; thể t&aacute;i tạo oxy cho kh&ocirc;ng gian bếp nh&agrave; bạn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chọn mua m&aacute;y h&uacute;t m&ugrave;i theo chức năng sử dụng<br />\r\n<br />\r\nMột chiếc m&aacute;y h&uacute;t m&ugrave;i cơ bản gồm 8 bộ phận: động cơ, lưới lọc mỡ, than hoạt t&iacute;nh, th&acirc;n m&aacute;y v&agrave; toa, k&iacute;nh gom, phễu h&uacute;t, bảng điều khiển v&agrave; b&oacute;ng đ&egrave;n. Ph&acirc;n loại theo cấu tạo, m&aacute;y h&uacute;t m&ugrave;i c&oacute; những loại phổ biến dưới đ&acirc;y.<br />\r\n<br />\r\nM&aacute;y h&uacute;t m&ugrave;i cổ điển (Classical Hood):&nbsp;L&agrave; loại m&aacute;y h&uacute;t m&ugrave;i đầu ti&ecirc;n v&agrave; c&oacute; lịch sử l&acirc;u đời nhất tr&ecirc;n thế giới với c&aacute;c chức năng cơ bản, đơn giản. Loại n&agrave;y thường c&oacute; t&iacute;nh năng lọc m&ugrave;i với lớp Carbon hoạt t&iacute;nh v&agrave; sau một thời gian sẽ cần thay lưới lọc. Với loại sử dụng chức năng h&uacute;t đẩy để thải kh&iacute; ra m&ocirc;i trường b&ecirc;n ngo&agrave;i th&igrave; sẽ kh&ocirc;ng cần thay lưới lọc.</p>\r\n\r\n<p><br />\r\nKhung gi&aacute;: 2 - 4 triệu đồng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&aacute;y h&uacute;t m&ugrave;i gắn tường (Chimney Hood): L&agrave; loại m&aacute;y được gắn trực tiếp l&ecirc;n tường, sử dụng cơ chế h&uacute;t &ndash; đẩy qua ống dẫn kh&iacute; để loại bỏ m&ugrave;i v&agrave; kh&oacute;i trong qu&aacute; tr&igrave;nh nấu nướng từ trong bếp ra ngo&agrave;i m&ocirc;i trường. Một số dạng thiết kế đặc biệt của m&aacute;y h&uacute;t m&ugrave;i gắn tường gồm c&oacute;: dạng n&oacute;n, k&iacute;nh cong, k&iacute;nh phẳng, h&igrave;nh v&aacute;t, chữ L, chữ T...</p>\r\n\r\n<p><br />\r\nKhung gi&aacute;: 5 &ndash; 30 triệu đồng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&aacute;y h&uacute;t m&ugrave;i &acirc;m tủ (Built Hood):&nbsp;L&agrave; loại m&aacute;y được lắp đặt nằm s&acirc;u trong tủ bếp gi&uacute;p tiết kiệm kh&ocirc;ng gian. Một số h&igrave;nh thức m&aacute;y h&uacute;t m&ugrave;i &acirc;m tủ phổ biến như loại m&aacute;y h&uacute;t m&ugrave;i k&eacute;o l&ecirc;n/xuống, k&eacute;o ra/v&agrave;o, dạng c&aacute;nh mở hay dạng ch&igrave;m ho&agrave;n to&agrave;n trong tủ bếp...</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Khung gi&aacute;: 4 &ndash; 20 triệu đồng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&aacute;y h&uacute;t m&ugrave;i đảo (Island Hood):&nbsp;L&agrave; loại m&aacute;y h&uacute;t m&ugrave;i đứng một m&igrave;nh, kh&ocirc;ng phụ thuộc v&agrave;o tường hay tủ bếp.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Khung gi&aacute;: 15 &ndash; 80 triệu đồng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&aacute;y h&uacute;t m&ugrave;i &aacute;p trần (Ceiling Hood):&nbsp;L&agrave; loại m&aacute;y h&uacute;t m&ugrave;i được &aacute;p s&aacute;t trần nh&agrave;. Loại n&agrave;y trong c&aacute;c nh&agrave; h&agrave;ng (lẩu, nướng) sử dụng kh&aacute; nhiều, tạo th&agrave;nh một hệ thống h&uacute;t kh&iacute; từ c&aacute;c bếp nướng rồi thải kh&iacute; v&agrave;o hệ thống kh&iacute; thải chung của c&aacute;c t&ograve;a nh&agrave;, trung t&acirc;m thương mại.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Khung gi&aacute;: 5 &ndash; 15 triệu​ đồng.</p>\r\n\r\n<p>N&ecirc;n mua m&aacute;y h&uacute;t m&ugrave;i nhập từ nước n&agrave;o?<br />\r\n<br />\r\nM&aacute;y h&uacute;t m&ugrave;i được sản xuất bởi rất nhiều c&aacute;c thương hiệu chuy&ecirc;n về thiết bị điện gia dụng. Một số nước nổi tiếng trong d&ograve;ng sản phẩm n&agrave;y như:<br />\r\n<br />\r\nĐức: C&aacute;c thương hiệu nổi tiếng của Đức c&oacute; Bosch, Munchen...</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nhật Bản: nổi tiếng với c&aacute;c nh&atilde;n hiệu m&aacute;y h&uacute;t m&ugrave;i như Toji, Panasonic, Rinnai, Miskio, Sakura hay Jiko.<br />\r\n<br />\r\n&Yacute;: L&agrave; nước sản xuất m&aacute;y h&uacute;t m&ugrave;i với quy m&ocirc; lớn v&agrave; đầu ti&ecirc;n tr&ecirc;n to&agrave;n thế giới. Những nh&atilde;n hiệu &Yacute; được người ti&ecirc;u d&ugrave;ng rất ưa chuộng l&agrave; Giovani, Napoli, Ariston, Cosmos, Batani hay Faber.<br />\r\n<br />\r\nT&acirc;y Ban Nha: Một nh&atilde;n hiệu m&aacute;y h&uacute;t m&ugrave;i T&acirc;y Ban Nha kh&aacute; phổ biến tr&ecirc;n thị trường Việt Nam hiện nay l&agrave; Malloca. Ngo&agrave;i ra, một số thương hiệu như: Fagor, Teka, Cata hay Benza cũng rất chất lượng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i c&aacute;c d&ograve;ng m&aacute;y xuất xứ Ch&acirc;u &Acirc;u, Nhật Bản như tr&ecirc;n th&igrave; tr&ecirc;n thị trường hiện nay c&oacute; nhiều d&ograve;ng m&aacute;y h&uacute;t m&ugrave;i xuất xứ từ Trung Quốc, Việt Nam với gi&aacute; th&agrave;nh rẻ hơn h&agrave;ng nhập khẩu Ch&acirc;u &Acirc;u.</p>', 1, NULL, '2018-08-15 07:54:42', '2018-08-15 07:54:42', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (7, 'Mách bạn 7 kinh nghiệm chọn bếp gas âm tốt nhất', 'mach-ban-7-kinh-nghiem-chon-bep-gas-am-tot-nhat', 'Bếp gas là vật dụng quen thuộc không thể thiếu trong gian bếp của mỗi gia đình. Ngày nay, bếp gas âm được các gia đình ưu tiên lựa chọn vì thiết kế đẹp, kiểu dáng sang trọng giúp không gian bếp trở nên rộng rãi, thoáng đãng hơn.', '/images/post/1534319800.4.jpg', '<p><strong>Kinh nghiệm chọn bếp gas &acirc;m</strong>&nbsp;tốt nhất th&igrave; kh&ocirc;ng phải ai cũng biết, v&igrave; vậy h&atilde;y tham khảo b&agrave;i viết dưới đ&acirc;y của ch&uacute;ng t&ocirc;i nh&eacute;. Bạn đọc muốn t&igrave;m hiểu th&ocirc;ng tin l&agrave;m sao để chọn được chiếc tủ lạnh đ&uacute;ng &yacute; th&igrave;<em><strong><a href=\"http://www.doisongphapluat.com/kinh-doanh/tu-van-tieu-dung/cach-chon-tu-lanh-tot-nhat-tiet-kiem-dien-nhat-a94815.html\">&nbsp;c&aacute;ch chọn tủ lạnh tốt nhất</a></strong></em>, bền đẹp nhất sẽ l&agrave; những mẹo hay cho quyết định của m&igrave;nh đấy.&nbsp;Sau đ&acirc;y l&agrave; những kinh nghiệm chọn bếp gas &acirc;m bạn cần lưu &yacute; những điểm sau:</p>\r\n\r\n<p><strong>1. Về chọn m&acirc;m bếp</strong></p>\r\n\r\n<p><strong>Kinh nghiệm chọn bếp gas &acirc;m</strong>&nbsp;đầu ti&ecirc;n m&agrave; nhiều người thường bỏ qua l&agrave; chọn m&acirc;m bếp. Bạn n&ecirc;n lưu &yacute; về m&acirc;m chia lửa của bếp gas v&igrave; đ&acirc;y l&agrave; phần quan trọng nhất của một chiếc bếp gas.</p>\r\n\r\n<p>Chất liệu l&agrave;m n&ecirc;n m&acirc;m v&agrave; v&ograve;ng chia lửa quyết định chất lượng khi sử dụng bếp,&nbsp;<strong>kinh nghiệm chọn bếp gas &acirc;m</strong>&nbsp;tốt nhất l&agrave; bạn n&ecirc;n chọn loại m&acirc;m được l&agrave;m từ hợp kim đồng hoặc somi hoặc sabaf để hiện tượng cong v&ecirc;nh m&acirc;m bếp, rạn nứt kh&oacute; c&oacute; thể xảy ra v&agrave; đảm bảo được ngọn lửa khi đun được đều v&agrave; xanh gi&uacute;p tiết kiệm gas.</p>\r\n\r\n<p><strong>2. Về số lượng bếp</strong></p>\r\n\r\n<p>Bếp gas &acirc;m thường được thiết kế c&oacute; từ 1-4 bếp đun, nếu gia đ&igrave;nh bạn kh&ocirc;ng đ&ocirc;ng người chỉ n&ecirc;n chọn loại bếp c&oacute; 2-3 bếp đun sẽ gi&uacute;p bạn sử dụng được hết tất cả c&aacute;c bếp đun trong qu&aacute; tr&igrave;nh nấu nướng, gi&uacute;p tiết kiệm thời gian.</p>\r\n\r\n<p>Nếu gia đ&igrave;nh đ&ocirc;ng người mua bếp c&oacute; từ 3 bếp đun trở l&ecirc;n, bạn n&ecirc;n lưu &yacute; khoảng c&aacute;ch giữa c&aacute;c bếp sao cho ph&ugrave; hợp để khi sử dụng kh&ocirc;ng vướng v&iacute;u, kh&oacute; đặt nồi l&ecirc;n bếp.</p>\r\n\r\n<p><strong>3. Về hệ thống đ&aacute;nh lửa</strong></p>\r\n\r\n<p>Hệ thống đ&aacute;nh lửa của bếp gas &acirc;m c&oacute; 2 loại l&agrave; d&ugrave;ng điện v&agrave; d&ugrave;ng pin. Bạn n&ecirc;n chọn bếp gas &acirc;m c&oacute; hệ thống đ&aacute;nh lửa IC tạo tia ch&aacute;y nhanh, giảm thiểu tối đa lượng gas thất tho&aacute;t khi sử dụng v&agrave; c&oacute; thể dễ d&agrave;ng thay thế pin khi hết.</p>\r\n\r\n<p><strong>4. Về xuất xứ bếp gas &acirc;m</strong></p>\r\n\r\n<p>C&oacute; rất nhiều c&aacute;c thương hiệu, c&aacute;c nh&agrave; sản xuất bếp gas &acirc;m tr&ecirc;n thị trường n&ecirc;n<strong>&nbsp;kinh nghiệm chọn bếp gas &acirc;m</strong>&nbsp;l&agrave; bạn n&ecirc;n chọn mua bếp từ c&aacute;c thương hiệu uy t&iacute;n, đảm bảo chất lượng v&agrave; gi&aacute; cả phải chăng của c&aacute;c thương hiệu như: Rinnai, Electrolux, Sunhouse,&hellip;</p>\r\n\r\n<p><strong>5. Về t&iacute;nh năng tự ngắt gas</strong></p>\r\n\r\n<p>Để bảo bảo an to&agrave;n cho người sử dụng v&agrave; tăng độ bền của sản phẩm, kinh nghiệm chọn bếp gas &acirc;m l&agrave; bạn phải lưu &yacute; đến t&iacute;nh năng tự ngắt gas khi c&oacute; gi&oacute; thổi l&agrave;m tắt lửa hoặc khi c&oacute; thức ăn, chất lỏng rơi xuống mặt bếp.</p>\r\n\r\n<p><strong>6. Về k&iacute;ch thước bếp gas &acirc;m</strong></p>\r\n\r\n<p>Bếp gas &acirc;m được thiết kế &acirc;m tường, n&ecirc;n nếu mua bếp cho nh&agrave; mới bạn n&ecirc;n xem x&eacute;t về k&iacute;ch thước của bếp gas &acirc;m rồi y&ecirc;u cầu thợ thiết kế bếp kho&eacute;t b&agrave;n vừa đủ rộng với k&iacute;ch thước của bếp.</p>\r\n\r\n<p>Nếu l&agrave; thay thế bếp cũ th&igrave; bạn n&ecirc;n mua chiếc bếp c&oacute; k&iacute;ch thước giống với bếp gas &acirc;m cũ để kh&ocirc;ng phải thay đổi, sửa chữa lại nh&agrave; bếp.</p>\r\n\r\n<p><strong>7. Về m&agrave;u sắc, kiểu d&aacute;ng</strong></p>\r\n\r\n<p>Bếp gas &acirc;m c&oacute; c&aacute;c m&agrave;u sắc như: đen, đỏ, trắng, xanh,.. C&aacute;c bạn n&ecirc;n chọn bếp gas &acirc;m c&oacute; m&agrave;u đen v&igrave; đ&acirc;y l&agrave; m&agrave;u th&ocirc;ng dụng nhất, n&oacute; cũng gi&uacute;p gian bếp nh&agrave; bạn tr&ocirc;ng sang trọng, s&aacute;ng b&oacute;ng hơn v&agrave; dễ lau ch&ugrave;i sạch sẽ.</p>\r\n\r\n<p>Với những kinh nghiệm chọn bếp gas &acirc;m tốt nhất m&agrave; ch&uacute;ng t&ocirc;i chia sẻ tr&ecirc;n đ&acirc;y hy vọng gi&uacute;p bạn c&oacute; được sự lựa chọn một sản phẩm bếp gas &acirc;m chất lượng nhất cho gia đ&igrave;nh m&igrave;nh. Bạn đọc muốn c&oacute; những th&ocirc;ng tin hay để chọn nồi đun inox th&igrave;<strong>&nbsp;<a href=\"http://www.doisongphapluat.com/kinh-doanh/tu-van-tieu-dung/meo-chon-mua-noi-inox-tot-an-toan-cho-suc-khoe-a152729.html\">mẹo chọn mua nồi inox tốt&nbsp;</a></strong>an to&agrave;n cho sức khỏe của ch&uacute;ng t&ocirc;i thực hiện trước đ&oacute; sẽ l&agrave; những gợi &yacute; hay cho bạn đấy.</p>', 1, NULL, '2018-08-15 07:56:40', '2018-08-15 07:56:40', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (8, 'Mùa lạnh, thị trường điều hòa vẫn hot', 'mua-lanh-thi-truong-dieu-hoa-van-hot', 'Bước vào những tháng cuối năm, thị trường máy điều hòa vẫn giữ không khí sôi động. Theo khảo sát tại một vài siêu thị điện máy phía Bắc, lượng người mua các mặt hàng điều hòa nhiệt độ và sản phẩm ngành hàng điện lạnh có xu hướng tăng trở lại.', '/images/post/1534319942.6.jpg', '<h3>Hưởng nhiều ưu đ&atilde;i</h3>\r\n\r\n<p>Theo đ&aacute;nh gi&aacute; của c&aacute;c trung t&acirc;m điện m&aacute;y v&agrave; chủ cửa h&agrave;ng, đ&acirc;y l&agrave; l&uacute;c th&iacute;ch hợp để chọn mua một chiếc điều h&ograve;a v&igrave; nhu cầu thị trường kh&ocirc;ng cao, nhiều cửa h&agrave;ng chững vốn n&ecirc;n tung ra nhiều g&oacute;i khuyến m&atilde;i, giảm gi&aacute; để k&iacute;ch cầu.</p>\r\n\r\n<p>Tr&aacute;i lại, v&agrave;o m&ugrave;a n&oacute;ng cao điểm, c&aacute;c mặt h&agrave;ng đều tăng gi&aacute; từ 500.000 đến 2 triệu đồng. V&igrave; vậy, khi mua điều h&ograve;a v&agrave;o m&ugrave;a lạnh, người d&ugrave;ng c&oacute; thể tiết kiệm chi ph&iacute; 7-30%. Gi&aacute; cả của m&aacute;y điều h&ograve;a &iacute;t biến động hơn, đem đến lựa chọn đa dạng cho người ti&ecirc;u d&ugrave;ng. Chỉ với mức gi&aacute; 6-8 triệu đồng, c&aacute;c hộ gia đ&igrave;nh c&oacute; thể sở hữu nhiều d&ograve;ng m&aacute;y uy t&iacute;n.</p>\r\n\r\n<h3>Sưởi ấm hiệu quả an to&agrave;n</h3>\r\n\r\n<p>C&aacute;c loại đ&egrave;n sưởi, quạt sưởi chỉ hữu dụng trong m&ugrave;a đ&ocirc;ng. B&ecirc;n cạnh đ&oacute;, điểm chung của c&aacute;c thiết bị n&agrave;y l&agrave; gi&aacute; rẻ nhưng tuổi thọ thấp, khi hỏng rất kh&oacute; sửa chữa. C&aacute;c loại m&aacute;y sưởi hoạt động tr&ecirc;n nguy&ecirc;n l&yacute; đốt n&oacute;ng kh&ocirc;ng kh&iacute; trực tiếp g&acirc;y thiếu oxy, l&agrave;m kh&ocirc; da v&agrave; k&eacute;m an to&agrave;n khi kh&ocirc;ng sử dụng đ&uacute;ng c&aacute;ch.</p>\r\n\r\n<p>M&aacute;y điều h&ograve;a 2 chiều dường như khắc phục được hầu hết c&aacute;c nhược điểm tr&ecirc;n nhờ nguy&ecirc;n l&yacute; l&agrave;m ấm (m&aacute;t) qua d&agrave;n trao đổi nhiệt n&ecirc;n kh&ocirc;ng kh&iacute; tỏa đều khắp ph&ograve;ng, kh&ocirc;ng g&acirc;y n&oacute;ng đột ngột, hại da hoặc nguy cơ ch&aacute;y, bỏng.</p>\r\n\r\n<h3>Dịch vụ đa dạng, chu đ&aacute;o</h3>\r\n\r\n<p>Đến c&aacute;c cửa h&agrave;ng điện m&aacute;y, si&ecirc;u thị để sắm m&aacute;y điều h&ograve;a trong m&ugrave;a lạnh, người d&ugrave;ng được tư vấn v&agrave; hưởng nhiều dịch vụ hơn. M&ugrave;a h&egrave;, người d&ugrave;ng kh&ocirc;ng thể y&ecirc;u cầu lắp đặt thiết bị bất cứ l&uacute;c n&agrave;o, thậm ch&iacute; phải chờ đợi v&agrave;i ng&agrave;y đến nửa th&aacute;ng.</p>\r\n\r\n<p>C&oacute; trường hợp, c&aacute;c gia đ&igrave;nh phải chấp nhận thi c&ocirc;ng đ&ecirc;m do đại l&yacute;, cửa h&agrave;ng kh&ocirc;ng đủ thợ. Chưa kể đến rủi ro về thi c&ocirc;ng ẩu, qu&ecirc;n dụng cụ, trục trặc kh&ocirc;ng được đ&aacute;p ứng, sửa chữa.</p>\r\n\r\n<h3>C&ocirc;ng nghệ t&acirc;n tiến t&iacute;ch hợp</h3>\r\n\r\n<p>Mức độ &ocirc; nhiễm của bầu kh&ocirc;ng kh&iacute; l&agrave; một trong những động lực th&uacute;c đẩy nh&agrave; cung cấp giới thiệu c&aacute;c c&ocirc;ng nghệ lọc sạch kh&ocirc;ng kh&iacute; t&iacute;ch hợp c&ugrave;ng m&aacute;y điều h&ograve;a, tiết kiệm khoản tiền đầu tư v&agrave;o một thiết bị kh&aacute;c.</p>\r\n\r\n<p>Một trong những h&atilde;ng đạt đến tiến bộ trong c&ocirc;ng nghệ lọc kh&ocirc;ng kh&iacute; l&agrave; Panasonic. T&iacute;ch hợp c&ocirc;ng nghệ nanoe-G gi&uacute;p loại bỏ v&agrave; v&ocirc; hiệu h&oacute;a tới 99% vi khuẩn, virus trong kh&ocirc;ng kh&iacute; gi&uacute;p kh&ocirc;ng gian sống lu&ocirc;n trong l&agrave;nh. Đặc biệt, chế độ lọc kh&ocirc;ng kh&iacute; nanoe-G c&oacute; khả năng hoạt động độc lập như một m&aacute;y lọc kh&ocirc;ng kh&iacute; trong ph&ograve;ng.</p>\r\n\r\n<p>Ngo&agrave;i ra, d&ograve;ng m&aacute;y t&acirc;n tiến nhất của Panasonic- SKY Series c&ograve;n được trang bị th&ecirc;m cảm biến bụi Dust Sensor. Cảm biến n&agrave;y tự động k&iacute;ch hoạt chế độ nanoe-G ngay khi đo được h&agrave;m lượng bụi bẩn trong kh&ocirc;ng kh&iacute; ở mức cao, đảm bảo bầu kh&ocirc;ng kh&iacute; lu&ocirc;n được duy tr&igrave; ở mức an to&agrave;n, trong l&agrave;nh.</p>', 1, NULL, '2018-08-15 07:59:02', '2018-08-15 07:59:02', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` VALUES (9, 'Xiaomi ra mắt robot hút bụi Mijia Roborock S50: nhiều tính năng thông minh nhưng giá cao', 'xiaomi-ra-mat-robot-hut-bui-mijia-roborock-s50-nhieu-tinh-nang-thong-minh-nhung-gia-cao', 'Nếu cảm thấy chán việc lau dọn nhà? Hãy để robot thông minh Mijia Roborock S50 của Xiaomi đảm nhiệm công việc này thay bạn.', '/images/post/1534320516.1.jpg', '<p>Tiếp tục loạt sản phẩm gia dụng, với mới đ&acirc;y h&atilde;ng c&ocirc;ng nghệ Xiaomi đ&atilde; ra mắt một ch&uacute; robot h&uacute;t bụi th&ocirc;ng minh ho&agrave;n to&agrave;n mới mang t&ecirc;n&nbsp;Mijia Roborock S50. N&oacute; c&oacute; thể qu&eacute;t nh&agrave;, sau đ&oacute; lau bằng nước, v&agrave; c&oacute; thể điều khiển hướng đi bằng phần mềm h&atilde;ng ph&aacute;t triển.</p>\r\n\r\n<p>Tr&ecirc;n th&acirc;n robot c&oacute; nhiều cảm biến, gi&uacute;p n&oacute; &#39;nh&igrave;n&#39; kh&ocirc;ng gian xung quanh tới 1800 lần một gi&acirc;y, gi&uacute;p điều chỉnh hướng đi ph&ugrave; hợp để kh&ocirc;ng động v&agrave;o đồ đạc trong nh&agrave;. Giống như c&aacute;c robot h&uacute;t bụi kh&aacute;c,&nbsp;Mijia Roborock S50 c&oacute; một số chế độ đặt sẵn như dọn h&igrave;nh tr&ograve;n, dọn to&agrave;n ph&ograve;ng hay dọn c&aacute;c khe kẽ nhỏ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mijia Roborock S50 được trang bị pin&nbsp;Li-ion 5200mAh gi&uacute;p cho thời gian hoạt động 2,5 giờ, tương đương với việc dọn một căn nh&agrave; 250 m&eacute;t vu&ocirc;ng. Phần dọn c&oacute; lực h&uacute;t&nbsp;2000Pa, gi&uacute;p h&uacute;t được cả những bụi bẩn lớn, thậm ch&iacute; cả những hạt l&agrave;m bằng kim loại, hạt hoa quả.</p>\r\n\r\n<p><em><strong>Th&ocirc;ng số kĩ thuật</strong></em></p>\r\n\r\n<p>- Dung t&iacute;ch hộp chứa bụi: 0.48L</p>\r\n\r\n<p>- Dung t&iacute;ch hộp chứa nước lau nh&agrave;: 0.14L</p>\r\n\r\n<p>- Lực h&uacute;t:&nbsp;2000Pa</p>\r\n\r\n<p>- C&ocirc;ng suất: 58W</p>\r\n\r\n<p>- D&ograve;ng&nbsp;100 &ndash; 240V</p>\r\n\r\n<p>- Pin 5200mAh</p>\r\n\r\n<p>- Thời lượng sử dụng 2.5 giờ</p>\r\n\r\n<p><em><strong>Sản phẩm hiện đang được b&aacute;n tại website ch&iacute;nh thức của h&atilde;ng, hoặc qua Coolicool với gi&aacute; $445 (10 triệu đồng).</strong></em></p>', 1, NULL, '2018-08-15 08:08:36', '2018-08-15 08:08:36', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for producer
-- ----------------------------
DROP TABLE IF EXISTS `producer`;
CREATE TABLE `producer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of producer
-- ----------------------------
INSERT INTO `producer` VALUES (4, 'Sunhouse Vietnam', 1, '2018-08-09 03:34:16', '2018-08-13 08:22:22', 'Công ty TNHH SUNHOUSE Việt Nam', '/images/producer/1533785656.tải xuống.png', 0, '0');
INSERT INTO `producer` VALUES (6, 'Bluestone', 1, '2018-08-15 04:44:43', '2018-08-15 04:44:43', 'BlueStone là thương hiệu điện gia dụng cao cấp được tập đoàn Great American Appliance Corporation (GAAC) tại Hoa Kỳ tư vấn và quản lý về quy trình kiểm soát chất lượng sản phẩm. Tại Việt Nam, sản phẩm BlueStone được độc quyền phân phối bởi Công ty Cổ Phần TARA.', '/images/producer/1534308283.tải xuống.jpg', 2, 'bluestone');
INSERT INTO `producer` VALUES (7, 'Sato', 1, '2018-08-15 04:56:10', '2018-08-15 04:56:10', 'Công ty TNHH Điện tử Việt Nhật được thành lập từ năm 2002, là tổ chức kinh tế có đủ tư cách pháp nhân, hạch toán độc lập. Phát huy thế mạnh của một tổ chức sản xuất đã tạo lập được vị trí xứng đáng trong thị trường sản xuất và kinh doanh sản phẩm Điện tử, điện dân dụng và thiết bị giáo dục trong nước.', '/images/producer/1534308970.tải xuống (1).jpg', 2, 'sato');
INSERT INTO `producer` VALUES (8, 'Bosch', 1, '2018-08-15 07:38:16', '2018-08-15 07:38:16', 'Lịch sử của tập đoàn Bosch là lịch sử của sự cách tân, đổi mới. Trong suốt những năm đầu của thế kỉ 20, Bosch đã tiếp tục đẩy mạnh việc kinh doanh thông qua việc phát triển những sản phẩm mới. Năm 1927, máy bơm tiêm diesel trong xe hơi được ra mắt và năm 1928 dụng cụ điện đầu tiên, máy tôngđơ cắt tóc bằng điện có tên Forfex, có mặt trên thị trường.', '/images/producer/1534318696.a.jpg', 2, 'bosch');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `thong_so` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `bao_hanh` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `thong_tin` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `price` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_sale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `title_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `primary_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sub_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `link_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (4, 'abc', 'abc', 'abc', NULL, '<p>abc</p>', '<p>abc</p>', '<p>abc</p>', '11111', '11111', 1, 2, 0, '2018-08-09 02:37:59', '2018-08-09 02:37:59', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (8, 'Nồi cơm đa năng', 'noi-com-da-nang', 'Nồi cơm điện đa năng Tiross (TS-991)', '/images/product/1534147104.4.jpg', '<ul>\r\n	<li><span style=\"font-size:small\"><span style=\"font-family:arial,helvetica,sans-serif\">Điện &aacute;p/c&ocirc;ng suất:&nbsp;&nbsp;&nbsp; 220V / 900W</span></span></li>\r\n	<li><span style=\"font-size:small\"><span style=\"font-family:arial,helvetica,sans-serif\">Dung t&iacute;ch:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1,8 l&iacute;t(tổng thế t&iacute;ch&nbsp;5 l&iacute;t)</span></span></li>\r\n	<li><span style=\"font-size:small\">M&agrave;u sắc:&nbsp;&nbsp;&nbsp;&nbsp; Đen</span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '<p>Bảo h&agrave;nh 06 th&aacute;ng</p>', '<p><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\"><strong>Xuất xứ:</strong>&nbsp;&nbsp;&nbsp;&nbsp;Tiross-Ba Lan&nbsp;sản xuất tại Trung Quốc&nbsp;đạt ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u</span></span></p>\r\n\r\n<p><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\"><strong>Đặc điểm:</strong></span></span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Đa năng: Nấu cơm, ch&aacute;o, s&uacute;p, hầm thịt, hầm xương....</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Dễ d&agrave;ng sử dụng với hệ thống điều khiển cơ, Tiện dụng với chương tr&igrave;nh nấu đ&atilde; được hẹn giờ sẵn v&agrave; c&oacute; thể điều chỉnh thời gian.</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Nấu rất nhanh gi&uacute;p tiết kiệm điện v&agrave; thời gian.</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Thiết kế bền, đẹp, an to&agrave;n cao với c&aacute;c thiết bị kiểm so&aacute;t &aacute;p suất v&agrave; nhiệt độ.</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Chức năng chống tr&agrave;n trong khi nấu, gi&uacute;p giữ nguy&ecirc;n chất dinh dưỡng v&agrave; hương vị của m&oacute;n ăn, c&oacute; lợi cho sức khỏe.</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">L&ograve;ng nồi bằng hợp kim nh&ocirc;m, tr&aacute;ng men chống d&iacute;nh, dễ d&agrave;ng vệ sinh.</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Chọn chế độ nấu dễ d&agrave;ng</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Nấu nhanh, tiết kiệm điện v&agrave; thời gian</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Giữ nguy&ecirc;n chất dinh dưỡng v&agrave; hương vị thức ăn</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Bộ vi mạch&nbsp;điều chỉnh&nbsp;nhiệt tự động gi&uacute;p tiết kiệm điện</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Chọn chế độ bằng n&uacute;m xoay</span></span></li>\r\n	<li><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:small\">Sản phẩm đạt ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u</span>.</span></li>\r\n</ul>', '5000000', '4750000', 11, 4, 1, '2018-08-13 07:58:24', '2018-08-15 07:42:07', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (9, 'Robot hút bụi lau nhà', 'robot-hut-bui-lau-nha', 'ROBOT HÚT BỤI LAU NHÀ QIHOO 360 WIFI VẼ SƠN ĐỒ SÀN NHÀ BẰNG LASER LDS', '/images/product/1534154515.14.jpg', '<ul>\r\n	<li>T&ecirc;n sản phẩm: QIHOO 360 S6</li>\r\n	<li>Dung lượng hộp: 0.6L</li>\r\n	<li>Chiều cao cao nhất: 9.5cm</li>\r\n	<li>Dung lượng pin: 3.200mAh</li>\r\n	<li>Lực h&uacute;t: 1800 Pa</li>\r\n</ul>', '<p>Bảo h&agrave;nh 1 năm</p>', '<ul>\r\n	<li>C&aacute;c t&iacute;nh năng kh&aacute;c : Cảm biến hồng ngoại, tiệm cận chống va chạm, cảm biến độ cao chống rơi cầu thang.</li>\r\n	<li>Tự t&igrave;m về dock sạc khi hết pin</li>\r\n	<li>Hẹn giờ l&agrave;m việc cho từng ng&agrave;y ri&ecirc;ng lẻ trong tuần</li>\r\n	<li>C&ocirc;ng nghệ Lidar SLAM vẽ sơ đồ s&agrave;n nh&agrave; v&agrave; d&ugrave;ng thuật to&aacute;n Slam để t&iacute;nh to&aacute;n qu&atilde;ng đường đi ngắn v&agrave; hiệu quả nhất để lau h&uacute;t (M&agrave;n h&igrave;nh điện thoại sẽ hiện l&ecirc;n những vị tr&iacute; n&agrave;o đ&atilde; đi qua, chỗ n&agrave;o chưa đi qua, số m2 đ&atilde; lau h&uacute;t được, x&aacute;c định được vị tr&iacute; sạc pin n&ecirc;n khi quay về dock sạc sẽ rất nhanh gọn)</li>\r\n	<li>Tạo tường ảo tr&ecirc;n app: Tr&ecirc;n m&agrave;n h&igrave;nh điện thoại c&oacute; thể tạo tường ảo ở những vị tr&iacute; trong ng&ocirc;i nh&agrave; kh&ocirc;ng muốn robot đi v&agrave;o</li>\r\n	<li>Diện t&iacute;ch ph&ugrave; hợp: 150-250m2</li>\r\n	<li>Độ ồn: Ti&ecirc;u chuẩn &lt;60db</li>\r\n	<li>M&agrave;u sắc: Trắng</li>\r\n</ul>', '8000000', '7500000', 13, 4, 1, '2018-08-13 10:01:55', '2018-08-15 07:42:17', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (10, 'Máy xay đa năng', 'may-xay-da-nang', 'Máy chế biến thực phẩm đa năng MCM4100 cho công việc nội trợ trở nên dễ dàng hơn.', '/images/product/1534317091.images.jpg', '<ul>\r\n	<li>Số lượng cối<a href=\"https://meta.vn/may-xay-da-nang-c443?specs=228.2930\" target=\"_blank\" title=\"Xem thêm sản phẩm Máy xay đa năng cùng Số lượng cối 3 cối\">3 cối</a></li>\r\n	<li>Thương hiệu<a href=\"https://meta.vn/may-xay-da-nang-c443?specs=69.1186\" target=\"_blank\" title=\"Xem thêm sản phẩm Máy xay đa năng cùng Thương hiệu Đức\">Đức</a></li>\r\n	<li>Sản xuất tại<a href=\"https://meta.vn/may-xay-da-nang-c443?specs=68.1143\" target=\"_blank\" title=\"Xem thêm sản phẩm Máy xay đa năng cùng Sản xuất tại Slovenia\">Slovenia</a></li>\r\n</ul>', '<p>Bảo h&agrave;nh :&nbsp;<a href=\"https://meta.vn/may-xay-da-nang-c443?specs=67.1017\" target=\"_blank\" title=\"Xem thêm sản phẩm Máy xay đa năng cùng Bảo hành 24 tháng\">24 th&aacute;ng</a></p>', '<ul>\r\n	<li>M&aacute;y chế biến thực phẩm đa năng MCM4100&nbsp;c&oacute; thiết kế gọn nhẹ, dễ d&agrave;ng th&aacute;o rời gi&uacute;p cho việc vệ sinh v&agrave; bảo quản thuận lợi hơn. Chốt an to&agrave;n, ch&acirc;n đế cao su chống trơn trượt đảm bảo an to&agrave;n cho người ti&ecirc;u d&ugrave;ng trong qu&aacute; tr&igrave;nh sử dụng.</li>\r\n	<li>M&aacute;y được t&iacute;ch hợp nhiều chức năng trong 1 sản phẩm: xay sinh tố, th&aacute;i sợi, vỡ hạt, vắt cam... rất tiện lợi. M&aacute;y sẽ gi&uacute;p người nội trợ tạo n&ecirc;n những m&oacute;n ăn ngon đ&uacute;ng điệu.</li>\r\n	<li>M&aacute;y hoạt động mạnh mẽ với c&ocirc;ng suất 800W gi&uacute;p bạn chế biến c&aacute;c nguy&ecirc;n vật liệu một c&aacute;ch nhanh ch&oacute;ng. Motor vận h&agrave;nh &ecirc;m &aacute;i, hạn chế tối đa tiếng ồn ph&aacute;t ra trong khi sử dụng.</li>\r\n	<li>M&aacute;y hoạt động với 2 tốc độ kh&aacute;c nhau, gi&uacute;p bạn dễ d&agrave;ng điều chỉnh tốc độ xay theo &yacute; muốn. Ch&ecirc;́ đ&ocirc;̣ trộn gi&uacute;p thức ăn được trộn đều trong khi xay cho hỗn hợp thật nhuyễn v&agrave; mịn.</li>\r\n	<li>Ngo&agrave;i chức năng xay rau củ, m&aacute;y xay đa năng Bosch MCM4100 c&ograve;n c&oacute; th&ecirc;m chức năng xay thịt đặc biệt gi&uacute;p người nội trợ dễ d&agrave;ng chế biến m&oacute;n ăn đơn giản, nhanh gọn hơn.</li>\r\n</ul>', '5400000', '4590000', 12, 6, 1, '2018-08-15 07:11:31', '2018-08-15 07:11:31', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (11, 'Nồi chiên rán không dầu', 'noi-chien-ran-khong-dau', 'Nồi chiên rán không dầu Steba HF5000 XL', '/images/product/1534317841.11111.jpg', '<p>C&ocirc;ng suất : 1800W</p>\r\n\r\n<p>K&iacute;ch thước : 37 x 34,5 x 42 cm</p>\r\n\r\n<p>Trọng lượng cả bao : 6,2kg</p>\r\n\r\n<p>&nbsp;</p>', '<p>Bảo h&agrave;nh 12 th&aacute;ng</p>', '<p>T&iacute;nh năng nổi bật:</p>\r\n\r\n<ul>\r\n	<li>Với nồi chi&ecirc;n kh&ocirc;ng dầu cho ph&eacute;p bạn chi&ecirc;n,nướng c&aacute;c m&oacute;n ăn cho bữa ăn ngon nhất với &iacute;t chất b&eacute;o hơn một nồi chi&ecirc;n th&ocirc;ng thường bằng c&aacute;ch sử dụng &iacute;t hoặc kh&ocirc;ng c&oacute; dầu.</li>\r\n	<li>Bộ hẹn giờ t&iacute;ch hợp của n&oacute; cho ph&eacute;p bạn c&agrave;i đặt trước thời gian nấu tối đa 60 ph&uacute;t.Điều khiển nhiệt độ ho&agrave;n to&agrave;n c&oacute; thể điều chỉnh cho ph&eacute;p bạn đặt trước nhiệt độ nấu ăn tốt nhất cho thực phẩm của bạn l&ecirc;n đến 200 độ</li>\r\n	<li>7 chương tr&igrave;nh c&agrave;i đặt sẵn gi&uacute;p bạn lựa chọn nấu nướng nhanh hơn.</li>\r\n	<li>Chức năng giữ ấm thức ăn sau nấu gi&uacute;p bạn thưởng thức c&aacute;c m&oacute;n ăn n&oacute;ng hổi.</li>\r\n</ul>', '6000000', '5000000', 9, 7, 1, '2018-08-15 07:24:01', '2018-08-15 07:24:01', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (12, 'Máy sưởi ấm panasonic', 'may-suoi-am-panasonic', 'Máy sưởi phòng tắm chức năng thông gió FV-27BV1', '/images/product/1534318152.2.jpg', '<ul>\r\n	<li>C&ocirc;ng suất :\r\n	<ul>\r\n		<li>Chế độ sưởi ấm<strong>&nbsp;: 1.130 W</strong></li>\r\n		<li>Chế độ th&ocirc;ng gi&oacute;<strong>&nbsp;: 21</strong>&nbsp;W</li>\r\n	</ul>\r\n	</li>\r\n	<li>Lưu lượng gi&oacute; :\r\n	<ul>\r\n		<li>Chế độ sưởi ấm :&nbsp;<strong>130</strong>&nbsp;m3/h</li>\r\n		<li>Chế độ th&ocirc;ng gi&oacute; :&nbsp;<strong>150</strong>&nbsp;m3/h</li>\r\n	</ul>\r\n	</li>\r\n	<li>K&iacute;ch thước chừa lỗ vu&ocirc;ng :&nbsp;<strong>27 x 27&nbsp;</strong>cm</li>\r\n	<li>K&iacute;ch thước ống dẫn :&nbsp;<strong>&Oslash;100</strong>&nbsp;mm</li>\r\n</ul>', '<p>Bảo h&agrave;nh 12 th&aacute;ng</p>', '<h3><strong>T&iacute;nh năng</strong></h3>\r\n\r\n<ul>\r\n	<li>Chức năng sưởi ấm v&agrave; th&ocirc;ng gi&oacute;</li>\r\n</ul>', '5990000', '5500000', 10, 5, 1, '2018-08-15 07:29:12', '2018-08-15 07:29:12', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (13, 'Bếp điện từ Bosch PIC645F17E', 'bep-dien-tu-bosch-pic645f17e', 'Bếp điện từ Bosch PIC645F17E', '/images/product/1534318914.3.jpg', '<p>Tổng c&ocirc;ng suất: 7200W&nbsp;<br />\r\nMặt k&iacute;nh: Schott Ceran&nbsp;<br />\r\nK&iacute;ch thước kho&eacute;t đ&aacute;: 560x490 mm<br />\r\nXuất xứ: T&acirc;y Ban Nha</p>', '<p>Bảo h&agrave;nh: 2 Năm</p>', '<ul>\r\n	<li>&nbsp;<strong>Bếp điện từ Bosch&nbsp;PIC645F17E</strong>&nbsp;moden hiếm hoi&nbsp;kết hợp bếp điện lẫn bếp từ của Bosch, nhập khẩu nguy&ecirc;n chiếc Đức&nbsp;được c&aacute;c b&agrave; nội trợ được ưa chuộng tr&ecirc;n thị trường hiện nay bởi những tiện &iacute;ch m&agrave; sản phẩm đem lại.</li>\r\n	<li><strong>Bếp điện từ&nbsp;PIC645F17E</strong>&nbsp;c&oacute; k&iacute;ch thước 60cm, được bao viền bằng th&eacute;p bo hai b&ecirc;n tinh tế chống chịu lực, va chạm bền bỉ, m&agrave;u sắc tinh tế tạo sự sang trọng trong kh&ocirc;ng gian bếp hiện đại.</li>\r\n	<li>Bốn v&ugrave;ng nấu của bếp điện từ Bosch&nbsp;với 2 từ v&agrave; 2 điện ( 1 v&ugrave;ng c&oacute; thể mở rộng)</li>\r\n	<li>1 v&ugrave;ng từ ph&iacute;a trước b&ecirc;n tr&aacute;i 14,5 cm - c&ocirc;ng suất 1,4 kW.</li>\r\n	<li>1 v&ugrave;ng điện ph&iacute;a sau b&ecirc;n tr&aacute;i 17/26,5 cm - c&ocirc;ng suất 1,6 / 2,4kW.</li>\r\n	<li>1 v&ugrave;ng điện ph&iacute;a sau b&ecirc;n phải 14,5 cm - c&ocirc;ng suất 1,2 kW.</li>\r\n	<li>1 v&ugrave;ng từ ph&iacute;a trước b&ecirc;n phải 21 cm - c&ocirc;ng suất 2,2 kW.</li>\r\n	<li>Tổng c&ocirc;ng suất của bếp l&agrave; 7,2kW với nguồn điện 220V.</li>\r\n	<li><strong>Bếp Bosch PIC645F17E&nbsp;</strong>c&oacute; bảng điều khiển dạng DirectSelect 1.0 (Timer) với m&agrave;n h&igrave;nh hiển thị kỹ thuật số si&ecirc;u&nbsp;n&eacute;t, Chức năng PowerBoost cho 2 v&ugrave;ng từ ph&iacute;a trước bếp,&nbsp;c&aacute;c v&ugrave;ng nấu điều khiển độc lập, 17 cấp độ nấu gi&uacute;p tăng giảm nhiệt chuẩn x&aacute;c nhất..</li>\r\n	<li><strong>Bếp PIC645F17E</strong>&nbsp;được trang bị chức năng kh&oacute;a th&ocirc;ng minh cho trẻ em, c&ocirc;ng tắc an to&agrave;n khi sử dụng, chức năng PowerManagement mang lại sự an to&agrave;n cho ng&ocirc;i nh&agrave; của bạn.</li>\r\n	<li>Mặt bếp l&agrave; k&iacute;nh Schott ceran c&oacute; khả năng chịu lực, chịu nhiệt rất cao, chịu được sốc nhiệt l&ecirc;n tới 700 độ C.</li>\r\n	<li>Chỉ số hiển thị đ&egrave;n cảnh b&aacute;o nhiệt dư 2 cấp độ (H/h).</li>\r\n	<li>Đ&aacute;y&nbsp;<strong>bếp Bosch PIC645F17E</strong>&nbsp;được thiết kế từ chất liệu nhựa c&oacute; khả năng chịu nhiệt tốt với c&aacute;c hệ thống tản nhiệt thiết kế th&ocirc;ng minh hiệu quả gi&uacute;p tăng th&ecirc;m tuổi thọ cho bếp.</li>\r\n	<li><strong>Bếp điện từ Bosch PIC645F17E</strong>&nbsp;được bảo h&agrave;nh 2 năm ch&iacute;nh h&atilde;ng Bosch tại nh&agrave; gi&uacute;p kh&aacute;ch h&agrave;ng an t&acirc;m sử dụng.</li>\r\n</ul>', '21700000', '18490000', 5, 8, 1, '2018-08-15 07:41:54', '2018-08-15 07:41:54', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (14, 'Bếp ga hồng ngoại', 'bep-ga-hong-ngoai', 'Bếp đôi hồng ngoại và từ Faber FB-IH 5200W', '/images/product/1534319274.4.jpg', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>M&agrave;u sắc</td>\r\n			<td>Đen</td>\r\n		</tr>\r\n		<tr>\r\n			<td>C&ocirc;ng suất</td>\r\n			<td>5200W</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Rộng</td>\r\n			<td>45cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>D&agrave;i</td>\r\n			<td>75cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cao</td>\r\n			<td>15cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Model</td>\r\n			<td>FB - 2I</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '<p>Bảo h&agrave;nh 12 th&aacute;ng</p>', '<p>- Faber FB-IH sở hữu 2 mặt bếp&nbsp;cho ph&eacute;p người d&ugrave;ng nấu nhiều m&oacute;n ăn c&ugrave;ng một l&uacute;c gi&uacute;p tiết kiệm thời gian cũng như c&ocirc;ng sức của người nội trợ. Với 1 bếp từ v&agrave; 1 bếp hồng ngoại, bạn ho&agrave;n to&agrave;n c&oacute; thể lựa chọn để ph&ugrave; hợp với c&aacute;c loại nồi cũng như nhu cầu sử dụng kh&aacute;c nhau. Bếp được thiết kế với bề mặt k&iacute;nh SCHOTT v&aacute;t cạnh&nbsp;cao cấp chịu lực, chịu nhiệt v&agrave; chống xước tốt, đồng thời chất liệu n&agrave;y cũng chống b&aacute;m bụi bẩn gi&uacute;p người d&ugrave;ng dễ d&agrave;ng vệ sinh, lau ch&ugrave;i sau mỗi lần sử dụng.<br />\r\n- Bếp tự động tắt khi kh&ocirc;ng sử dụng, đ&acirc;y l&agrave; một t&iacute;nh năng rất cần thiết d&agrave;nh cho những người nội &nbsp;trợ đặt biệt l&agrave; với những người hay qu&ecirc;n tắt bếp sau khi sử dụng nhằm tiết kiệm tối đa điện năng cũng như đảm bảo an to&agrave;n.&nbsp;<br />\r\n- Sản phẩm sử dụng bảng điều khiển cảm ứng, bạn chỉ cần chạm nhẹ l&agrave; đ&atilde; c&oacute; thể điều chỉnh mức nhiệt độ hay chức năng mong muốn. Đồng thời, bếp c&ograve;n được trang bị cảm biến nhiệt v&agrave; cảm biến chống tr&agrave;n đảm bảo an to&agrave;n cho người d&ugrave;ng khi sử dụng.<br />\r\n- Với c&ocirc;ng suất tối đa l&ecirc;n tới 5200W v&agrave; 2 mặt bếp, mọi bữa ăn của gia đ&igrave;nh bạn sẽ lu&ocirc;n được chuẩn bị trong một thời gian ngắn. Bộ định thời gian hẹn giờ đ&ecirc;́n 99 phút cho m&ocirc;̃i b&ecirc;́p gi&uacute;p bạn c&oacute; thể thoải m&aacute;i l&agrave;m những việc y&ecirc;u th&iacute;ch trong thời gian nấu m&agrave; kh&ocirc;ng cần tr&ocirc;ng bếp.</p>', '21600000', '18360000', 6, 4, 1, '2018-08-15 07:47:54', '2018-08-15 07:47:54', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (15, 'Máy hút mùi Bosch DWW09W851B', 'may-hut-mui-bosch-dww09w851b', 'Máy hút mùi Bosch DWW09W851B', '/images/product/1534319509.5.jpg', '<p>C&ocirc;ng suất h&uacute;t : 790 m3/h&nbsp;<br />\r\nĐộ ồn : 70 dB&nbsp;<br />\r\nK&iacute;ch thước: 900 mm</p>', '<p>Bảo h&agrave;nh: 2 năm</p>', '<p>&nbsp;&bull; Bộ lọc dầu mỡ bằng nh&ocirc;m được l&agrave;m sạch đơn giản m&agrave;&nbsp;hiệu quả với m&aacute;y rửa b&aacute;t.<br />\r\n&bull; Chức năng h&uacute;t kh&ocirc;ng tiếng ồn với hệ thống c&aacute;ch &acirc;m đặc&nbsp;biệt y&ecirc;n tĩnh<br />\r\n&bull; Hệ thống điều khiển bằng điện tử điều chỉnh dễ d&agrave;ng, nội&nbsp;thất m&aacute;y được bảo vệ an to&agrave;n v&agrave; dễ d&agrave;ng vệ sinh&bull; 3 tốc độ h&uacute;t&nbsp;<br />\r\n&bull; Đ&egrave;n Led chiếu s&aacute;ng 2 x 40W&nbsp;<br />\r\n&bull; Bộ lọc dầu mỡ bằng nh&ocirc;m được l&agrave;m sạch đơn giản m&agrave; hiệu quả với m&aacute;y rửa b&aacute;t.&nbsp;<br />\r\n&bull; Chức năng h&uacute;t kh&ocirc;ng tiếng ồn với hệ thống c&aacute;ch &acirc;m đặc biệt y&ecirc;n tĩnh&nbsp;<br />\r\n&bull; Hệ thống điều khiển bằng điện tử điều chỉnh dễ d&agrave;ng, nội thất m&aacute;y được bảo vệ an to&agrave;n v&agrave; dễ d&agrave;ng vệ sinh</p>', '14725000', '11780000', 3, 8, 1, '2018-08-15 07:51:49', '2018-08-15 07:51:49', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `title_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `primary_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sub_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `link_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sliders
-- ----------------------------
INSERT INTO `sliders` VALUES (11, 'Slider1', 'https://www.google.com/', '/images/slider/1534495078.giadung2.png', 1, '2018-08-08 10:13:28', '2018-08-17 08:37:58', 'bếp từ bosch', 'bếp từ bosch', 'bếp từ bosch', 'bếp từ bosch', 'bếp từ bosch');
INSERT INTO `sliders` VALUES (13, 'Slider3', 'https://www.google.com/', '/images/slider/1534494939.giadung.png', 1, '2018-08-13 09:24:20', '2018-08-17 08:35:39', 'Gia dụng châu âu', 'Gia dụng châu âu', 'Gia dụng châu âu', 'Gia dụng châu âu', 'Gia dụng châu âu');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Trần Duy Huy', 'huytd', 'backmylove.duyhuy@gmail.com', '$2y$10$sZ0DgpXLFpAsNJW.d8fde.dqJt/pWjoz3I.yLkTYBk.wqWhlKiM7e', 99, 'GTbR10ICpMEpVfljd7g1KeWj9Hpknt2ZHz5aB7MgyXGYmqe1J6GA43HEmhTo', '2018-08-01 08:30:11', '2018-08-01 08:30:11');
INSERT INTO `users` VALUES (3, 'Tuntin', 'Đặng Thu', 'dangthu.th1905@gmailcom', '$2y$10$lIWb//gOrWQsGnqBTgNP5uv1itM2tG2WmRps8ytvQv04NL377PCny', 0, NULL, '2018-08-08 04:44:28', '2018-08-08 04:44:28');
INSERT INTO `users` VALUES (9, 'Huy', 'Trần Duy Huy', 'huy@gmail.com', '$2y$10$64wy1HWsZROc4rgejJsJLeAkzaA59dNWgVYW/z2w6gVba/9Ca86OC', 0, NULL, '2018-08-08 06:46:32', '2018-08-08 06:46:32');

-- ----------------------------
-- Table structure for warranty
-- ----------------------------
DROP TABLE IF EXISTS `warranty`;
CREATE TABLE `warranty`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `primary_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sub_key_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `link_seo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of warranty
-- ----------------------------
INSERT INTO `warranty` VALUES (3, 'Chế độ bảo hành của Bosch', '/doc/warranty/1534394679.Dieu_kien_bao_hanh_cho_dong_may_HD.pdf', 1, '2018-08-16 04:44:39', '2018-08-16 04:44:39', '/images/warranty/1534394679.bosch.png', 'Chế độ bảo hành của Bosch', 'Chế độ bảo hành của Bosch', 'Chế độ bảo hành của Bosch', 'Bosch', 'http://vn.bosch-pt.com/vn/media/vietnam/desktop/professional/files_3/service_7/hd_warranty_vn/Dieu_kien_bao_hanh_cho_dong_may_HD.pdf');

SET FOREIGN_KEY_CHECKS = 1;
