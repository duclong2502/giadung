<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2WarrantyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warranty', function (Blueprint $table) {
            $table->string('title_seo')->nullable();
            $table->longText('description_seo')->nullable();
            $table->string('primary_key_seo')->nullable();
            $table->string('sub_key_seo')->nullable();
            $table->string('link_seo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
