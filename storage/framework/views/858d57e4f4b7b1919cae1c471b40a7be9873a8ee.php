<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

    <?php 
        $link = Request::fullUrl();
     ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', ['title_seo' => $product_detail->title_seo,
    'link_seo' => $product_detail->link_seo,
    'primary_key_seo' => $product_detail->primary_key_seo,
    'sub_key_seo' => $product_detail->sub_key_seo,
    'description_seo' => $product_detail->description_seo], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-product-detail">
        <br>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="lb-content lb-content-accordion">
                        <div id="accordion" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                             data-collapsible="true" data-height-style="content">
                            <h3 class="accordion-header-mod">
                                <span class="heading-line title-accordion-menu-item">Danh mục</span>
                                <span class="accordion-icon"></span>
                            </h3>
                            <div>
                                <ul>
                                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="<?php if($value->id == $category_seo->id): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(URL::action('FontEnd_Controller\ProductController@getProductCategory',$value->link)); ?>">
                                                <i class="fa fa-caret-square-o-right"></i><?php echo e($value->name); ?>

                                                <span class="category-counter">[ <?php echo e(MAGHelper::getTotalProduct($value->id)); ?>

                                                    ]</span>
                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                        <div id="accordion2" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                             data-collapsible="true" data-height-style="content">
                            <h3>
                                <span class="heading-line title-accordion-menu-item">Hãng sản xuất</span>
                                <span class="accordion-icon"></span>
                            </h3>
                            <div>
                                <ul>
                                    <?php $__currentLoopData = $producer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="<?php if($val->id == $product_detail->producer_id): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(route('getProductCategory',['cat'=>$category_seo->link,'hang'=>$val->slug])); ?>">
                                                <i class="fa fa-caret-square-o-right"></i>
                                                <?php echo e($val->name); ?>

                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="detail-title">
                        <h3 class=heading-line><?php echo e($product_detail->name); ?></h3>
                        
                    </div>
                    <div class="detail-main">
                        <div class="row">
                            <div class="col-sm-5 product-image wow fadeInLeft">
                                <ul class="bxslider-product enable-bx-slider" data-pager-custom="#bx-pager"
                                    data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="0"
                                    data-slide-margin="0" data-pager="true" data-mode="horizontal"
                                    data-infinite-loop="true">
                                    <?php $__currentLoopData = $img_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><img src="<?php echo e(asset($it->image)); ?>" alt="/"
                                                 style="width: 335px; height: 415px !important;"/></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <div class="product-image-thumbs">
                                    <ul id="bx-pager" class="pager-custom list-unstyled enable-bx-slider"
                                        data-pager-custom="null" data-controls="true" data-min-slides="4"
                                        data-max-slides="5" data-slide-width="60" data-slide-margin="5"
                                        data-pager="false" data-mode="horizontal" data-infinite-loop="false">
                                        <?php $__currentLoopData = $img_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a data-slide-index="<?php echo e($key); ?>" href="#"><img
                                                            src="<?php echo e(asset($it->image)); ?>" alt="/"
                                                            style="width: 56px; height: 56px !important;"/></a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-7 wow fadeInRight">
                                <div class="detail-info pd1">
                                    <div class="card-info">
                                        <div class="caption">
                                            <div class="name-item">
                                                <div class="card-price-block clearfix">
                                                    <?php if($product_detail->price_sale != 0): ?>
                                                        <span class="price-title">Giá chỉ còn</span>
                                                        <span class="product-price"><?php echo e(number_format($product_detail->price_sale)); ?>

                                                            VNĐ</span>
                                                        <br>
                                                        <span class="product-price-old"><?php echo e(number_format($product_detail->price)); ?>

                                                            VNĐ</span>
                                                    <?php else: ?>
                                                        <span class="product-price">Giá liên hệ</span>
                                                    <?php endif; ?>


                                                    <span class="product-availability pull-right">Còn hàng</span>
                                                </div>
                                                <div class="rating">
                                                    <span class="star"><i class="fa fa-star"></i></span>
                                                    <span class="star"><i class="fa fa-star"></i></span>
                                                    <span class="star"><i class="fa fa-star"></i></span>
                                                    <span class="star"><i class="fa fa-star"></i></span>
                                                    <span class="star star-empty"><i class="fa fa-star-o"></i></span>
                                                    <div class="add-review">
                                                        <span><span class="review-counter">4</span>Review(s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-description">
                                                <h6 class="heading-line">giới thiệu về sản phẩm</h6>
                                                <p>
                                                    <?php echo e($product_detail->description); ?>

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail-qty-color">
                                        <h6 class="heading-line">Số lượng</h6>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="input-group spinner" data-trigger="spinner">
                                                    <input type="text" data-rule="quantity" value="1" name="quality"
                                                           id="p_count">
                                                    <div class="spinner-btn">
                                                        <a class="btn btn-default" href="javascript:;" data-spin="up"><i
                                                                    class="fa fa-chevron-up"></i></a>
                                                        <a class="btn btn-default" href="javascript:;" data-spin="down"><i
                                                                    class="fa fa-chevron-down"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="cart-add-buttons">
                                                    <button type="button" name="addcart"
                                                            onclick="return addtocart(<?php echo e($product_detail->id); ?>);"
                                                            class="btn btn-cart-color2"><i
                                                                class="fa fa-shopping-cart fa-lg"></i> Thêm vào giỏ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="b-socials full-socials clearfix">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="b-hr">
                                    <hr>
                                </div>
                                <div class="detail-tabs wow fadeInUp">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a class="heading-line" href="product-details.html#description"
                                               aria-controls="description" role="tab" data-toggle="tab">Thông tin sản
                                                phẩm</a>
                                        </li>
                                        <li role="presentation">
                                            <a class="heading-line" href="product-details.html#thongso"
                                               aria-controls="reviews" role="tab" data-toggle="tab">Thông số kỹ
                                                thuật</a>
                                        </li>
                                        <li role="presentation">
                                            <a class="heading-line" href="product-details.html#comment"
                                               aria-controls="comment" role="tab" data-toggle="tab">Bình luận - Đánh
                                                giá</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="description">
                                            <?php echo $product_detail->thong_tin; ?>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="thongso">
                                            <?php echo $product_detail->thong_so; ?>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="reviews">
                                            <div class="form-group">
                                                <div class="content">
                                                    <?php $__currentLoopData = $comment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="row">
                                                            <div class="col-md-3 user-name">
                                                                <img src="<?php echo e(asset('/images/user.png')); ?>"
                                                                     style="width: 30%; border-radius: 50% !important; border: 1px solid #E15517;padding: 5px;">
                                                                <span class="name"
                                                                      style="padding-left: 5px"><?php echo e($rate->fullname); ?></span>
                                                            </div>
                                                            <div class="col-md-9 content-comment">
                                                                <div class="star" style="padding-bottom: 10px">
                                                                    <?php for($i = 0;$i<$rate->star;$i++): ?>
                                                                        <i class="fa fa-star-o" aria-hidden="true"
                                                                           style="color:#ffbb24"></i>
                                                                    <?php endfor; ?>
                                                                </div>
                                                                <p style="margin-bottom: 0 !important;"><?php echo e($rate->comment); ?></p>
                                                                <p class="date-comment"><?php echo date("d/m/Y",strtotime($rate->created_at)); ?></p>
                                                            </div>
                                                        </div>
                                                        <hr>

                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </div>
                                            </div>
                                            <div class="rating">
                                                <form method="post"
                                                      action="<?php echo e(URL::action('FontEnd_Controller\CartController@addRate',$product_detail->id)); ?>"
                                                      id="formValidate" class="form-horizontal"
                                                      enctype="multipart/form-data">
                                                    <?php echo e(csrf_field()); ?>

                                                    <h4>Đánh giá của bạn:</h4>
                                                    <div class='rating-stars'>
                                                        <ul id='stars'>
                                                            <li class='star' title='Poor' data-value='1'>
                                                                <i class='fa fa-star'></i>
                                                            </li>
                                                            <li class='star' title='Fair' data-value='2'>
                                                                <i class='fa fa-star'></i>
                                                            </li>
                                                            <li class='star' title='Good' data-value='3'>
                                                                <i class='fa fa-star'></i>
                                                            </li>
                                                            <li class='star' title='Excellent' data-value='4'>
                                                                <i class='fa fa-star'></i>
                                                            </li>
                                                            <li class='star' title='WOW!!!' data-value='5'>
                                                                <i class='fa fa-star'></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <br/>
                                                    <input type="hidden" id="total_star" name="star" value="5"/>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-body">
                                                                <input class="form-control" name="fullname"
                                                                       placeholder="Họ tên *" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-body">
                                                                <input class="form-control" name="email"
                                                                       placeholder="Email *" required>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="col-lg-12" style="padding-top: 10px">
                                                            <div class="form-body">
                                                                <textarea type="text" class="form-control"
                                                                          placeholder="Vui lòng để lại bình luận..."
                                                                          name="comment" required>
                                                                </textarea>
                                                            </div>
                                                            <button type="submit" class="btn btn-default">Gửi</button>
                                                        </div>
                                                        <div class="form-actions">

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="comment">
                                            <div class="fb-comments" data-href="<?php echo e($link); ?>" data-width="100%"
                                                 data-numposts="5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-hr custom-hr-1">
            <hr>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <h3 class="heading-line-long">Sản phẩm nổi bật</h3>

                </div>
                <div class="col-sm-12 wow fadeInUp">
                    <div class="row">
                        <div id="detail-related" class="b-related enable-owl-carousel" data-loop="true"
                             data-auto-width="false" data-dots="false" data-nav="false" data-margin="0"
                             data-responsive-class="true"
                             data-responsive='{"0":{"items":1},"479":{"items":2},"768":{"items":3},"1199":{"items":4}}'
                             data-slider-next=".slider-next" data-slider-prev=".slider-prev">
                            <?php $__currentLoopData = $product_highlight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $prh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="related-item">
                                    <div class="b-item-card">
                                        <div class="image">
                                            <a href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($prh->category_id),'slug'=>$prh->slug])); ?>">
                                                <img src="<?php echo e(asset($prh->image)); ?>"
                                                     class="img-responsive center-block" alt="/"
                                                     style="width: 260px; height: 248px !important;">
                                            </a>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="card-info">
                                            <div class="caption">
                                                <p class="name-item">
                                                    <a class="product-name"
                                                       href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($prh->category_id),'slug'=>$prh->slug])); ?>"
                                                       style="padding: 0px 10px"><?php echo e($prh->name); ?></a>
                                                </p>
                                                <?php if($prh->price_sale != 0): ?>
                                                    <span class="product-price">Giá liên hệ</span>
                                                <?php else: ?>
                                                    <span class="product-price"><?php echo e(number_format($prh->price_sale)); ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="add-buttons">
                                                <button type="button" class="btn btn-add btn-add-cart"
                                                        onclick="return addtocart(<?php echo e($prh->id); ?>);"><i
                                                            class="fa fa-shopping-cart"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=1486572324821329&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('themes/js/jquery-1.11.2.min.js')); ?>"></script>

    <script>
        function addtocart(pid) {
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@addCart')); ?>",
                type: "get",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    pid: pid,
                    count: $('#p_count').val(),
                },

                success: function (result) {
                    alert(result.mess);
                    location.reload();
                }
            });
        }
    </script>



    <script>
        $(document).ready(function () {
            $('#stars li').on('mouseover', function () {
                var onStar = parseInt($(this).data('value'), 10);
                $(this).parent().children('li.star').each(function (e) {
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });
            }).on('mouseout', function () {
                $(this).parent().children('li.star').each(function (e) {
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function () {
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');
                $('#total_star').val(onStar);
                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }
            });
        });
    </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>