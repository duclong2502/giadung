<h4>Có đơn hàng mới</h4>
<p>Tên khách hàng: <?php echo e($name); ?></p>
<p>Email: <?php echo e($mail); ?></p>
<p>Phone: <?php echo e($phone); ?></p>
<p>Địa chỉ: <?php echo e($address); ?></p>
<p>Ngày đặt: <?php echo date("<b>d/m/Y </b>",strtotime($date)); ?></p>
<div class="table-giohang table-responsive">
    <table border="1" class="table table-bordered table-middle table-striped table-condensed flip-content"
           style="border: 1px solid #000">
        <thead class="flip-content">
        <tr style="border: 1px solid #000">
            <th style="border: 1px solid #000"> Tên sản phẩm</th>
            <th style="border: 1px solid #000"> Đơn giá</th>
            <th style="border: 1px solid #000"> Số lượng</th>
            <th style="border: 1px solid #000"> Thành tiền</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($list) == 0): ?>
            <tr>
                <td colspan="5">Chưa có sản phẩm nào</td>
            </tr>
        <?php else: ?>
            <?php $total_price_sum = 0; ?>
            <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr id="cart-item-<?php echo e($item->id); ?>">
                    <td style="text-align: left;"><?php echo e($item->name); ?></td>
                    <td style="color: #E15517;text-align: right;">
                        <?php if($item->price != 0): ?>
                            <?php echo e(number_format($item->price)); ?>

                        <?php else: ?>
                            Giá liên hệ
                        <?php endif; ?>
                    </td>
                    <td style="color: #E15517;text-align: right;"> <?php echo e($item->total); ?></td>

                    <td style="color: #E15517;text-align: right;">
                        <?php echo e(number_format($item->price * $item->total)); ?>

                    </td>
                    <?php $total_price_sum = ($item->price * $item->total) + $total_price_sum ?>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<?php if(count($list) > 0): ?>
    <div class="tongthanhtoan">
        <span>Tổng thanh toán (VNĐ):</span><span class="money">
                <?php echo e(number_format($total_price_sum)); ?> VNĐ
        </span>
    </div>
<?php endif; ?>