<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-blog-main">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left">Tin tức</h3>
                        <div class="b-breadcrumbs pull-right">
                            
                                
                                    
                                
                                
                                    
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                    <div class="b-posts-holder">
                        <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="b-post-preview clearfix <?php if($key>1): ?>wow fadeInUp <?php endif; ?>">
                                <div class="post-image pull-left ">
                                    <div class="post-img-holder">
                                        <div>
                                            <img src="<?php echo e(asset($new->image)); ?>" width="320" height="360"
                                                 class="img-responsive center-block"
                                                 alt="/">
                                            <div class="image-add">
                                                <a href="<?php echo e(route('newsDetail',$new->slug)); ?>"
                                                   class="btn btn-default-color1 btn-sm">chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-data">
                                        <span><i class="fa fa-calendar"></i> <?php echo date("<b>d/m/20y </b>",strtotime($new->created_at)); ?></span>
                                    </div>
                                </div>
                                <div class="post-caption pull-left">
                                    <div class="post-author">
                                        <span><i class="fa fa-user"></i>Admin</span>
                                        
                                    </div>
                                    <div class="caption">
                                        <h5 class="heading-line"><?php echo e($new->title); ?></h5>
                                        <div class="post-description">
                                            <p>
                                                <?php echo e($new->description); ?>

                                            </p>
                                        </div>
                                    </div>
                                    <a href="<?php echo e(route('newsDetail',$new->slug)); ?>">đọc tiếp</a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <nav class="pagination-full clearfix wow fadeInUp">
                        <?php echo e($news->links()); ?>


                        <?php if(count($news) > 5): ?>
                            <ul class="pagination pagination-add" id="pagination-huytd">
                                <li>
                                    <a href="<?php echo e($news->previousPageUrl()); ?>" aria-label="Previous"
                                       class="prev">Previous</a>
                                </li>
                                <li>
                                    <a href="<?php echo e($news->nextPageUrl()); ?>" aria-label="Next" class="next">Next</a>
                                </li>
                            </ul>
                        <?php endif; ?>

                    </nav>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                    <div class="lb-content lb-content-accordion">
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">Sản phẩm nổi bật</h3>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="l-box-content">
                                    <div class="b-latest-rev">
                                        <div class="latest-rev-img pull-left">
                                            <img src="<?php echo e(asset($product->image)); ?>" class="img-responsive"
                                                 alt="/" style="width: 85px;height: 85px !important;">
                                        </div>
                                        <div class="latest-rev-caption">
                                            <a class="rev-caption-title"
                                               href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>"><?php echo e($product->name); ?></a>
                                            <p><?php echo e($product->description); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="l-box-mod">
                            <h3 class="heading-line">Tin tức liên quan</h3>
                            <?php $__currentLoopData = $news_lq; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="b-popular-post">
                                    <a class="popular-caption-title"
                                       href="<?php echo e(route('newsDetail',$item->slug)); ?>"><?php echo e($item->title); ?></a>
                                    <div class="popular-date"><?php echo date("<b>d/m/20y </b>",strtotime($item->created_at)); ?></div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>