<?php $__env->startSection("styles"); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
    <h1 class="page-title"> <?php echo e($title); ?>

        <small><?php echo e($title_description); ?></small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> <?php echo e($title); ?></span>
                    </div>
                    <div class="actions">
                        <a href="<?php echo e(URL::action('Cms\SlidersController@add')); ?>" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên slider</th>
                            <th> Liên kết</th>
                            <th> Ảnh</th>
                            <th colspan="2"> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td width="50px"><?php echo e(($sliders->currentPage()-1)*$sliders->perPage()+$key+1); ?></td>
                                <td><?php echo e($item->name); ?></td>
                                <td><?php echo e($item->link); ?></td>
                                <td>
                                    <img src="<?php echo e(asset($item->image)); ?>" width="20%">
                                </td>

                                <td class="numeric" width="50px">
                                    <a href="<?php echo e(URL::action('Cms\SlidersController@edit',$item->id)); ?>"
                                       class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>
                                </td>
                                <td class="" width="50px">
                                    <a href="<?php echo e(URL::action('Cms\SlidersController@delete',$item->id)); ?>"
                                       onclick="return confirm('Xác nhận xóa?')" title="Xóa"
                                       class="btn red"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div align="center">
                        <?php echo e($sliders->links()); ?>

                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("admin.layout.master", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>