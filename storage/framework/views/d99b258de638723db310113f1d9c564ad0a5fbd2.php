<?php $__env->startSection("styles"); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
    <h1 class="page-title"> <?php echo e($title); ?>

        <small><?php echo e($title_description); ?></small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> <?php echo e($title); ?></span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên khách hàng</th>
                            <th> Email</th>
                            <th> Số điện thoại</th>
                            <th> Lời nhắn</th>
                            <th> Trạng thái</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $contact; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td width="50px"><?php echo e(($contact->currentPage()-1)*$contact->perPage()+$key+1); ?></td>
                                <td><?php echo e($item->customer_name); ?></td>
                                <td><?php echo e($item->email); ?></td>
                                <td><?php echo e($item->phone); ?></td>
                                <td><?php echo e($item->message); ?></td>

                                <td>
                                    <?php if($item->status == "0"): ?>
                                        <button class="btn btn-circle btn-xs default change-status">Chưa duyệt</button>
                                    <?php else: ?>
                                        <button class="btn btn-circle btn-xs green change-status">Đã duyệt</button>
                                    <?php endif; ?>
                                </td>
                                
                                
                                
                                
                                <td class="" width="50px">
                                    <a href="<?php echo e(URL::action('Cms\ContactController@delete',$item->id)); ?>"
                                       onclick="return confirm('Xác nhận xóa?')" title="Xóa"
                                       class="btn red"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div align="center">
                        <?php echo e($contact->links()); ?>

                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("admin.layout.master", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>