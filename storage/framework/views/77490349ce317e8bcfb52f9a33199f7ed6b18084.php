<?php $__env->startSection("styles"); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection("content"); ?>
    <h1 class="page-title"> <?php echo e($title); ?>

        <small><?php echo e($title_description); ?></small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> <?php echo e($title); ?></span>
                    </div>
                    <div class="actions">
                        <a href="<?php echo e(URL::action('Cms\AdminController@add')); ?>" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên đăng nhập</th>
                            <th> Email</th>
                            <th> Level</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $admin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td width="50px"><?php echo e(($admin->currentPage()-1)*$admin->perPage()+$key+1); ?></td>
                                <td><?php echo e($item->username); ?></td>
                                <td><?php echo e($item->email); ?></td>
                                <td>
                                    <?php if($item->level == "0"): ?>
                                        <button class="btn btn-circle btn-xs green change-status">User</button>
                                    <?php else: ?>
                                        <button class="btn btn-circle btn-xs default change-status">Admin</button>
                                    <?php endif; ?>
                                </td>
                                <td class="numeric" width="50px">
                                    
                                       
                                    <a href="<?php echo e(URL::action("Cms\AdminController@delete", $item->id)); ?>"
                                       onclick="return confirm('Xác nhận xóa?')"
                                       class="btn red"><i class="fa fa-trash" title="Xóa"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div align="center">
                        <?php echo e($admin->links()); ?>

                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("admin.layout.master", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>