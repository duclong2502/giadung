<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-home home-2">
        <div class="header-slider background-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 nopadding-right">
                        <div class="sidebar-menu">
                            <ul class="parent-menu">
                                <?php $__currentLoopData = $categories_sidebar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <li class="li-parent">
                                            <a href="javascript:void(0)"><?php echo e($category->name); ?></a>
                                            <span style="font-size: 11px"><?php echo $category->description; ?></span>
                                            <?php if(count($category->childs)): ?>
                                                <?php echo $__env->make('font-end.layout.manageChild2',['childs' => $category->childs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            <?php endif; ?>
                                        </li>
                                    
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-10 nopadding-left">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <?php $i = 0;?>
                                <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $im): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo e($i); ?>"
                                        <?php if($key==0): ?>
                                        class="active"
                                            <?php endif; ?>>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="item <?php if($key==0): ?> active <?php endif; ?>"
                                         style="background-image: url(<?php echo e(asset($slider->image)); ?>)">
                                        
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty($ads)): ?>
            <div class="home-banner-ads background-gray banner-header">
                <div class="container">
                    <div class="row">
                        <?php $__currentLoopData = $ads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-lg-4 col-xs-4 col-sm-4">
                                <img src="<?php echo e(asset($a->image)); ?>"/>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="home-product-business background-yellow">
            <div class="container">
                <h1>Sản phẩm bán chạy</h1>
                <div class="row">
                    <div class="b-related by-brands column-slider wow bounce animated">
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="related-item col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="b-item-card">
                                    <?php if($product->price != 0 && $product->price_sale != 0): ?>
                                        <div class="special-plank sale">
                                            <span>- <?php echo e(ceil((($product->price - $product->price_sale)/$product->price) * 100)); ?>

                                                %</span>
                                        </div>
                                    <?php endif; ?>
                                    <div class="image">
                                        <a href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>">
                                            <img src="<?php echo e(asset($product->image)); ?>"
                                                 class="img-responsive center-block" alt="/"
                                                 style="width: 260px; height: 248px !important;">
                                        </a>
                                        
                                            
                                                
                                                    
                                                    
                                                    
                                                       
                                                       
                                                        
                                                    
                                                
                                            
                                        
                                    </div>
                                    <div class="card-info">
                                        <div class="caption">
                                            <p class="name-item">
                                                <a class="product-name"
                                                   href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>">
                                                    <?php echo e($product->name); ?></a>
                                            </p>
                                            <?php if($product->price_sale != 0): ?>
                                            <div class="product-price"><?php echo e(number_format($product->price_sale)); ?>

                                                VNĐ
                                            </div>
                                            <del><?php echo e(number_format($product->price)); ?></del>
                                            <?php else: ?>
                                                <br/>
                                                <div class="product-price">Giá liên hệ</div>
                                            <?php endif; ?>
                                            <button type="button" name="addcart"
                                                    onclick="return addtocart(<?php echo e($product->id); ?>);"
                                                    class="btn btn-cart-color2"><i
                                                        class="fa fa-shopping-cart fa-lg"></i> Mua nhanh
                                            </button>

                                            
                                                
                                                    
                                                    
                                                        
                                                              
                                                              
                                                              
                                                            
                                                            
                                                            

                                                                
                                                                    
                                                                        
                                                                            
                                                                                
                                                                                    
                                                                                    
                                                                                           
                                                                                
                                                                            
                                                                        
                                                                    
                                                                
                                                                
                                                                    
                                                                        
                                                                            
                                                                                
                                                                                    
                                                                                    
                                                                                           
                                                                                
                                                                            
                                                                        
                                                                    
                                                                
                                                                
                                                                    
                                                                        
                                                                            
                                                                                
                                                                                    
                                                                                    
                                                                                           
                                                                                           
                                                                                
                                                                            
                                                                        
                                                                    
                                                                
                                                                
                                                                    
                                                                        
                                                                            
                                                                                
                                                                                    
                                                                                    
                                                                                           
                                                                                
                                                                            
                                                                        
                                                                    
                                                                
                                                                
                                                                    
                                                                        
                                                                            
                                                                                
                                                                                    
                                                                                    
                                                                                              
                                                                                
                                                                            
                                                                        
                                                                        
                                                                    
                                                                
                                                                
                                                                    

                                                                    
                                                                    
                                                                        
                                                                        
                                                                    
                                                                
                                                            
                                                        
                                                    
                                                    
                                                        
                                                        
                                                            

                                                                
                                                                    
                                                                                         
                                                                    

                                                        
                                                        
                                                            
                                                                
                                                                    
                                                                        
                                                            
                                                        
                                                    
                                                
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="home-product-business background-gray">
            <div class="container">
                <h1>Sản phẩm mới</h1>
                <div class="row">
                    <div class="b-related by-brands column-slider wow bounce animated">
                        <?php $__currentLoopData = $products_new; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="<?php echo e($product->category->link); ?> related-item col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="b-item-card">
                                    <?php if($key < 4): ?>
                                        <div class="special-plank new">
                                            <span>new</span>
                                        </div>
                                    <?php endif; ?>
                                    <div class="image">
                                        <a href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>">
                                            <img src="<?php echo e(asset($product->image)); ?>"
                                                 class="img-responsive center-block" alt="/"
                                                 style="width: 260px; height: 248px !important;">
                                        </a>
                                        
                                            
                                                
                                                    
                                                    
                                                    
                                                       
                                                       
                                                        
                                                    
                                                
                                            
                                        
                                    </div>
                                    <div class="card-info">
                                        <div class="caption">
                                            <p class="name-item">
                                                <a class="product-name"
                                                   href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>">
                                                    <?php echo e($product->name); ?></a>
                                            </p>
                                            <?php if($product->price_sale != 0): ?>
                                                <div class="product-price"><?php echo e(number_format($product->price_sale)); ?>

                                                    VNĐ
                                                </div>
                                                <del><?php echo e(number_format($product->price)); ?></del>
                                            <?php else: ?>
                                                <br/>
                                                <div class="product-price">Giá liên hệ</div>
                                            <?php endif; ?>
                                            <button type="button" name="addcart"
                                                    onclick="return addtocart(<?php echo e($product->id); ?>);"
                                                    class="btn btn-cart-color2"><i
                                                        class="fa fa-shopping-cart fa-lg"></i> Thêm vào giỏ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty($ads_footer)): ?>
            <div class="home-banner-ads-f background-white">
                <div class="container">
                    <div class="row">
                        <?php $__currentLoopData = $ads_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ads): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-lg-4 col-xs-4 col-sm-4">
                                <img src="<?php echo e(asset($ads->image)); ?>"/>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>





    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    
    
    
    <script>
        function addtocart(pid) {
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@addCart')); ?>",
                type: "get",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    pid: pid,
                    count: 1,
                },

                success: function (result) {
                    // alert(result.mess);
                    location.reload();
                }
            });
        }
    </script>
    
        
            
            
        
    

<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>