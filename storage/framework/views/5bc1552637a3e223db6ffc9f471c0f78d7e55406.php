<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

    <?php 
        $link = Request::fullUrl();
     ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', ['title_seo' => $new_detail->title_seo,
    'link_seo' => $new_detail->link_seo,
    'primary_key_seo' => $new_detail->primary_key_seo,
    'sub_key_seo' => $new_detail->sub_key_seo,
    'description_seo' => $new_detail->description_seo], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-blog-post">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left"><?php echo $new_detail->title; ?></h3>
                        <div class="b-breadcrumbs pull-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                    <div class="post-image wow fadeInUp">
                        
                            
                                 
                        
                        
                            
                        
                    </div>
                    <div class="b-posts-holder wow fadeInUp">
                        <div class="b-post-main clearfix">
                            <div class="post-caption">
                                <div class="caption">
                                    <h5 class="heading-line"><?php echo $new_detail->title; ?></h5>
                                    <div class="post-author">
                                        <span><i class="fa fa-user"></i>Admin</span>
                                        <span><i class="fa fa-comment-o"></i>12 Comments</span>
                                        <span><i class="fa fa-clock-o"></i><?php echo date("<b>d/m/20y </b>",strtotime($new_detail->created_at)); ?></span>
                                    </div>
                                    <hr>
                                    <div class="post-description">
                                        <?php echo $new_detail->content; ?>

                                    </div>
                                    <div class="b-socials full-socials clearfix">
                                        <ul class="list-unstyled">
                                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook fa-fw"></i>Share</a>
                                            </li>
                                            <li><a href="https://plus.google.com/"><i
                                                            class="fa fa-google-plus fa-fw"></i>Google+</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-post-comments wow fadeInUp">
                        <h3 class="heading-line">Comments</h3>
                        <div class="fb-comments" data-href="<?php echo e($link); ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                    <div class="lb-content lb-content-accordion">
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">Sản phẩm nổi bật</h3>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="l-box-content">
                                    <div class="b-latest-rev">
                                        <div class="latest-rev-img pull-left">
                                            <img src="<?php echo e(asset($product->image)); ?>" class="img-responsive"
                                                 alt="/" style="width: 85px;height: 85px !important;">
                                        </div>
                                        <div class="latest-rev-caption">
                                            <a class="rev-caption-title" href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>"><?php echo e($product->name); ?></a>
                                            <p><?php echo e($product->description); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">tin tức liên quan</h3>
                            <?php $__currentLoopData = $population_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="b-popular-post">
                                    <a class="popular-caption-title" href="<?php echo e(route('newsDetail',$pn->slug)); ?>"><?php echo e($pn->title); ?></a>
                                    <div class="popular-date"><?php echo date("<b>d/m/20y </b>",strtotime($pn->created_at)); ?></div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=1486572324821329&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>