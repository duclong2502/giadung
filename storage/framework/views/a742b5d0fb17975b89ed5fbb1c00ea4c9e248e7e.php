<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
    <meta name="csrf-param"/>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <?php echo $__env->yieldContent('script-h'); ?>
    <title><?php echo $__env->yieldContent('title'); ?></title>
    <base href="<?php echo e(asset('')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>" type="text/css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700&amp;subset=vietnamese"
          rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <link href="/themes/css/master.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/rating.css" type="text/css">

    <!-- SWITCHER -->
    <link rel="stylesheet" id="switcher-css" type="text/css" href="/themes/assets/switcher/css/switcher.css'"
          media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="/themes/assets/switcher/css/color1.css'" title="color1"
          media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="/themes/assets/switcher/css/color2.css'" title="color2"
          media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="/themes/assets/switcher/css/color3.css'" title="color3"
          media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="/themes/assets/switcher/css/color4.css'" title="color4"
          media="all"/>

    <?php echo $__env->yieldContent('seo'); ?>

    <script type="text/JavaScript">
        function killCopy(e){
            return false
        }
        function reEnable(){
            return true
        }
        document.onselectstart=new Function ("return false")
        if (window.sidebar){
            document.onmousedown=killCopy
            document.onclick=reEnable
        }
    </script>

</head>
<body>

<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+)
    center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 80px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -140px; top: 5px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style>
<div class="fb-livechat"> <div class="ctrlq fb-overlay"></div>
    <div class="fb-widget"> <div class="ctrlq fb-close"></div>
        <div class="fb-page" data-href="https://www.facebook.com/mocanhlimited" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div>
        <div class="fb-credit"> <a href="https://www.facebook.com/mocanhlimited/" target="_blank">MAG</a> </div>
        <div id="fb-root"></div>
    </div>
    <a href="https://m.me/mocanhlimited" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button">
        <div class="bubble">1</div>
        <div class="bubble-msg">Bạn cần hỗ trợ?</div>
    </a>
</div>
<script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>jQuery(document).ready(function($){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) )
{return true;}
else{return false;}}
var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")};
setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>


<!-- Loader -->

<!-- Loader end -->
<div class="b-page">
    <!-- Start Switcher -->






















<!-- End Switcher -->
    
    <?php echo $__env->make('font-end.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    

    

    
    <?php echo $__env->yieldContent('font-end-content'); ?>
    

    
    <?php echo $__env->make('font-end.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
</div>


    
    
        
        
        
        
        
        
    

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b7f8c84f31d0f771d84187e/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
<?php echo $__env->yieldContent('script'); ?>


<script src="/themes/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/global/plugins/jquery.min.js')); ?>"></script>
<script src="/themes/js/bootstrap.min.js"></script>

<script src="/themes/assets/switcher/js/dmss.js"></script>

<script src="/themes/js/jquery-ui.min.js"></script>
<script src="/themes/js/modernizr.custom.js"></script>
<script src="/themes/js/wow.min.js"></script>

<!--bootstrap-select -->
<script src="/themes/assets/bootstrap-select/bootstrap-select.min.js"></script>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Countdown Timer -->
<script src="/themes/assets/countdown/dscountdown.min.js"></script>

<!--Owl Carousel-->
<script src="/themes/assets/owl-carousel/owl.carousel.min.js"></script>

<!--bx slider-->
<script src="/themes/assets/bxslider/jquery.bxslider.min.js"></script>

<!-- slider-pro-master -->
<script src="/themes/assets/slider-pro-master/js/jquery.sliderPro.min.js"></script>

<script src="/themes/assets/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<script src="/themes/js/waypoints.min.js"></script>
<script src="/themes/js/jquery.easypiechart.min.js"></script>
<script src="/themes/js/jquery.spinner.min.js"></script>
<script src="/themes/js/isotope.pkgd.min.js"></script>
<script src="/themes/js/jquery.placeholder.min.js"></script>
<script src="/themes/js/theme.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        <?php if(session('success')): ?>
        swal("Thành công!", '<?php echo e(session('success')); ?>', "success");
        <?php elseif(session('error')): ?>
        swal("Lỗi!", '<?php echo e(session('error')); ?>', "error");
        <?php endif; ?>
    });
</script>

</html>