<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-category">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left"><?php echo e($category_child->name); ?></h3>
                        <div class="b-breadcrumbs pull-right">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="/">home</a>
                                </li>

                                <li>
                                    <span><?php echo e($category_child->name); ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <form action="" method="get" id="hjhj">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="lb-content lb-content-accordion">
                            <div id="accordion" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3 class="accordion-header-mod">
                                    <span class="heading-line title-accordion-menu-item">Danh mục</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    <ul>
                                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="<?php if($value->id == $category_child->id): ?> active <?php endif; ?>">
                                                <a href="<?php echo e(URL::action('FontEnd_Controller\ProductController@getProductCategory',$value->link)); ?>">
                                                    <i class="fa fa-caret-square-o-right"></i><?php echo e($value->name); ?>

                                                    <span class="category-counter">[ <?php echo e(MAGHelper::getTotalProduct($value->id)); ?>

                                                        ]</span>
                                                </a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <div id="accordion2" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3>
                                    <span class="heading-line title-accordion-menu-item">Hãng sản xuất</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    <ul>
                                        <?php $__currentLoopData = $producer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="<?php if($producer_child != null && $producer_child->id == $val->id): ?> active <?php endif; ?>">
                                                <a href="<?php echo e(route('getProductCategory',['cat'=>$slug,'hang'=>$val->slug])); ?>">
                                                    <i class="fa fa-caret-square-o-right"></i>
                                                    <?php echo e($val->name); ?>

                                                </a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                            <div id="accordion3" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3>
                                    <span class="heading-line title-accordion-menu-item">Giá</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    

                                    <div class="price-block">
                                        
                                        
                                        
                                        
                                        
                                        <ul>
                                            <li>
                                                <a href="<?php echo e(url()->current().'?p-min=0&p-max=2,000,000'); ?>" id="a-submit"><i
                                                            class="fa fa-caret-square-o-right"></i>0
                                                    - 2,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo e(url()->current().'?p-min=2,000,000&p-max=5,000,000'); ?>"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>2,000,000
                                                    - 5,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo e(url()->current().'?p-min=5,000,000&p-max=10,000,000'); ?>"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>5,000,000
                                                    - 10,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo e(url()->current().'?p-min=10,000,000&p-max=15,000,000'); ?>"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>10,000,000
                                                    - 15,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo e(url()->current().'?p-min=15,000,000&p-max='); ?>" id="a-submit"><i
                                                            class="fa fa-caret-square-o-right"></i>
                                                    > 15,000,000đ </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <button class="btn btn-default-color1 btn-sm">Lọc</button>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="side-offer wow fadeInUp">
                            
                            
                            
                            
                            
                            <div class="l-box-mod">
                                <h3 class="heading-line">Tin tức liên quan</h3>
                                <?php $__currentLoopData = $news_lq; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="b-popular-post">
                                        <a class="popular-caption-title"
                                           href="<?php echo e(route('newsDetail',$item->slug)); ?>"><?php echo e($item->title); ?></a>
                                        <div class="popular-date"><?php echo date("<b>d/m/20y </b>",strtotime($item->created_at)); ?></div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <div class="b-offers wow fadeInUp">
                            
                            
                            
                            
                        </div>
                        <div class="b-settings">
                            <div class="settings-tools">
                                <h3 class="heading-line pull-left">
                                    <?php echo e($category_child->name); ?>

                                    <span class="settings-counter">[ <?php echo e(count($products)); ?> sản phẩm ]</span>
                                </h3>
                                <div class="settings-block pull-right">
                                    <div class="settings-options">
                                        <form action="" method="get" id="hjhj">
                                            <div class="select-block">
                                                <span class="select-title">Sắp xếp theo</span>
                                                <select class="selectpicker" id="select-box" onchange="getval()"
                                                        data-width="134px" name="sort">
                                                    <option>Lọc</option>
                                                    <option value="new" <?php if(old('sort') == 'new'): ?> selected <?php endif; ?>>Mới
                                                        nhất
                                                    </option>
                                                    <option value="min-max"
                                                            <?php if(old('sort') == 'min-max'): ?> selected <?php endif; ?>>Giá thấp - cao
                                                    </option>
                                                    <option value="max-min"
                                                            <?php if(old('sort') == 'max-min'): ?> selected <?php endif; ?>>Giá cao - thấp
                                                    </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="settings-view hidden-md hidden-sm hidden-xs">
                                        <ul id="type-of-display" class="list-unstyled">
                                            
                                            
                                            
                                            
                                            
                                            <li>
                                                <button class="btn toogle-view grid-3 active-view">
                                                    <i class="fa fa-th-large fa-fw"></i>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-grid">
                            <div class="row">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                        <div class="b-item-card">
                                            <div class="image"
                                                 style="background-image: url(<?php echo e(asset($product->image)); ?>)">
                                                <a href="<?php echo e(asset($product->image)); ?>" data-gal="prettyPhoto"
                                                   title="6s Plus">
                                                    <img src="<?php echo e(asset($product->image)); ?>"
                                                         class="img-responsive center-block" alt="<?php echo e($product->name); ?>"
                                                         style="width: 260px;height: 248px !important;">
                                                    <div class="image-add-mod">
														<span class="btn btn-lightbox btn-default-color1 btn-sm">
															<i class="fa fa-search-plus fa-lg"></i>
														</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="card-info">
                                                <div class="caption">
                                                    <div class="name-item">
                                                        <a class="product-name" style="padding: 0px 10px"
                                                           href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])); ?>"><?php echo e($product->name); ?></a>
                                                        <div class="rating">
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="card-price-block">
                                                        <span class="price-title">Giá</span>
                                                        <?php if($product->price_sale != 0): ?>
                                                            <span class="product-price"><?php echo e(number_format($product->price_sale)); ?></span>
                                                            <div class="">
                                                                <del><?php echo e(number_format($product->price)); ?></del>
                                                            </div>
                                                        <?php else: ?>
                                                            <span class="product-price">Giá liên hệ</span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="product-description">
                                                        <p><?php echo e($product->description); ?>

                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="add-buttons">
                                                    <button type="button" class="btn btn-add btn-add-cart"
                                                            onclick="return addtocart(<?php echo e($product->id); ?>);"><i
                                                                class="fa fa-shopping-cart"></i></button>
                                                    <div class="cart-add-buttons">
                                                        <button type="button" class="btn btn-cart-color1"
                                                                onclick="return addtocart(<?php echo e($product->id); ?>);"><i
                                                                    class="fa fa-shopping-cart"></i> Thêm vào giỏ
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <nav class="pagination-full clearfix wow fadeInUp">
                                <?php echo e($products->links()); ?>


                                <?php if(count($products) > 8): ?>
                                    <ul class="pagination pagination-add" id="pagination-huytd">
                                        <li>
                                            <a href="<?php echo e($products->previousPageUrl()); ?>" aria-label="Previous"
                                               class="prev">Previous</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo e($products->nextPageUrl()); ?>" aria-label="Next"
                                               class="next">Next</a>
                                        </li>
                                    </ul>
                                <?php endif; ?>

                            </nav>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('themes/js/jquery-1.11.2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/money.js')); ?>"></script>
    <script>
        function addtocart(pid) {
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@addCart')); ?>",
                type: "get",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    pid: pid,
                    count: 1,
                },

                success: function (result) {
                    alert(result.mess);
                    location.reload();
                }
            });
        }

        function getval() {
            $('#select-box').change(function () {
                $('#hjhj').submit();
            });
        }
    </script>
    <script>
        $('#price-max').simpleMoneyFormat();

        $('#a-submit').click(function () {
            $('#haha').submit();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>