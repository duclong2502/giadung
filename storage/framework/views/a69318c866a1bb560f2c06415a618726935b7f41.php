
<meta name="title" content="<?php echo e(isset($title_seo) ? $title_seo : MAGHelper::getTitleSeoConfig($key = "title_seo")); ?>">
<meta name="link" content="<?php echo e(isset($link_seo) ? $link_seo : MAGHelper::getTitleSeoConfig($key = "link_seo")); ?>">
<meta name="primary_key_seo" content="<?php echo e(isset($primary_key_seo) ? $primary_key_seo : MAGHelper::getTitleSeoConfig($key = "primary_key_seo")); ?>">
<meta name="sub_key_seo" content="<?php echo e(isset($sub_key_seo) ? $sub_key_seo : MAGHelper::getTitleSeoConfig($key = "sub_key_seo")); ?>">
<meta name="description_seo" content="<?php echo e(isset($description_seo) ? $description_seo : MAGHelper::getTitleSeoConfig($key = "description_seo")); ?>">
