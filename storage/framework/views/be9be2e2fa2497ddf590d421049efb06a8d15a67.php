<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>Gia dụng MAG</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>

    
    <link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
<?php echo $__env->yieldContent("styles",""); ?>
<!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>

<!-- END QUICK NAV -->
<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script>
<script src="/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="#">
                    <h4 class="logo-default" style="color: #36c6d3; font-weight: bold">MAG</h4>
                    
                </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN INBOX DROPDOWN -->

                    <!-- END INBOX DROPDOWN -->

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            
                                
                                    
                            

                            <li>
                                <a href="<?php echo e(url('/logout')); ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
                    data-slide-speed="200" style="padding-top: 20px">
                    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <li class="sidebar-toggler-wrapper hide">
                        <div class="sidebar-toggler">
                            <span></span>
                        </div>
                    </li>
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                    <li class="nav-item start ">
                        <a href="/dashboard" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">Trang chủ</span>
                        </a>
                    </li>
                    <li class="heading">
                        <h3 class="uppercase">Danh mục</h3>
                    </li>

                    <li class="nav-item <?php if(Request::is('user') || Request::is('user') ): ?> active <?php endif; ?>  ">
                        <a href="<?php echo e(URL::action('Cms\AdminController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-user"></i>
                            <span class="title">Quản lý tài khoản</span>
                        </a>
                    </li>


                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\PostsController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-file-text"></i>
                            <span class="title">Quản lý bài viết</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\SlidersController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-sliders"></i>
                            <span class="title">Quản lý slider</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\OrdersController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="title">Quản lý đơn hàng</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\CategoryTermController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-bars"></i>
                            <span class="title">Quản lý danh mục sản phẩm</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\WarrantyController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-bars"></i>
                            <span class="title">Chế độ bảo hành</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\BannerController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-sliders"></i>
                            <span class="title">Banner quảng cáo</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\ContactController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-comments"></i>
                            <span class="title">Phản hồi khách hàng</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\ConfigController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i>
                            <span class="title">Config hệ thống</span>
                        </a>
                    </li>

                    
                        
                            
                            
                        
                    

                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-cubes"></i>
                            <span class="title nav-link nav-toggle">Sản phẩm</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu"
                            <?php if(Request::is('product-category')||Request::is('product')||Request::is('product-category/*')||Request::is('product/*')): ?>
                            style="display: block;" <?php endif; ?>>
                            <li class="nav-item <?php if(Request::is('product')||Request::is('product/*')): ?> active <?php endif; ?> ">
                                <a href="<?php echo e(URL::action('Cms\ProductsController@index')); ?>">
                                    <i class="fa fa-cubes"></i>
                                    <span class="title">Quản lý sản phẩm</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if(Request::is('producer')||Request::is('producer/*')): ?> active <?php endif; ?> ">
                                <a href="<?php echo e(URL::action('Cms\ProducerController@index')); ?>">
                                    <i class="fa fa-cubes"></i>
                                    <span class="title">Quản lý nhà sản xuất</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\FooterController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i>
                            <span class="title">Footer</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if(Request::is('posts') || Request::is('posts/*') ): ?> active <?php endif; ?>   ">
                        <a href="<?php echo e(URL::action('Cms\MenuController@index')); ?>" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i>
                            <span class="title">Quản lý menu</span>
                        </a>
                    </li>


                </ul>
                <!-- END SIDEBAR MENU -->
                <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->

                <!-- END PAGE BAR -->

                <?php if($errors->any()): ?>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(Session::has("success")): ?>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-lg-12">
                            <div class="alert alert-success"><?php echo e(Session::get("success")); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if(Session::has("error")): ?>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-lg-12">
                            <div class="alert alert-danger"><?php echo e(Session::get("error")); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php echo $__env->yieldContent("content",""); ?>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; MAG

        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->
</div>
<?php echo $__env->yieldContent("script",""); ?>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    //    $(document).ready(function () {
    //        $('#clickmewow').click(function () {
    //            $('#radio1003').attr('checked', 'checked');
    //        });
    //    })
</script>
</body>

</html>