<div class="b-hr">
    <hr>
</div>
<div class="container" style="display: none">
    <div class="row">
        <div class="col-sm-12">
            <div class="b-features-wrapper">
                <div class="b-store-features clearfix wow fadeInUp">
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-thumbs-up"></i>
                            </div>
                            <div class="feature-info">
                                <p>Bảo hành đầy đủ</p>
                                <p>Đọc về bảo hành của chúng tôi</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <div class="feature-info">
                                <p>Dịch vụ vận chuyển</p>
                                <p>Dịch vụ vận chuyển chu đáo</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-commenting"></i>
                            </div>
                            <div class="feature-info">
                                <p>Cập nhật liên tục</p>
                                <p>Cập nhật thông tin hàng ngày</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-headphones"></i>
                            </div>
                            <div class="feature-info">
                                <p>24/7 Support</p>
                                <p>Liên hệ với chúng tôi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="style-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="b-logo">
                    <a href="/">
                        <img src="/themes/images/logo.png">
                    </a>
                </div>
                <div class="b-footer-contacts wow pulse nimated">
                    <?php echo $footer; ?>

                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="row">
                    <div class="b-footer-menu clearfix">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-menu-item wow pulse nimated">
                                <div class="heading-line">Menu</div>
                                <ul class="list-unstyled">
                                    <?php $__currentLoopData = $categories_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="#"><?php echo e($cf->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/tin-tuc">Tin tức</a></li>
                                    <li><a href="/lien-he">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-menu-item wow pulse nimated">
                                <div class="heading-line">Mua hàng tại MAG</div>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo e(URL::action('FontEnd_Controller\CartController@getCart')); ?>">Giỏ
                                            hàng</a></li>
                                    <li><a href="<?php echo e(URL::action('FontEnd_Controller\HomeController@getWarranty')); ?>">Chế
                                            độ bảo hành</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="b-latest-tweets wow pulse nimated">
                                <div class="heading-line">Fanpage</div>
                                <div class="fb-page" data-href="<?php echo e($fanpage_facebook); ?>"
                                     data-tabs="timeline" data-height="200" data-small-header="false"
                                     data-adapt-container-width="true"
                                     data-hide-cover="false" data-show-facepile="false">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="b-footer-add">
        <div class="container">
            <div class="row">
                
                    
                        
                            
                                
                            
                        
                    
                
                <div class="col-sm-4">
                    <div class="b-payments ">
                        <h6>Cách thức thanh toán</h6>
                        <ul class="list-unstyled wow fadeInRight">
                            <li>
                                <img src="/images/icon/Layer 1.png" class="img-responsive" alt="/"
                                     style="width: 50px;height: 28px">
                            </li>
                            <li>
                                <img src="/images/icon/Layer 2.png" class="img-responsive" alt="/"
                                     style="width: 47px;height: 28px">
                            </li>
                            <li>
                                <img src="/images/icon/Layer 3.png" class="img-responsive" alt="/"
                                     style="width: 47px;height: 28px">
                            </li>
                            <li>
                                <img src="/images/icon/Layer 4.png" class="img-responsive" alt="/"
                                     style="width: 47px;height: 28px">
                            </li>
                            <li>
                                <img src="/images/icon/Layer 5.png" class="img-responsive" alt="/"
                                     style="width: 47px;height: 28px">
                            </li>
                            <li>
                                <img src="/images/icon/Layer 6.png" class="img-responsive" alt="/"
                                     style="width: 47px;height: 28px">
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="p-payments attitude">
                        <h6>Dịch vụ giao hàng</h6>
                        <div>
                            <span class="post-department"><img src="/images/icon/Layer 7.png"></span>
                            <span class="post-department"><img src="/images/icon/Layer 8.png"></span>
                        </div>
                        <div>
                            <span class="post-department"><img src="/images/icon/Layer 9.png" ></span>
                            <span class="post-department"><img src="/images/icon/Layer 10.png"></span>
                        </div>
                        <div>
                            <span class="post-department"><img src="/images/icon/Layer 11.png" ></span>
                            <span class="post-department"><img src="/images/icon/Layer 12.png" ></span>
                        </div>
                        <div>
                            <span class="post-department"><img src="/images/icon/Layer 13.png" ></span>
                            <span class="post-department"><img src="/images/icon/Layer 14.png" ></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="b-payments ">
                        <h6>Chứng nhận</h6>
                        <div class="row">
                            <div class="col-sm-3">
                                <div>
                                    <span class="post-department"><img src="/images/icon/Layer 15.png"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <span class="post-department"><img src="/images/icon/Layer 16.png"></span>
                                </div>
                                <div>
                                    <span class="post-department"><img src="/images/icon/Layer 17.png"></span>
                                </div>
                                <div>
                                    <span class="post-department"><img src="/images/icon/Layer 18.png"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="end-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h6>MAG - KÊNH MUA SẮM TRỰC TUYẾN HÀNG ĐẦU</h6>
                    <p>
                        Với một tinh thần chăm chỉ mỗi ngày, thái độ phục vụ tốt nhất và đặt tiêu chí chất lượng hàng
                        đầu châu Âu là giá trị cốt lõi hoạt động của chúng tôi.
                    </p>
                </div>
                <div class="col-sm-4 hashtag">
                    <h6>SẢN PHẨM MỚI</h6>
                    <?php $__currentLoopData = $product_new; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($value->category_id),'slug'=>$value->slug])); ?>"><?php echo e($value->name); ?> ,</a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
                <div class="col-sm-4 hashtag">
                    <h6>SẢN PHẨM NỔI BẬT</h6>
                    <div class="category">
                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <h1><?php echo e($val->name); ?></h1>
                            <?php $__currentLoopData = $product_hot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($value->category_id == $val->id): ?>
                                    <?php if($key <10): ?>
                                        <a href="<?php echo e(route('productDetail',['cat'=>MAGHelper::getSlugCategory($value->category_id),'slug'=>$value->slug])); ?>"><?php echo e($value->name); ?>,</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=1486572324821329&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>