<ul class="sub-menu">
    <?php $__currentLoopData = $childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li>
            <a href="<?php echo e(URL::action('FontEnd_Controller\ProductController@getProductCategory',$child->link)); ?>

                    "><?php echo e($child->name); ?></a>

            <?php if(count($child->childs)): ?>
                <?php echo $__env->make('manageChild',['childs' => $child->childs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
