<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-shopping-cart">
        
        
        
        
        
        <div class="container">
            <div class="row" style="background: #f7f7f7">
                <div class="col-sm-12 cart-table wow fadeInUp">
                    <div class="b-table b-cart-table table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>
                                    <span>Sản phẩm</span>
                                </td>
                                <td>
                                    <span>Đơn giá</span>
                                </td>
                                <td>
                                    <span>Số lượng</span>
                                </td>
                                <td>
                                    <span>Thành tiền</span>
                                </td>
                                <td>
                                    <span>Xóa</span>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  $total = 0;  ?>
                            <?php if(count($item) == 0): ?>
                                <tr>
                                    <td colspan="8" style="padding-bottom: 10px">Chưa có sản phẩm nào</td>
                                </tr>
                            <?php else: ?>
                                <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr id="cart-item-<?php echo e($value->id); ?>">
                                        <td>
                                            <div class="image">
                                                <img src="<?php echo e(asset($value->image)); ?>"
                                                     class="img-responsive img-thumbnail center-block"
                                                     alt="<?php echo e($value->name); ?>"
                                                     style="width: 80px; height: 55px !important;">
                                            </div>
                                            <div class="caption">
                                                <a class="product-name" href="#"><?php echo e($value->name); ?></a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if($value->price != 0): ?>
                                                <span class="product-price"><?php echo e(number_format($value->price)); ?></span>
                                            <?php else: ?>
                                                <span class="product-price">Giá liên hệ</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <div class="input-group btn-block qty-block" data-trigger="spinner">
                                                <a class="spinner-btn-mod" href="javascript:;"
                                                   onclick="return minus(<?php echo e($value->id); ?>);"
                                                   data-spin="down">-</a>
                                                <input class="form-control" type="text" value="<?php echo e($value->total); ?>"
                                                       data-rule="quantity" name="quantity"
                                                       id="quality-<?php echo e($value->id); ?>"
                                                       data-id="<?php echo e($value->id); ?>"
                                                       style="width: 55px" readonly>
                                                <a class="spinner-btn-mod" onclick="return plus(<?php echo e($value->id); ?>);"
                                                   href="javascript:;"
                                                   data-spin="up">+</a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if($value->price != 0): ?>
                                                <span class="product-price total-price"><?php echo e(number_format($value->price * $value->total)); ?></span>
                                                <?php  $total = $total + ($value->price * $value->total);  ?>
                                            <?php else: ?>
                                                <span class="product-price total-price">Giá liên hệ</span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-left">
                                            <a onclick="return remove(<?php echo e($value->id); ?>);" href="javascript:void(0)"
                                               class="btn btn-remove"><i class="fa fa-trash fa-lg"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                <?php if(count($item) > 0): ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="b-total-table clearfix">
                            <table class="table table-condensed">
                                <tbody>
                                <tr class="total">
                                    <td style="padding-top: 50px;">Tổng đơn giá:</td>
                                    <td style="padding-top: 50px;"><?php echo e(number_format($total)); ?>đ</td>
                                    <td width="27%">
                                        <a href="<?php echo e(URL::action('FontEnd_Controller\CartController@thanhtoan')); ?>"
                                           class="btn btn-primary-color2 btn-sm">Thanh toán
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('themes/js/jquery-1.11.2.min.js')); ?>"></script>

    <script>
        function remove(id) {
            if (confirm("Bạn có muốn xóa sản phẩm!")) {
                $.ajax({
                    url: "<?php echo e(URL::action('FontEnd_Controller\CartController@removeProduct')); ?>",
                    type: "post",
                    dateType: "json",
                    data: {
                        _token: "<?php echo e(csrf_token()); ?>",
                        id: id,

                    },
                    success: function (result) {
                        location.reload();
                        alert(result.mess);
                    }
                });
            } else {
                return false;
            }
        }

        function plus(id) {
            var quality = (+$("#quality-" + id).val() + 1);
            console.log(quality);
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@plus')); ?>",
                type: "post",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    id: id,
                    quality: quality,

                },
                success: function (result) {
                    location.reload();
                    console.log(result.mess);
                }
            });
        }

        function minus(id) {
            var quality = (+$("#quality-" + id).val() - 1);
            console.log(quality);
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@minus')); ?>",
                type: "post",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    id: id,
                    quality: quality,

                },
                success: function (result) {
                    location.reload();
                    console.log(result.mess);
                }
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>