<header>
    <div class="b-top-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="b-top-options b-top-info pull-left">
                        <div class="nav">
                            <ul class="list-inline">
                                <li>
                                    <span><i class="fa fa-phone"></i>0964.791.791 - 0989.198.548</span>
                                </li>
                                <li>
                                    <span><i class="fa fa-envelope-o"></i>mocanhxnk@gmail.com</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="b-top-options b-top-info pull-right">
                        <div class="nav">
                            <ul class="list-inline">
                                <li>
                                    <span><a href="<?php echo e($facebook); ?>"><i class="fa fa-facebook"></i></a></span>
                                </li>
                                <li>
                                    <span><a href="<?php echo e($youtube); ?>"><i class="fa fa-youtube-play"></i></a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-header-main style-2">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                    <div class="b-logo">
                        <a href="/">
                            
                            <img src="/themes/images/logo.png">
                        </a>
                    </div>
                </div>
                <div id="toggle-nav" class="col-xs-12 col-sm-10 col-md-8 col-lg-9 menu-wrapper clearfix">
                    <div class="toggle-nav-btn">
                        <button class="btn btn-menu"><i class="fa fa-bars fa-lg"></i></button>
                    </div>
                    <div class="b-header-menu pull-right">
                        <ul class="list-inline">
                            <li>
                                <a class="heading-line" href="/">Trang chủ</a>

                            </li>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    
                                    <?php if($category->type == 2): ?>
                                        <a class="heading-line"
                                           href="<?php echo e(URL::action('FontEnd_Controller\ProductController@getProductCategory',$category->getLinkByID['link'])); ?>"><?php echo e($category->name); ?></a>
                                    <?php elseif($category->type == 1): ?>
                                        <a class="heading-line"
                                           href="<?php echo e($category->link); ?>"><?php echo e($category->name); ?></a>
                                    <?php elseif($category->type == 3): ?>
                                        <a class="heading-line"
                                           href="<?php echo e(route('newsDetail',$category->getSlugByID['slug'])); ?>"><?php echo e($category->name); ?></a>
                                    <?php endif; ?>

                                    <?php if(count($category->childs)): ?>
                                        <?php echo $__env->make('font-end.layout.manageChild',['childs' => $category->childs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <li class="search">
                                <a id="search-open" class="iconSearch" href="#"><i class="fa fa-search"></i></a>
                                <div id="search">
                                    <form method="get">
                                        <div class="form-group">
                                            <input id="searchQuery" type="search" name="home-search"
                                                   placeholder="Search...">
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="cart-wrapper" class="col-xs-6 col-sm-12 col-md-2 col-lg-1">
                    <div class="b-cart pull-right">
                        

                            <span><i class="fa fa-shopping-cart fa-2x" style="cursor: pointer;"></i></span>
                            <span class="counter-wrapper"><span class="counter"><?php echo count($carts); ?></span></span>
                        
                        <div class="cart-products">
                            <div class="c-holder">
                                <h3 class="title">
                                    sản phẩm đã đặt
                                </h3>
                                <ul class="products-list list-unstyled">
                                    <?php $__currentLoopData = $carts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <div class="row b-cart-table">
                                                <a href="" class="image">
                                                    <img src="<?php echo e(asset($cart->image)); ?>" alt="/"
                                                         style="width: 40px;height: 40px">
                                                </a>
                                                <div class="caption">
                                                    <a class="product-name" href=""
                                                       title="<?php echo e($cart->name); ?>"><?php echo e($cart->name); ?></a>
                                                    <span class="product-price"><?php echo e($cart->total); ?>

                                                        x <?php echo e(number_format($cart->price)); ?></span>
                                                </div>
                                                
                                                <a onclick="return remove(<?php echo e($cart->id); ?>);" href="javascript:void(0)"
                                                   class="btn btn-remove"><i class="fa fa-trash fa-lg"></i>
                                                </a>
                                                
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <div class="products-buttons text-center">
                                    <a href="<?php echo e(URL::action('FontEnd_Controller\CartController@getCart')); ?>"
                                       class="btn btn-primary-color2 btn-sm">Tới giỏ hàng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
    function remove(id) {
        if (confirm("Bạn có muốn xóa!")) {
            $.ajax({
                url: "<?php echo e(URL::action('FontEnd_Controller\CartController@removeProduct')); ?>",
                type: "post",
                dateType: "json",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    id: id,

                },
                success: function (result) {
                    location.reload();
                    alert(result.mess);
                }
            });
        } else {
            return false;
        }
    }
</script>
