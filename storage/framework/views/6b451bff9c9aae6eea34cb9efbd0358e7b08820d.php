<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
    <?php echo $__env->make('font-end.layout.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('font-end-content'); ?>
    <section class="section-shopping-cart">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 cart-table">
                    <form method="post"
                          action="<?php echo e(URL::action('FontEnd_Controller\CartController@successOrder',$cookie)); ?>"
                          id="formValidate" class="form-horizontal"
                          enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <h4>Địa chỉ nhận hàng</h4>
                        <div class="form-order" style="padding: 20px;background: #f7f7f7;">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control"
                                                       placeholder="Họ tên (*)" name="name" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control"
                                                       placeholder="Email (*)" name="email" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control"
                                                       placeholder="Số điện thoại (*)" name="phone" id="form-subject"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control"
                                                       placeholder="Địa chỉ (*)" name="address" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <textarea type="text" class="form-control"
                                                          placeholder="Message" name="message" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <small>(*) Các trường bắt buộc nhập.</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-primary-color2 btn-lg" style="width: 100%">Thanh toán
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6 cart-table">
                    <h4>Đơn hàng</h4>
                    <table class="table" style="background: #f7f7f7;border-bottom: 1px solid #e3e3e3">
                        <?php  $total = 0;  ?>
                        <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td width="15%"><img src="<?php echo e(asset($cart->image)); ?>" alt="/"
                                                     style="width: 40px;height: 40px"></td>
                                <td class="tt-name"><?php echo e($cart->name); ?></td>
                                <td>
                                    <?php if($cart->price == -1): ?>
                                        <?php echo e($cart->total); ?> x  0
                                    <?php else: ?>
                                        <?php echo e($cart->total); ?> x <?php echo e(number_format($cart->price)); ?>

                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php  $total = $total + ($cart->price * $cart->total);  ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                    <table class="table" style="background: #f7f7f7;border-bottom: 1px solid #e3e3e3">
                        <tr>
                            <td style="text-align: center"><span style="padding-right: 150px">Tổng thanh toán:</span>
                                <span style="padding-right: 50px;font-size: 18px;font-weight: bold"><?php echo e(number_format($total)); ?>

                                    đ</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('themes/js/jquery-1.11.2.min.js')); ?>"></script>
    <script>
        $(document).ready(function () {
            //called when key is pressed in textbox
            $("#form-subject").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('font-end.layout.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>