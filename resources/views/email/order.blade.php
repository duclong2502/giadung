<style>
    .table-bordered, .table-bordered tr, .table-bordered th, .table-bordered td {
        border: 1px solid #000;
    }
</style>


<h3>Chúc mừng bạn đã đặt hàng thành công tại Gia dụng MAG!</h3>
<h5>Mã code của bạn: <br>{{$code}}</h5>

<div class="table-giohang table-responsive">
    <table border="1" class="table table-bordered table-middle table-striped table-condensed flip-content"
           style="border: 1px solid #000">
        <thead class="flip-content">
        <tr style="border: 1px solid #000">
            <th style="border: 1px solid #000"> Tên sản phẩm</th>
            <th style="border: 1px solid #000"> Đơn giá</th>
            <th style="border: 1px solid #000"> Số lượng</th>
            <th style="border: 1px solid #000"> Thành tiền</th>
        </tr>
        </thead>
        <tbody>
        @if(count($list) == 0)
            <tr>
                <td colspan="5">Chưa có sản phẩm nào</td>
            </tr>
        @else
            <?php $total_price_sum = 0; ?>
            @foreach($list as $item)
                <tr id="cart-item-{{$item->id}}">
                    <td style="text-align: left;">{{$item->name}}</td>
                    <td style="color: #E15517;text-align: right;">
                        @if($item->price != 0)
                        {{number_format($item->price)}}
                        @else
                            Giá liên hệ
                        @endif
                    </td>
                    <td style="color: #E15517;text-align: right;"> {{$item->total}}</td>

                    <td style="color: #E15517;text-align: right;">
                        {{number_format($item->price * $item->total)}}
                    </td>

                    <?php $total_price_sum = ($item->price * $item->total) + $total_price_sum ?>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@if(count($list) > 0)
    <div class="tongthanhtoan">
        <span>Tổng thanh toán (VNĐ):</span><span class="money">
                {{number_format($total_price_sum)}} VNĐ
        </span>
    </div>
@endif