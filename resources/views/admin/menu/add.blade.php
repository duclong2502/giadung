@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\MenuController@index')}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action('Cms\MenuController@doAdd')}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tên danh mục
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" name="name" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Danh mục cha
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="parent_id">
                                            <option value="0">Chọn danh mục cha</option>
                                            @foreach($menu as $ct)
                                                <option value="{{$ct->id}}">{{$ct->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Loại menu
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="menu_group_id">
                                            <option value="0">Chọn loại menu</option>
                                            @foreach($menu_group as $mg)
                                                <option value="{{$mg->id}}">{{$mg->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status" value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Loại
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="type">
                                            <option value="1">Link tùy biến</option>
                                            <option value="2">Danh mục sản phẩm</option>
                                            <option value="3">Bài viết</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Link tùy biến
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" name="link" id="link_customer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Danh mục sản phẩm
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="link" id="product_category" disabled>
                                            <option value="0">Chọn danh mục</option>
                                            @foreach($category as $p)
                                                <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Bài viết
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="link" id="post_category" disabled>
                                            <option value="0">Chọn bài viết</option>
                                            @foreach($post as $p)
                                                <option value="{{$p->id}}">{{$p->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section("script")
    <script>
        $("select[name=type]").change(function () {

            if ($(this).val() == 1) {
                $('#product_category').attr('disabled', 'disabled');
                $('#post_category').attr('disabled', 'disabled');
                $('#link_customer').attr('disabled', false);
            }
            if ($(this).val() == 2) {
                $('#product_category').attr('disabled', false);
                $('#post_category').attr('disabled', 'disabled');
                $('#link_customer').attr('disabled', 'disabled');
            }
            if ($(this).val() == 3) {
                $('#product_category').attr('disabled', 'disabled');
                $('#post_category').attr('disabled', false);
                $('#link_customer').attr('disabled', 'disable');
            }
        })
    </script>
@endsection
