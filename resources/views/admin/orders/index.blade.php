@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-pane active" id="portlet_tab1">
                        <form class="form-horizontal" style="margin-top: 10px">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tên khách hàng<span
                                                    class="font-red"></span> </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control"
                                                   placeholder="Tên khách hàng" name="name"
                                                   value="{{old("name")}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Số điện thoại
                                            <span class="font-red"></span> </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"
                                                   placeholder="Số điện thoại" name="phone"
                                                   value="{{old("phone")}}">
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Từ ngày<span
                                                    class="font-red"></span> </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="datepicker1"
                                                   placeholder="Đặt từ ngày" name="start_date"
                                                   value="{{old("start_date")}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Đến ngày
                                            <span class="font-red"></span> </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="datepicker2"
                                                   placeholder="Đến ngày" name="end_date"
                                                   value="{{old("end_date")}}">
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 20px">
                                <div class="col-lg-12 text-center form-btn-action">
                                    <button type="submit" class="btn btn-default bold">
                                        <i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên khách hàng</th>
                            <th> Địa chỉ</th>
                            <th> Email</th>
                            <th> Số điện thoại</th>
                            <th> Lời nhắn</th>
                            <th> Trạng thái</th>
                            <th colspan="2"> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $key => $item)
                            <tr>
                                <td width="50px">{{($orders->currentPage()-1)*$orders->perPage()+$key+1}}</td>
                                <td>{{$item->customer_name}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->message}}</td>

                                <td>
                                    @if($item->status == "0")
                                        <button data-id="{{$item->id}}"
                                                class="btn btn-circle btn-xs default change-status"
                                        >Đơn hàng mới <i
                                                    class="fa fa-check" aria-hidden="true"></i></button>
                                    @elseif ($item->status == "1")
                                        <button data-id="{{$item->id}}"
                                                class="btn btn-circle btn-xs yellow change-status"
                                        >Đã chuyển về kho <i
                                                    class="fa fa-check" aria-hidden="true"></i></button>
                                    @elseif ($item->status == "2")
                                        <button data-id="{{$item->id}}"
                                                class="btn btn-circle btn-xs blue-dark change-status"
                                        >Đang phát <i
                                                    class="fa fa-check" aria-hidden="true"></i></button>
                                    @elseif ($item->status == "3")
                                        <button data-id="{{$item->id}}"
                                                class="btn btn-circle btn-xs green change-status" disabled
                                        >Hoàn thành <i class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                    @elseif ($item->status == "4")
                                        <button data-id="{{$item->id}}"
                                                class="btn btn-circle btn-xs red change-status"
                                        >Đã hủy <i class="fa fa-check"
                                                   aria-hidden="true"></i></button>
                                    @endif
                                </td>
                                <td class="numeric" width="50px">
                                    <a href="{{URL::action('Cms\OrdersController@detail',$item->id)}}"
                                       class="btn green-dark" title="Chi tiết"> <i class="fa fa-info"></i></a>
                                </td>
                                <td class="" width="50px">
                                    <a href="{{URL::action('Cms\OrdersController@delete',$item->id)}}"
                                       onclick="return confirm('Xác nhận xóa?')"
                                       class="btn red"><i class="fa fa-trash" title="Xóa"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$orders->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
            <!-- Modal -->
            <div id="modal-change-status" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">TRẠNG THÁI ĐƠN HÀNG</h4>
                        </div>
                        <div class="modal-body">
                            <form style="padding-left: 50px" method="post"
                                  action="{{URL::action('Cms\OrdersController@updateStatus')}}">
                                {{ csrf_field() }}
                                <input type="hidden" id="hidden" name="id"/>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" value="0">Đơn hàng mới</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" value="1">Đã chuyển về kho</label>
                                </div>
                                <div class="radio disabled">
                                    <label><input type="radio" name="optradio" value="2">Đang phát</label>
                                </div>
                                <div class="radio disabled">
                                    <label><input type="radio" name="optradio" value="3">Hoàn thành</label>
                                </div>
                                <div class="radio disabled">
                                    <label><input type="radio" name="optradio" value="4">Đã hủy</label>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success">Duyệt
                                    </button>
                                    <a class="btn btn-default" data-dismiss="modal">Đóng</a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section("script")
    <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script type="text/javascript">

        $("#datepicker1").datepicker({
            format: "dd/mm/yyyy"
        });
        $("#datepicker2").datepicker({
            format: "dd/mm/yyyy"
        });
    </script>
    <script>
        $('.change-status').click(function () {
            $('#modal-change-status').modal();
            var id = $(this).data("id");
            $('#hidden').val(id);
        });

    </script>
@endsection