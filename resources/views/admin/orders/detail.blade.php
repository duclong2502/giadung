@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\OrdersController@index')}}" class="btn btn-default">
                            <i class="fa"></i> Quay lại</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                            <thead class="flip-content">
                            <tr>
                                <th> STT</th>
                                <th> Tên SP</th>
                                <th> Ảnh SP</th>
                                <th> Số lượng</th>
                                <th> Đơn giá</th>
                                <th> Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_price_sum = 0; ?>
                            @foreach ($detail as $key => $item)
                                <tr>
                                    <td width="50px">{{($detail->currentPage()-1)*$detail->perPage()+$key+1}}</td>
                                    <td>{{$item->product_name}}</td>
                                    <td><img src="{{asset($item->image)}}" width="100px"></td>
                                    <td>{{$item->amount}}</td>
                                    <td>{{number_format($item->price)}}</td>
                                    <td>{{number_format($item->amount * $item->price )}}</td>
                                    <?php $total_price_sum = ($item->price * $item->amount) + $total_price_sum?>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="tongthanhtoan" style="text-align: right;padding-right: 170px">
                            <span style="font-weight: bold; font-size: 18px">Tổng thanh toán (VNĐ):  </span><span
                                    class="money" style="font-size: 18px">{{number_format($total_price_sum)}}
                                VNĐ</span>
                        </div>
                        <div align="center">
                            {{$detail->links()}}
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
        </div>
        @endsection
        @section("script")
            <script src="/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
                    type="text/javascript"></script>
            <script type="text/javascript">

                $("#datepicker1").datepicker({
                    format: "dd/mm/yyyy"
                });
                $("#datepicker2").datepicker({
                    format: "dd/mm/yyyy"
                });
            </script>
@endsection