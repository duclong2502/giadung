@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\CategoryTermController@index')}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action('Cms\CategoryTermController@doAdd')}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tên danh mục
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="nameNews" type="text" class="form-control" name="name" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Liên kết
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="slug_name" type="text" class="form-control" name="link" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Danh mục cha
                                        <span class="required"> </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="parent_id">
                                            <option value="0">Chọn danh mục cha</option>
                                            @foreach($category_term as $ct)
                                                <option value="{{$ct->id}}">{{$ct->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Loại menu
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="menu_group_id">
                                            <option value="0">Chọn loại menu</option>
                                            @foreach($menu_group as $mg)
                                                <option value="{{$mg->id}}">{{$mg->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status" value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Ảnh
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="file" class="form-control" name="image1"
                                                   accept="image/x-png,image/gif,image/jpeg" required>
                                        </div>
                                        <span style="color: #27a4b0;font-size: 12px">Gợi ý: kích thước ảnh 848x290</span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-2">Mô tả
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-10">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            {{--<input type="text" class="form-control" name="description" autofocus required>--}}
                                            <textarea id="description" class="form-control"
                                                      placeholder="Nội dung" name="description"
                                                      required autofocus></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section("script")
    <script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/ckfinder/ckfinder.js" type="text/javascript"></script>
    <script src="/js/changetoslug.js"></script>

    <script>
        CKEDITOR.replace('description');
    </script>
    <script>
        $(document).ready(function () {
            $('#nameNews').keyup(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
            $('#nameNews').blur(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
        });
    </script>
@endsection
