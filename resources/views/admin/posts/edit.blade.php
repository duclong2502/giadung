@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\PostsController@index")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action("Cms\PostsController@doEdit",$post->id)}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tiêu đề
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="nameNews" type="text" class="form-control"
                                                   placeholder="Tiêu đề" name="title" value="{{$post->title}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">URL
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="slug_name" type="text" class="form-control"
                                                   placeholder="URL" name="slug" autofocus readonly value="{{$post->slug}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control"
                                                      placeholder="Mô tả" name="description"  autofocus>{{$post->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Ảnh
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <img src="{{asset($post->image)}}" width="50%">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="file" class="form-control" name="image1"  accept="image/x-png,image/gif,image/jpeg" >
                                        </div>
                                        <span style="color: #27a4b0;font-size: 12px">Gợi ý: kích thước ảnh 540x380</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status" @if($post->status == 1) checked @endif value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tiêu đề SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="title_seo" value="{{$post->title_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa chính
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="primary_key_seo" value="{{$post->primary_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa phụ
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="sub_key_seo" value="{{$post->sub_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Đường dẫn SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="link_seo" value="{{$post->link_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control"
                                                      placeholder="" name="description_seo">{{$post->description_seo}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>









                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nội dung</label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <textarea id="content" type="text" class="form-control"
                                                          placeholder="Nội dung" name="content"
                                                          required autofocus>{{$post->content}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section("script")
    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/ckfinder/ckfinder.js" type="text/javascript"></script>
    <script src="/js/changetoslug.js"></script>

    <script>
        CKEDITOR.replace('content');
    </script>
    <script>
        $(document).ready(function () {
            $('#nameNews').keyup(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
            $('#nameNews').blur(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
        });
    </script>
@endsection