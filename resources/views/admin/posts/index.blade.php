@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\PostsController@add')}}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tiêu đề</th>
                            <th> Mô tả</th>
                            <th> Ảnh</th>
                            <th colspan="2"> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $key => $item)
                            <tr>
                                <td width="50px">{{($posts->currentPage()-1)*$posts->perPage()+$key+1}}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->description}}</td>
                                <td>
                                    <img src="{{asset($item->image)}}" width="20%">
                                </td>

                                <td class="numeric" width="50px">
                                    <a href="{{URL::action('Cms\PostsController@edit',$item->id)}}"
                                       class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>
                                </td>
                                <td class="" width="50px">
                                    <a href="{{URL::action('Cms\PostsController@delete',$item->id)}}"
                                       onclick="return confirm('Xác nhận xóa?')" title="Xóa"
                                       class="btn red"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$posts->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection