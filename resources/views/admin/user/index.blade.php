@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\AdminController@add')}}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên đăng nhập</th>
                            <th> Email</th>
                            <th> Level</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($admin as $key => $item)
                            <tr>
                                <td width="50px">{{($admin->currentPage()-1)*$admin->perPage()+$key+1}}</td>
                                <td>{{$item->username}}</td>
                                <td>{{$item->email}}</td>
                                <td>
                                    @if($item->level == "0")
                                        <button class="btn btn-circle btn-xs green change-status">User</button>
                                    @else
                                        <button class="btn btn-circle btn-xs default change-status">Admin</button>
                                    @endif
                                </td>
                                <td class="numeric" width="50px">
                                    {{--<a href="{{URL::action('Cms\AdminController@edit',$item->id)}}"--}}
                                       {{--class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>--}}
                                    <a href="{{URL::action("Cms\AdminController@delete", $item->id)}}"
                                       onclick="return confirm('Xác nhận xóa?')"
                                       class="btn red"><i class="fa fa-trash" title="Xóa"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$admin->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection