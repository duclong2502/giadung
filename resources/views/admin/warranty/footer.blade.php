<div class="b-hr">
    <hr>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="b-features-wrapper">
                <div class="b-store-features clearfix wow fadeInUp">
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-thumbs-up"></i>
                            </div>
                            <div class="feature-info">
                                <p>Bảo hành đầy đủ</p>
                                <p>Đọc về bảo hành của chúng tôi</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <div class="feature-info">
                                <p>Dịch vụ vận chuyển</p>
                                <p>Dịch vụ vận chuyển chu đáo</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-commenting"></i>
                            </div>
                            <div class="feature-info">
                                <p>Cập nhật liên tục</p>
                                <p>Cập nhật thông tin hàng ngày</p>
                            </div>
                        </div>
                    </div>
                    <div class="b-feature-holder col-sm-3">
                        <div class="feature-block">
                            <div class="feature-icon">
                                <i class="fa fa-headphones"></i>
                            </div>
                            <div class="feature-info">
                                <p>24/7 Support</p>
                                <p>Liên hệ với chúng tôi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="style-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="b-logo">
                    <a href="/">
                        <img src="/themes/images/logo.png">
                    </a>
                </div>
                <div class="b-footer-contacts wow pulse nimated">
                    {!! $footer !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="row">
                    <div class="b-footer-menu clearfix">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-menu-item wow pulse nimated">
                                <div class="heading-line">Menu</div>
                                <ul class="list-unstyled">
                                    @foreach($categories_footer as $cf)
                                        <li><a href="#">{{$cf->name}}</a></li>
                                    @endforeach
                                    <li><a href="/tin-tuc">Tin tức</a></li>
                                    <li><a href="/lien-he">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-menu-item wow pulse nimated">
                                <div class="heading-line">Mua hàng tại EUS</div>
                                <ul class="list-unstyled">
                                    <li><a href="{{URL::action('FontEnd_Controller\CartController@getCart')}}">Giỏ
                                            hàng</a></li>
                                    <li><a href="{{URL::action('FontEnd_Controller\HomeController@getWarranty')}}">Chế
                                            độ bảo hành</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="b-latest-tweets wow pulse nimated">
                                <div class="heading-line">Fanpage</div>
                                <div class="fb-page" data-href="{{$fanpage_facebook}}"
                                     data-tabs="timeline" data-height="200" data-small-header="false"
                                     data-adapt-container-width="true"
                                     data-hide-cover="false" data-show-facepile="false">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="b-footer-add">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="clearfix">
                        <div class="b-copy pull-left wow fadeInLeft">
                            <p>
                                Copyright © 2015 <a href="#">giadungeus.com</a>
                            </p>
                        </div>
                        <div class="b-payments pull-right">
                            <ul class="list-unstyled wow fadeInRight">
                                <li>
                                    <img src="/themes/media/paycards/1.jpg" class="img-responsive" alt="/">
                                </li>
                                <li><img src="/themes/media/paycards/2.jpg" class="img-responsive" alt="/"></li>
                                <li><img src="/themes/media/paycards/3.jpg" class="img-responsive" alt="/"></li>
                                <li><img src="/themes/media/paycards/4.jpg" class="img-responsive" alt="/"></li>
                                <li><img src="/themes/media/paycards/5.jpg" class="img-responsive" alt="/"></li>
                                <li><img src="/themes/media/paycards/6.jpg" class="img-responsive" alt="/"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=1486572324821329&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>