@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\ProducerController@index")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action('Cms\ProducerController@doEdit',$producer->id)}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tên NXS
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="nameNews" type="text" class="form-control" name="name" value="{{$producer->name}}" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Logo
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <img src="{{asset($producer->image)}}" width="50%">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="file" class="form-control" name="image1" accept="image/x-png,image/gif,image/jpeg">
                                        </div>
                                        <span style="color: #27a4b0;font-size: 12px">Gợi ý: kích thước ảnh 260x248</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control" name="description">{{$producer->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status" @if($producer->status == 1) checked @endif value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Slug
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" id="slug_name" class="form-control" name="slug" readonly value="{{$producer->slug}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-3">Danh mục cha
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="category_id">
                                            @foreach ($category as $cat)
                                                <option value="{{$cat->id}}" @if($producer->category_id == $cat->id) selected @endif>{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section("script")
    <script src="/js/changetoslug.js"></script>
    <script>
        $(document).ready(function () {
            $('#nameNews').keyup(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
            $('#nameNews').blur(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
        });
    </script>
@endsection
