@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\SlidersController@index")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action('Cms\SlidersController@doEdit',$slider->id)}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tên slider
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{$slider->name}}" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Liên kết
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" name="link"
                                                   value="{{$slider->link}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Ảnh
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <img src="{{asset($slider->image)}}" width="50%">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="file" class="form-control" name="image1" accept="image/x-png,image/gif,image/jpeg" >
                                        </div>
                                        <span style="color: #27a4b0;font-size: 12px">Gợi ý: kích thước ảnh 1600x570</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status"
                                                           @if($slider->status == 1) checked @endif value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tiêu đề SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="title_seo" value="{{$slider->title_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa chính
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="primary_key_seo" value="{{$slider->primary_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa phụ
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="sub_key_seo" value="{{$slider->sub_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Đường dẫn SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="link_seo" value="{{$slider->link_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control"
                                                      placeholder="" name="description_seo">{{$slider->description_seo}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
