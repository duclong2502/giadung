@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action('Cms\ProductsController@index')}}" class="btn btn-default">
                            <i class="fa fa-back"></i> Quay lại</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <br>
                        <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                            <thead class="flip-content">
                            <tr>
                                <th> STT</th>
                                <th> Hình ảnh</th>
                                <th> Trạng thái</th>
                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $key => $item)
                                <tr>
                                    <td width="50px">{{($list->currentPage()-1)*$list->perPage()+$key+1}}</td>
                                    <td><img src="{{asset($item->image)}}" width="100px"></td>
                                    <td>
                                        <button class="btn btn-circle btn-xs green change-status">Đã duyệt</button>
                                    </td>
                                    <td class="" width="50px">
                                        <a href="{{URL::action('Cms\ProductsController@getDeleteSub',$item->id)}}"
                                           onclick="return confirm('Xác nhận xóa?')"
                                           class="btn red"><i class="fa fa-trash" title="Xóa"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div align="center">
                            {{$list->links()}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
