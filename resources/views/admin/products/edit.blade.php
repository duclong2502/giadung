@extends("admin.layout.master")

@section("content")
    <h1></h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">
                        <a href="{{URL::action("Cms\ProductsController@index")}}" class="btn btn-default">
                            Quay lại <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" action="{{URL::action("Cms\ProductsController@doEdit", $product->id)}}"
                          id="formValidate"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                Bạn chưa điền thông tin.
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tên sản phẩm
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="nameNews" type="text" class="form-control"
                                                   placeholder="Tên sản phẩm" name="name" value="{{$product->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">URL
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="slug_name" type="text" class="form-control"
                                                   placeholder="URL" name="slug" autofocus readonly value="{{$product->slug}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control"
                                                      placeholder="Mô tả" name="description" autofocus>{{$product->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Ảnh
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <img src="{{asset($product->image)}}" width="50%">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="file" class="form-control" name="image1" accept="image/x-png,image/gif,image/jpeg" >
                                        </div>
                                        <span style="color: #27a4b0;font-size: 12px">Gợi ý: kích thước ảnh 260x248</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Trạng thái</label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" name="status" @if($product->status == 1) checked @endif value="1">Bật
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-3">Danh mục
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="category_id">
                                            @foreach($category as $cat)
                                                <option value="{{$cat->id}}" @if($cat->id == $product->category_id) selected @endif>{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Giá bán
                                        <span class="required"> * </span>
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input id="lien-he" type="checkbox" name="price" value="-1"
                                                           @if($product->price_sale == 0) checked @endif>Liên hệ
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Giá gốc
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="gia_goc" type="text" class="form-control child"
                                                   placeholder="Giá gốc" name="price" autofocus @if($product->price_sale == 0) disabled
                                                   @endif value="{{$product->price}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Giá sale
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="price" type="text" class="form-control child"
                                                   placeholder="Giá sale" name="price_sale" @if($product->price_sale == 0) disabled
                                                   @endif autofocus value="{{$product->price_sale}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group margin-top-20">
                                    <label class="control-label col-md-3">Hãng sản xuất
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="producer_id">
                                            @foreach ($producer as $prc)
                                                <option value="{{$prc->id}}" @if($prc->id == $product->producer_id) selected @endif>{{$prc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Tiêu đề SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="title_seo" value="{{$product->title_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa chính
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="primary_key_seo" value="{{$product->primary_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Từ khóa phụ
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="sub_key_seo" value="{{$product->sub_key_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Đường dẫn SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control"
                                                   placeholder="" name="link_seo" value="{{$product->link_seo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group  margin-top-20">
                                    <label class="control-label col-md-3">Mô tả SEO
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea type="text" class="form-control"
                                                      placeholder="" name="description_seo">{{$product->description_seo}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>










                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ảnh phụ</label>
                                        <div class="col-md-9">
                                            <div class="dropzone" id="my-dropzone" name="myDropzone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="innerHTML">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Thông số kĩ thuật</label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <textarea id="thong_so" type="text" class="form-control" name="thong_so"
                                                          required autofocus>{{$product->thong_so}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Bảo hành</label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <textarea id="bao_hanh" type="text" class="form-control" name="bao_hanh"
                                                          required autofocus>{{$product->bao_hanh}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-body margin-top-20">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Thông tin</label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <textarea id="thong_tin" type="text" class="form-control"
                                                          name="thong_tin"
                                                          required autofocus>{{$product->thong_tin}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection
@section("script")
    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/ckfinder/ckfinder.js" type="text/javascript"></script>
    <script src="/js/changetoslug.js"></script>
    <script src="/js/money.js"></script>

    <link rel="stylesheet" href="{{ asset('themes/dropzone/dropzone.min.css') }}">
    <script src="{{ asset('themes/dropzone/dropzone.min.js') }}"></script>
    <script type="text/javascript">
        //dropzone
        abc = Dropzone.options.myDropzone = {
            paramName: "nameImageSub",
            url: '{{ route('uploadProImg') }}',

            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            autoProcessQueue: true,
            uploadMultiple: true,
            parallelUploads: 5,
            maxFiles: 10,
            maxFilesize: 5,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            dictFileTooBig: 'Image is bigger than 5MB',
            addRemoveLinks: true,
            successmultiple: function (file, response) {
                $.each(response, function (key, value) {
                    $(".innerHTML").append("<input type='hidden' name='nameImageSub[]' value='" + value + "'>");
                });
            },
            removedfile: function (file) {
                var string = $.parseJSON(file.xhr.response);
                var substring = file.name;
                $.each(string, function (key, value) {
                    if (value.includes(substring) == true) {
                        var name = file.name;
                        name = name.replace(/\s+/g, '-').toLowerCase();
                        $.ajax({
                            type: 'POST',
                            url: '/product/deleteImg/' + value,
                            headers: {
                                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                            },
                            data: "id=" + name,
                            dataType: 'html',
                            success: function (data) {
                                $("#msg").html(data);
                            }
                        });
                        var _ref;
                        if (file.previewElement) {
                            if ((_ref = file.previewElement) != null) {
                                _ref.parentNode.removeChild(file.previewElement);
                            }
                        }

                    }
                });
                return this._updateMaxFilesReachedClass();
            },
            previewsContainer: null,
            hiddenInputContainer: "body",
        }
    </script>

    <script>
        CKEDITOR.replace('thong_so');
        CKEDITOR.replace('bao_hanh');
        CKEDITOR.replace('thong_tin');
    </script>
    <script>
        $(document).ready(function () {
            $('#nameNews').keyup(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
            $('#nameNews').blur(function () {
                $('#slug_name').val(ChangeToSlug($(this).val()));
            });
        });
    </script>
    <script>
        $(function () {
            $('#formValidate').on('keydown', '.child', function (e) {
                -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
            });
        });
        $('#price').simpleMoneyFormat();
        $('#gia_goc').simpleMoneyFormat();
    </script>



    <script>
        $(this).click(function () {
            if ($('#lien-he').is(':checked')) {

                $('#price').attr('disabled', true); //enable input
                $("#price").val("");

                $('#gia_goc').attr('disabled', true); //enable input
                $("#gia_goc").val("");
//                $('.time_element').removeAttr('disabled'); //enable input

            } else {
//                $(".time_element").val("");
                $('#gia_goc').removeAttr('disabled'); //disable input
                $('#price').removeAttr('disabled'); //disable input
            }
        });
    </script>
@endsection