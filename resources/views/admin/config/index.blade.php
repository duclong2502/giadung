@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Key</th>
                            <th> Content</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($config as $key => $item)
                            <tr>
                                <td width="50px">{{($config->currentPage()-1)*$config->perPage()+$key+1}}</td>
                                <td>{{$item->key}}</td>
                                <td>{{$item->content}}</td>
                                <td class="numeric" width="50px">
                                    <a href="{{URL::action('Cms\ConfigController@edit',$item->id)}}"
                                       class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$config->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection