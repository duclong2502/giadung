@extends("admin.layout.master")
@section("styles")

@endsection
@section("content")
    <h1 class="page-title"> {{$title}}
        <small>{{$title_description}}</small>
    </h1>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> {{$title}}</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-middle table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th> STT</th>
                            <th> Tên khách hàng</th>
                            <th> Email</th>
                            <th> Số điện thoại</th>
                            <th> Lời nhắn</th>
                            <th> Trạng thái</th>
                            <th> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contact as $key => $item)
                            <tr>
                                <td width="50px">{{($contact->currentPage()-1)*$contact->perPage()+$key+1}}</td>
                                <td>{{$item->customer_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->message}}</td>

                                <td>
                                    @if($item->status == "0")
                                        <button class="btn btn-circle btn-xs default change-status">Chưa duyệt</button>
                                    @else
                                        <button class="btn btn-circle btn-xs green change-status">Đã duyệt</button>
                                    @endif
                                </td>
                                {{--<td class="numeric" width="50px">--}}
                                {{--<a href="#"--}}
                                {{--class="btn yellow-gold" title="Sửa"> <i class="fa fa-edit"></i></a>--}}
                                {{--</td>--}}
                                <td class="" width="50px">
                                    <a href="{{URL::action('Cms\ContactController@delete',$item->id)}}"
                                       onclick="return confirm('Xác nhận xóa?')" title="Xóa"
                                       class="btn red"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div align="center">
                        {{$contact->links()}}
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection