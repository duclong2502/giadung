@extends('font-end.layout.index')
@section('title')
    {{ $title }}
@stop
@section('seo')
    @include('font-end.layout.seo')
@endsection
@section('font-end-content')
    <section class="section-home home-2">
        <div class="header-slider background-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 nopadding-right">
                        <div class="sidebar-menu">
                            <ul class="parent-menu">
                                @foreach($categories_sidebar as $key => $category)
{{--                                    @if($key < 7)--}}
                                        <li class="li-parent">
                                            <a href="javascript:void(0)">{{ $category->name}}</a>
                                            <span style="font-size: 11px">{!! $category->description !!}</span>
                                            @if(count($category->childs))
                                                @include('font-end.layout.manageChild2',['childs' => $category->childs])
                                            @endif
                                        </li>
                                    {{--@endif--}}
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-10 nopadding-left">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <?php $i = 0;?>
                                @foreach($sliders as $key=> $im)
                                    <li data-target="#myCarousel" data-slide-to="{{$i}}"
                                        @if($key==0)
                                        class="active"
                                            @endif>
                                    </li>
                                @endforeach
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @foreach($sliders as $key => $slider)
                                    <div class="item @if($key==0) active @endif"
                                         style="background-image: url({{asset($slider->image)}})">
                                        {{--                                    <img src="{{asset($slider->image)}}" alt="{{$slider->name}}">--}}
                                    </div>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!empty($ads))
            <div class="home-banner-ads background-gray banner-header">
                <div class="container">
                    <div class="row">
                        @foreach($ads as $a)
                            <div class="col-lg-4 col-xs-4 col-sm-4">
                                <img src="{{asset($a->image)}}"/>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="home-product-business background-yellow">
            <div class="container">
                <h1>Sản phẩm bán chạy</h1>
                <div class="row">
                    <div class="b-related by-brands column-slider wow bounce animated">
                        @foreach($products as $key => $product)
                            <div class="related-item col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="b-item-card">
                                    @if($product->price != 0 && $product->price_sale != 0)
                                        <div class="special-plank sale">
                                            <span>- {{ceil((($product->price - $product->price_sale)/$product->price) * 100)}}
                                                %</span>
                                        </div>
                                    @endif
                                    <div class="image">
                                        <a href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">
                                            <img src="{{asset($product->image)}}"
                                                 class="img-responsive center-block" alt="/"
                                                 style="width: 260px; height: 248px !important;">
                                        </a>
                                        {{--<div class="image-add-mod">--}}
                                            {{--<div class="add-description">--}}
                                                {{--<div>--}}
                                                    {{--{{$product->description}}--}}
                                                    {{--<br>--}}
                                                    {{--<a href="{{asset($product->image)}}"--}}
                                                       {{--data-gal="prettyPhoto" title="6s plus"--}}
                                                       {{--class="btn btn-lightbox btn-default-color1 btn-sm">--}}
                                                        {{--<i class="fa fa-search-plus fa-lg"></i>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="card-info">
                                        <div class="caption">
                                            <p class="name-item">
                                                <a class="product-name"
                                                   href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">
                                                    {{$product->name}}</a>
                                            </p>
                                            @if($product->price_sale != 0)
                                            <div class="product-price">{{number_format($product->price_sale)}}
                                                VNĐ
                                            </div>
                                            <del>{{number_format($product->price)}}</del>
                                            @else
                                                <br/>
                                                <div class="product-price">Giá liên hệ</div>
                                            @endif
                                            <button type="button" name="addcart"
                                                    onclick="return addtocart({{$product->id}});"
                                                    class="btn btn-cart-color2"><i
                                                        class="fa fa-shopping-cart fa-lg"></i> Mua nhanh
                                            </button>

                                            {{--<div class="">--}}
                                                {{--<div class="row">--}}
                                                    {{--<php> @dd($pr)</php>--}}
                                                    {{--<div class="col-sm-6 cart-table">--}}
                                                        {{--<form method="post"--}}
                                                              {{--action="{{URL::action('FontEnd_Controller\CartController@successOrder',$product)}}"--}}
                                                              {{--id="formValidate" class="form-horizontal"--}}
                                                              {{--enctype="multipart/form-data">--}}
                                                            {{--{{ csrf_field() }}--}}
                                                            {{--<h4>Địa chỉ nhận hàng</h4>--}}
                                                            {{--<div class="form-order" style="padding: 20px;background: #f7f7f7;">--}}

                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<div class="col-md-12">--}}
                                                                                {{--<div class="input-icon right">--}}
                                                                                    {{--<i class="fa"></i>--}}
                                                                                    {{--<input type="text" class="form-control"--}}
                                                                                           {{--placeholder="Họ tên (*)" name="name" value="" required>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<div class="col-md-12">--}}
                                                                                {{--<div class="input-icon right">--}}
                                                                                    {{--<i class="fa"></i>--}}
                                                                                    {{--<input type="text" class="form-control"--}}
                                                                                           {{--placeholder="Email (*)" name="email" value="" required>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<div class="col-md-12">--}}
                                                                                {{--<div class="input-icon right">--}}
                                                                                    {{--<i class="fa"></i>--}}
                                                                                    {{--<input type="text" class="form-control"--}}
                                                                                           {{--placeholder="Số điện thoại (*)" name="phone" id="form-subject"--}}
                                                                                           {{--required>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<div class="col-md-12">--}}
                                                                                {{--<div class="input-icon right">--}}
                                                                                    {{--<i class="fa"></i>--}}
                                                                                    {{--<input type="text" class="form-control"--}}
                                                                                           {{--placeholder="Địa chỉ (*)" name="address" value="" required>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<div class="col-md-12">--}}
                                                                                {{--<div class="input-icon right">--}}
                                                                                    {{--<i class="fa"></i>--}}
                                                                                    {{--<textarea type="text" class="form-control"--}}
                                                                                              {{--placeholder="Message" name="message" rows="4"></textarea>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        {{--<small>(*) Các trường bắt buộc nhập.</small>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="row">--}}
                                                                    {{--<div class="col-lg-6">--}}

                                                                    {{--</div>--}}
                                                                    {{--<div class="col-lg-6">--}}
                                                                        {{--<button class="btn btn-primary-color2 btn-lg" style="width: 100%">Thanh toán--}}
                                                                        {{--</button>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</form>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-sm-6 cart-table">--}}
                                                        {{--<h4>Đơn hàng</h4>--}}
                                                        {{--<table class="table" style="background: #f7f7f7;border-bottom: 1px solid #e3e3e3">--}}
                                                            {{--@php $total = 0; @endphp--}}

                                                                {{--<tr>--}}
                                                                    {{--<td width="15%"><img src="{{asset($product->image)}}" alt="/"--}}
                                                                                         {{--style="width: 40px;height: 40px"></td>--}}
                                                                    {{--<td class="tt-name">{{$product->name}}</td>--}}

                                                        {{--</table>--}}
                                                        {{--<table class="table" style="background: #f7f7f7;border-bottom: 1px solid #e3e3e3">--}}
                                                            {{--<tr>--}}
                                                                {{--<td style="text-align: center"><span style="padding-right: 150px">Tổng thanh toán:</span>--}}
                                                                    {{--<span style="padding-right: 50px;font-size: 18px;font-weight: bold">{{number_format($product->price_sale)}}--}}
                                                                        {{--đ</span></td>--}}
                                                            {{--</tr>--}}
                                                        {{--</table>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="home-product-business background-gray">
            <div class="container">
                <h1>Sản phẩm mới</h1>
                <div class="row">
                    <div class="b-related by-brands column-slider wow bounce animated">
                        @foreach($products_new as $key => $product)
                            <div class="{{$product->category->link}} related-item col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="b-item-card">
                                    @if($key < 4)
                                        <div class="special-plank new">
                                            <span>new</span>
                                        </div>
                                    @endif
                                    <div class="image">
                                        <a href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">
                                            <img src="{{asset($product->image)}}"
                                                 class="img-responsive center-block" alt="/"
                                                 style="width: 260px; height: 248px !important;">
                                        </a>
                                        {{--<div class="image-add-mod">--}}
                                            {{--<div class="add-description">--}}
                                                {{--<div>--}}
                                                    {{--{{$product->description}}--}}
                                                    {{--<br>--}}
                                                    {{--<a href="{{asset($product->image)}}"--}}
                                                       {{--data-gal="prettyPhoto" title="6s plus"--}}
                                                       {{--class="btn btn-lightbox btn-default-color1 btn-sm">--}}
                                                        {{--<i class="fa fa-search-plus fa-lg"></i>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="card-info">
                                        <div class="caption">
                                            <p class="name-item">
                                                <a class="product-name"
                                                   href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">
                                                    {{$product->name}}</a>
                                            </p>
                                            @if($product->price_sale != 0)
                                                <div class="product-price">{{number_format($product->price_sale)}}
                                                    VNĐ
                                                </div>
                                                <del>{{number_format($product->price)}}</del>
                                            @else
                                                <br/>
                                                <div class="product-price">Giá liên hệ</div>
                                            @endif
                                            <button type="button" name="addcart"
                                                    onclick="return addtocart({{$product->id}});"
                                                    class="btn btn-cart-color2"><i
                                                        class="fa fa-shopping-cart fa-lg"></i> Thêm vào giỏ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @if(!empty($ads_footer))
            <div class="home-banner-ads-f background-white">
                <div class="container">
                    <div class="row">
                        @foreach($ads_footer as $ads)
                            <div class="col-lg-4 col-xs-4 col-sm-4">
                                <img src="{{asset($ads->image)}}"/>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </section>





    {{--<div class="b-bestsellers" style="display: none">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12 text-center">--}}
    {{--<div class="tab-content">--}}
    {{--<div role="tabpanel" class="tab-pane active" id="bybrand">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--<div class="b-brand-filters">--}}
    {{--<div>--}}
    {{--<ul class="tags-buttons list-inline by-brands-buttons">--}}
    {{--<li>--}}
    {{--<button data-filter="*" class="btn btn-tag active">all</button>--}}
    {{--</li>--}}
    {{--@foreach($category_parent as $cp)--}}
    {{--<li>--}}
    {{--<button data-filter=".{{$cp->link}}"--}}
    {{--class="btn btn-tag btn-filter"--}}
    {{-->{{$cp->name}} </button>--}}
    {{--</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-sm-12">--}}
    {{--<h3 class="heading-line-long">Sản phẩm</h3>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
    {{--<div class="b-related by-brands column-slider wow bounce animated">--}}
    {{--@foreach($products as $key => $product)--}}
    {{--<div class="{{$product->category->link}} related-item col-xs-6 col-sm-6 col-md-3 col-lg-3">--}}
    {{--<div class="b-item-card">--}}
    {{--<div class="image">--}}
    {{--<a href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">--}}
    {{--<img src="{{asset($product->image)}}"--}}
    {{--class="img-responsive center-block" alt="/"--}}
    {{--style="width: 260px; height: 248px !important;">--}}
    {{--</a>--}}
    {{--<div class="image-add-mod">--}}
    {{--<div class="add-description">--}}
    {{--<div>--}}
    {{--{{$product->description}}--}}
    {{--<br>--}}
    {{--<a href="{{asset($product->image)}}"--}}
    {{--data-gal="prettyPhoto" title="6s plus"--}}
    {{--class="btn btn-lightbox btn-default-color1 btn-sm">--}}
    {{--<i class="fa fa-search-plus fa-lg"></i>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="card-info">--}}
    {{--<div class="caption">--}}
    {{--<p class="name-item">--}}
    {{--<a class="product-name"--}}
    {{--href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">--}}
    {{--{{$product->name}}</a>--}}
    {{--</p>--}}
    {{--<span class="product-price">{{number_format($product->price_sale)}}--}}
    {{--VNĐ</span>--}}
    {{--</div>--}}
    {{--<div class="add-buttons">--}}
    {{--<button type="button" class="btn btn-add btn-add-cart"--}}
    {{--onclick="return addtocart({{$product->id}});"><i--}}
    {{--class="fa fa-shopping-cart"></i></button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="b-home-blog home-blog-mod" style="display: none">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--<h3 class="heading-line-long">tin tức</h3>--}}
    {{--</div>--}}
    {{--<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">--}}
    {{--<div class="row">--}}
    {{--<div class="b-posts-holder clearfix">--}}
    {{--@foreach($news as $key => $new)--}}
    {{--<div class="b-post-preview col-xs-12 col-sm-4 col-md-4 col-lg-4 clearfix @if($key == 0) wow fadeInLeft @elseif($key == 2) wow fadeInRight @endif">--}}
    {{--<div class="post-image">--}}
    {{--<div class="post-img-holder">--}}
    {{--<div>--}}
    {{--<img src="{{asset($new->image)}}"--}}
    {{--class="img-responsive center-block" alt="/"--}}
    {{--style="width: 360px;height: 300px !important;">--}}
    {{--<div class="image-add">--}}
    {{--<a href="{{route('newsDetail',$new->slug)}}"--}}
    {{--class="btn btn-default-color1 btn-sm">xem--}}
    {{--chi tiết</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="post-data">--}}
    {{--<span>{!! date("<b>d/m/20y </b>",strtotime($new->created_at)) !!}</span>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="post-caption">--}}
    {{--<div class="caption">--}}
    {{--<h5 class="heading-line">{{$new->title}}</h5>--}}
    {{--<div class="post-author">--}}
    {{--{{$new->description}}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
@endsection
@section('script')
    {{--<script src="/js/jquery.slimscroll.js"></script>--}}
    {{--<script src="/themes/js/jquery-1.11.2.min.js"></script>--}}
    {{--<script src="/js/jquery.slimscroll.js"></script>--}}
    <script>
        function addtocart(pid) {
            $.ajax({
                url: "{{URL::action('FontEnd_Controller\CartController@addCart')}}",
                type: "get",
                dateType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    pid: pid,
                    count: 1,
                },

                success: function (result) {
                    // alert(result.mess);
                    location.reload();
                }
            });
        }
    </script>
    {{--<script>--}}
        {{--$('.parent-menu').slimscroll({--}}
            {{--height: '100%',--}}
            {{--// width: '300px'--}}
        {{--});--}}
    {{--</script>--}}

@endsection