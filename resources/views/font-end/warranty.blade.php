@extends('font-end.layout.index')
@section('title')
    {{ $title }}
@stop
@section('seo')
    @include('font-end.layout.seo')
@endsection
@section('font-end-content')
    <section class="section-blog-main">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left">Bảo hành</h3>
                        <div class="b-breadcrumbs pull-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                    <div class="b-posts-holder">
                        @foreach ($warranty as $key => $new)
                            <div class="b-post-preview clearfix @if($key>1)wow fadeInUp @endif">
                                <div class="post-image pull-left ">
                                    <div class="post-img-holder">
                                        <div>
                                            <img src="{{asset($new->image)}}" class="img-responsive center-block" alt="{{$new->name}}" style="width: 320px; height: 360px !important;">
                                            <div class="image-add">
                                                <a href="{{asset($new->link)}}"
                                                   class="btn btn-default-color1 btn-sm" target="_blank">chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-data">
                                        <span><i class="fa fa-calendar"></i> {!! date("<b>d/m/20y </b>",strtotime($new->created_at)) !!}</span>
                                    </div>
                                </div>
                                <div class="post-caption pull-left">
                                    <div class="caption">
                                        <h5 class="heading-line">{{$new->name}}</h5>
                                        <div class="post-description">
                                            <p>
                                                {{$new->description}}
                                            </p>
                                        </div>
                                    </div>
                                    <a href="{{asset($new->link)}}" target="_blank">đọc tiếp</a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <nav class="pagination-full clearfix wow fadeInUp">
                        {{$warranty->links()}}

                        @if(count($warranty) > 5)
                            <ul class="pagination pagination-add" id="pagination-huytd">
                                <li>
                                    <a href="{{$warranty->previousPageUrl()}}" aria-label="Previous"
                                       class="prev">Previous</a>
                                </li>
                                <li>
                                    <a href="{{$warranty->nextPageUrl()}}" aria-label="Next" class="next">Next</a>
                                </li>
                            </ul>
                        @endif

                    </nav>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                    <div class="lb-content lb-content-accordion">
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">Sản phẩm nổi bật</h3>
                            @foreach($products as $product)
                                <div class="l-box-content">
                                    <div class="b-latest-rev">
                                        <div class="latest-rev-img pull-left">
                                            <img src="{{asset($product->image)}}" class="img-responsive"
                                                 alt="/" style="width: 85px;height: 85px !important;">
                                        </div>
                                        <div class="latest-rev-caption">
                                            <a class="rev-caption-title" href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">{{$product->name}}</a>
                                            <p>{{$product->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="l-box-mod">
                            <h3 class="heading-line">Tin tức liên quan</h3>
                            @foreach($news_lq as $item)
                                <div class="b-popular-post">
                                    <a class="popular-caption-title" href="{{route('newsDetail',$item->slug)}}">{{$item->title}}</a>
                                    <div class="popular-date">{!! date("<b>d/m/20y </b>",strtotime($item->created_at)) !!}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
