@extends('font-end.layout.index')
@section('title')
    {{ $title }}
@stop
@section('seo')
    @include('font-end.layout.seo')
@endsection
@section('font-end-content')
    <section class="section-category">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left">{{$category_child->name}}</h3>
                        <div class="b-breadcrumbs pull-right">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="/">home</a>
                                </li>

                                <li>
                                    <span>{{$category_child->name}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <form action="" method="get" id="hjhj">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="lb-content lb-content-accordion">
                            <div id="accordion" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3 class="accordion-header-mod">
                                    <span class="heading-line title-accordion-menu-item">Danh mục</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    <ul>
                                        @foreach ($category as $key => $value)
                                            <li class="@if($value->id == $category_child->id) active @endif">
                                                <a href="{{URL::action('FontEnd_Controller\ProductController@getProductCategory',$value->link)}}">
                                                    <i class="fa fa-caret-square-o-right"></i>{{$value->name}}
                                                    <span class="category-counter">[ {{MAGHelper::getTotalProduct($value->id)}}
                                                        ]</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div id="accordion2" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3>
                                    <span class="heading-line title-accordion-menu-item">Hãng sản xuất</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    <ul>
                                        @foreach ($producer as $k => $val)
                                            <li class="@if($producer_child != null && $producer_child->id == $val->id) active @endif">
                                                <a href="{{route('getProductCategory',['cat'=>$slug,'hang'=>$val->slug])}}">
                                                    <i class="fa fa-caret-square-o-right"></i>
                                                    {{$val->name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div id="accordion3" class="accordion-l-box wow fadeInUp enable-accordion" data-active="0"
                                 data-collapsible="true" data-height-style="content">
                                <h3>
                                    <span class="heading-line title-accordion-menu-item">Giá</span>
                                    <span class="accordion-icon"></span>
                                </h3>
                                <div>
                                    {{--<form action="" method="get">--}}

                                    <div class="price-block">
                                        {{--<div id="slider-range"></div>--}}
                                        {{--<input type="text" name="price" id="price-min">--}}
                                        {{--<span class="price-diveder">-</span>--}}
                                        {{--<input type="text" name="price2" id="price-max">--}}
                                        {{--<span><a href="">0 - 2,000,000</a></span>--}}
                                        <ul>
                                            <li>
                                                <a href="{{url()->current().'?p-min=0&p-max=2,000,000'}}" id="a-submit"><i
                                                            class="fa fa-caret-square-o-right"></i>0
                                                    - 2,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="{{url()->current().'?p-min=2,000,000&p-max=5,000,000'}}"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>2,000,000
                                                    - 5,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="{{url()->current().'?p-min=5,000,000&p-max=10,000,000'}}"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>5,000,000
                                                    - 10,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="{{url()->current().'?p-min=10,000,000&p-max=15,000,000'}}"
                                                   id="a-submit"><i class="fa fa-caret-square-o-right"></i>10,000,000
                                                    - 15,000,000đ</a>
                                            </li>
                                            <li>
                                                <a href="{{url()->current().'?p-min=15,000,000&p-max='}}" id="a-submit"><i
                                                            class="fa fa-caret-square-o-right"></i>
                                                    > 15,000,000đ </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <button class="btn btn-default-color1 btn-sm">Lọc</button>
                                    {{--</form>--}}
                                </div>
                            </div>
                        </div>
                        <div class="side-offer wow fadeInUp">
                            {{--<div class="b-offers">--}}
                            {{--<a href="#">--}}
                            {{--<img src="/themes/media/offers/category-b-side.png" class="img-responsive" alt="/">--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            <div class="l-box-mod">
                                <h3 class="heading-line">Tin tức liên quan</h3>
                                @foreach($news_lq as $item)
                                    <div class="b-popular-post">
                                        <a class="popular-caption-title"
                                           href="{{route('newsDetail',$item->slug)}}">{{$item->title}}</a>
                                        <div class="popular-date">{!! date("<b>d/m/20y </b>",strtotime($item->created_at)) !!}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <div class="b-offers wow fadeInUp">
                            {{--<a href="#">--}}
                            {{--<img src="{{asset($category_child->image)}}" class="img-responsive center-block"--}}
                            {{--alt="/">--}}
                            {{--</a>--}}
                        </div>
                        <div class="b-settings">
                            <div class="settings-tools">
                                <h3 class="heading-line pull-left">
                                    {{$category_child->name}}
                                    <span class="settings-counter">[ {{count($products)}} sản phẩm ]</span>
                                </h3>
                                <div class="settings-block pull-right">
                                    <div class="settings-options">
                                        <form action="" method="get" id="hjhj">
                                            <div class="select-block">
                                                <span class="select-title">Sắp xếp theo</span>
                                                <select class="selectpicker" id="select-box" onchange="getval()"
                                                        data-width="134px" name="sort">
                                                    <option>Lọc</option>
                                                    <option value="new" @if(old('sort') == 'new') selected @endif>Mới
                                                        nhất
                                                    </option>
                                                    <option value="min-max"
                                                            @if(old('sort') == 'min-max') selected @endif>Giá thấp - cao
                                                    </option>
                                                    <option value="max-min"
                                                            @if(old('sort') == 'max-min') selected @endif>Giá cao - thấp
                                                    </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="settings-view hidden-md hidden-sm hidden-xs">
                                        <ul id="type-of-display" class="list-unstyled">
                                            {{--<li>--}}
                                            {{--<button class="btn toogle-view grid-list">--}}
                                            {{--<i class="fa fa-th-list fa-fw"></i>--}}
                                            {{--</button>--}}
                                            {{--</li>--}}
                                            <li>
                                                <button class="btn toogle-view grid-3 active-view">
                                                    <i class="fa fa-th-large fa-fw"></i>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-grid">
                            <div class="row">
                                @foreach($products as $key => $product)
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                        <div class="b-item-card">
                                            <div class="image"
                                                 style="background-image: url({{asset($product->image)}})">
                                                <a href="{{asset($product->image)}}" data-gal="prettyPhoto"
                                                   title="6s Plus">
                                                    <img src="{{asset($product->image)}}"
                                                         class="img-responsive center-block" alt="{{$product->name}}"
                                                         style="width: 260px;height: 248px !important;">
                                                    <div class="image-add-mod">
														<span class="btn btn-lightbox btn-default-color1 btn-sm">
															<i class="fa fa-search-plus fa-lg"></i>
														</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="card-info">
                                                <div class="caption">
                                                    <div class="name-item">
                                                        <a class="product-name" style="padding: 0px 10px"
                                                           href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">{{$product->name}}</a>
                                                        <div class="rating">
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                            <span class="star"><i class="fa fa-star"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="card-price-block">
                                                        <span class="price-title">Giá</span>
                                                        @if($product->price_sale != 0)
                                                            <span class="product-price">{{number_format($product->price_sale)}}</span>
                                                            <div class="">
                                                                <del>{{number_format($product->price)}}</del>
                                                            </div>
                                                        @else
                                                            <span class="product-price">Giá liên hệ</span>
                                                        @endif
                                                    </div>
                                                    <div class="product-description">
                                                        <p>{{$product->description}}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="add-buttons">
                                                    <button type="button" class="btn btn-add btn-add-cart"
                                                            onclick="return addtocart({{$product->id}});"><i
                                                                class="fa fa-shopping-cart"></i></button>
                                                    <div class="cart-add-buttons">
                                                        <button type="button" class="btn btn-cart-color1"
                                                                onclick="return addtocart({{$product->id}});"><i
                                                                    class="fa fa-shopping-cart"></i> Thêm vào giỏ
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <nav class="pagination-full clearfix wow fadeInUp">
                                {{$products->links()}}

                                @if(count($products) > 8)
                                    <ul class="pagination pagination-add" id="pagination-huytd">
                                        <li>
                                            <a href="{{$products->previousPageUrl()}}" aria-label="Previous"
                                               class="prev">Previous</a>
                                        </li>
                                        <li>
                                            <a href="{{$products->nextPageUrl()}}" aria-label="Next"
                                               class="next">Next</a>
                                        </li>
                                    </ul>
                                @endif

                            </nav>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('themes/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('js/money.js')}}"></script>
    <script>
        function addtocart(pid) {
            $.ajax({
                url: "{{URL::action('FontEnd_Controller\CartController@addCart')}}",
                type: "get",
                dateType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    pid: pid,
                    count: 1,
                },

                success: function (result) {
                    alert(result.mess);
                    location.reload();
                }
            });
        }

        function getval() {
            $('#select-box').change(function () {
                $('#hjhj').submit();
            });
        }
    </script>
    <script>
        $('#price-max').simpleMoneyFormat();

        $('#a-submit').click(function () {
            $('#haha').submit();
        });
    </script>
@endsection