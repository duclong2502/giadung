@extends('font-end.layout.index')
@section('title')
    {{ $title }}
@stop
@section('seo')
    @include('font-end.layout.seo')
@endsection
@section('font-end-content')
    <section class="section-contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
                    <h3 class="heading-line">
                        Bạn có thắc mắc về sản phẩm dịch vụ của chúng tôi
                    </h3>
                    <div id="success">
                        @if(Session::has("success"))
                            <div class="row" style="margin-top: 10px">
                                <div class="col-lg-12">
                                    <div class="alert alert-success">{{Session::get("success")}}</div>
                                </div>
                            </div>
                        @endif
                        @if(Session::has("error"))
                            <div class="row" style="margin-top: 10px">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger">{{Session::get("error")}}</div>
                                </div>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="row" style="margin: 10px">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger">
                                        <ul style="margin-left: 10px">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <form id="formValidate" class="b-form form-horizontal" method="post"
                          action="{{URL::action('FontEnd_Controller\ContactController@postContact')}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="b-form-description">
                                <p>
                                    Gửi tin nhắn phản hồi về dịch vụ
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="form-first-name" name="customer_name"
                                           placeholder="Họ và tên *">
                                </div>
                                <div class="col-sm-12">
                                    <input type="email" class="form-control" id="form-email"
                                           placeholder="Email *" name="email">
                                </div>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control child" id="form-subject"
                                           placeholder="Số điện thoại" name="phone">
                                </div>
                                <div class="col-sm-12">
                                    <textarea id="form-comment" class="form-control" rows="6"
                                              placeholder="Nội dung" name="message"></textarea>
                                </div>
                                <input type="hidden" class="form-control" value="0" name="status">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-default">Gửi tin</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInRight">
                    <h3 class="heading-line">
                        địa chỉ map
                    </h3>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="b-map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3724.8766673817713!2d105.79537321534536!3d20.997580394216932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zMjkgS2h14bqldCBEdXkgVGnhur9uLCBUaGFuaCBYdcOibiwgSMOgIE7hu5lp!5e0!3m2!1svi!2s!4v1534414146892"
                                        width="600" height="450" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>
                            <div class="b-contact-info">
                                <ul class="list-unstyled">
                                    <li>
                                        <span class="info-title"><i class="fa fa-map-pin"></i>Địa chỉ</span>
                                        <span class="info-text">T6 Times City, 458 phố Minh Khai, Vĩnh Tuy, Hai Bà Trưng, Hà Nội.</span>
                                    </li>
                                    <li>
                                        <span class="info-title"><i class="fa fa-phone"></i>Hỗ trợ trực tuyến</span>
                                        <span class="info-text">0964.791.791 - 0989.198.548</span>
                                    </li>
                                    <li>
                                        <span class="info-title"><i class="fa fa-envelope-o"></i>email</span>
                                        <span class="info-text">mocanhxnk@gmail.com</span>
                                    </li>
                                    {{--<li>--}}
                                    {{--<span class="info-title"><i class="fa fa-skype"></i>Facebook Chat</span>--}}
                                    {{--<span class="info-text">facebook.com</span>--}}
                                    {{--</li>--}}
                                </ul>
                            </div>
                            <div class="b-socials clearfix">
                                <ul class="list-unstyled">
                                    {{--<li><a href="https://twitter.com/"><i class="fa fa-twitter fa-fw"></i></a></li>--}}
                                    <li><a href="{{$facebook}}"><i class="fa fa-facebook fa-fw"></i></a>
                                    </li>
                                    {{--<li><a href="https://www.linkedin.com/"><i class="fa fa-linkedin fa-fw"></i></a>--}}
                                    {{--</li>--}}
                                    <li><a href="{{$google_plus}}"><i class="fa fa-google-plus fa-fw"></i></a>
                                    </li>
                                    {{--<li><a href="http://pinterest.com/"><i class="fa fa-pinterest-p fa-fw"></i></a></li>--}}
                                    <li><a href="{{$youtube}}"><i class="fa fa-youtube-play fa-fw"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('themes/js/jquery-1.11.2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            //called when key is pressed in textbox
            $("#form-subject").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });
    </script>
@endsection
