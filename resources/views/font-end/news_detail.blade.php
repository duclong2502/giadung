@extends('font-end.layout.index')
@section('title')
    {{ $title }}
    @php
        $link = Request::fullUrl();
    @endphp
@stop
@section('seo')
    @include('font-end.layout.seo', ['title_seo' => $new_detail->title_seo,
    'link_seo' => $new_detail->link_seo,
    'primary_key_seo' => $new_detail->primary_key_seo,
    'sub_key_seo' => $new_detail->sub_key_seo,
    'description_seo' => $new_detail->description_seo])
@endsection
@section('font-end-content')
    <section class="section-blog-post">
        <div class="b-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <h3 class="page-title pull-left">{!! $new_detail->title !!}</h3>
                        <div class="b-breadcrumbs pull-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                    <div class="post-image wow fadeInUp">
                        {{--<div class="post-img-holder">--}}
                            {{--<img src="{{asset($new_detail->image)}}" width="820" height="400px" class="img-responsive"--}}
                                 {{--alt="/">--}}
                        {{--</div>--}}
                        {{--<div class="post-data">--}}
                            {{--<span>{!! date("<b>d/m/20y </b>",strtotime($new_detail->created_at)) !!}</span>--}}
                        {{--</div>--}}
                    </div>
                    <div class="b-posts-holder wow fadeInUp">
                        <div class="b-post-main clearfix">
                            <div class="post-caption">
                                <div class="caption">
                                    <h5 class="heading-line">{!! $new_detail->title !!}</h5>
                                    <div class="post-author">
                                        <span><i class="fa fa-user"></i>Admin</span>
                                        <span><i class="fa fa-comment-o"></i>12 Comments</span>
                                        <span><i class="fa fa-clock-o"></i>{!! date("<b>d/m/20y </b>",strtotime($new_detail->created_at)) !!}</span>
                                    </div>
                                    <hr>
                                    <div class="post-description">
                                        {!! $new_detail->content !!}
                                    </div>
                                    <div class="b-socials full-socials clearfix">
                                        <ul class="list-unstyled">
                                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook fa-fw"></i>Share</a>
                                            </li>
                                            <li><a href="https://plus.google.com/"><i
                                                            class="fa fa-google-plus fa-fw"></i>Google+</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-post-comments wow fadeInUp">
                        <h3 class="heading-line">Comments</h3>
                        <div class="fb-comments" data-href="{{$link}}" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                    <div class="lb-content lb-content-accordion">
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">Sản phẩm nổi bật</h3>
                            @foreach($products as $product)
                                <div class="l-box-content">
                                    <div class="b-latest-rev">
                                        <div class="latest-rev-img pull-left">
                                            <img src="{{asset($product->image)}}" class="img-responsive"
                                                 alt="/" style="width: 85px;height: 85px !important;">
                                        </div>
                                        <div class="latest-rev-caption">
                                            <a class="rev-caption-title" href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">{{$product->name}}</a>
                                            <p>{{$product->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="l-box-mod wow fadeInRight">
                            <h3 class="heading-line">tin tức liên quan</h3>
                            @foreach($population_news as $pn)
                                <div class="b-popular-post">
                                    <a class="popular-caption-title" href="{{route('newsDetail',$pn->slug)}}">{{$pn->title}}</a>
                                    <div class="popular-date">{!! date("<b>d/m/20y </b>",strtotime($pn->created_at)) !!}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--COMMENT FB--}}
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=1486572324821329&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
@endsection