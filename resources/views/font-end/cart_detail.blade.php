@extends('font-end.layout.index')
@section('title')
    {{ $title }}
@stop
@section('seo')
    @include('font-end.layout.seo')
@endsection
@section('font-end-content')
    <section class="section-shopping-cart">
        {{--        <form method="post" action="{{URL::action('FontEnd_Controller\CartController@successOrder',$cookie)}}"--}}
        {{--<form method="post" action="{{URL::action('FontEnd_Controller\CartController@successOrder',$cookie)}}"--}}
        {{--id="formValidate" class="form-horizontal"--}}
        {{--enctype="multipart/form-data">--}}
        {{--{{ csrf_field() }}--}}
        <div class="container">
            <div class="row" style="background: #f7f7f7">
                <div class="col-sm-12 cart-table wow fadeInUp">
                    <div class="b-table b-cart-table table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>
                                    <span>Sản phẩm</span>
                                </td>
                                <td>
                                    <span>Đơn giá</span>
                                </td>
                                <td>
                                    <span>Số lượng</span>
                                </td>
                                <td>
                                    <span>Thành tiền</span>
                                </td>
                                <td>
                                    <span>Xóa</span>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @php $total = 0; @endphp
                            @if(count($item) == 0)
                                <tr>
                                    <td colspan="8" style="padding-bottom: 10px">Chưa có sản phẩm nào</td>
                                </tr>
                            @else
                                @foreach($item as $value)
                                    <tr id="cart-item-{{$value->id}}">
                                        <td>
                                            <div class="image">
                                                <img src="{{asset($value->image)}}"
                                                     class="img-responsive img-thumbnail center-block"
                                                     alt="{{$value->name}}"
                                                     style="width: 80px; height: 55px !important;">
                                            </div>
                                            <div class="caption">
                                                <a class="product-name" href="#">{{$value->name}}</a>
                                            </div>
                                        </td>
                                        <td>
                                            @if($value->price != 0)
                                                <span class="product-price">{{number_format($value->price)}}</span>
                                            @else
                                                <span class="product-price">Giá liên hệ</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="input-group btn-block qty-block" data-trigger="spinner">
                                                <a class="spinner-btn-mod" href="javascript:;"
                                                   onclick="return minus({{$value->id}});"
                                                   data-spin="down">-</a>
                                                <input class="form-control" type="text" value="{{$value->total}}"
                                                       data-rule="quantity" name="quantity"
                                                       id="quality-{{$value->id}}"
                                                       data-id="{{$value->id}}"
                                                       style="width: 55px" readonly>
                                                <a class="spinner-btn-mod" onclick="return plus({{$value->id}});"
                                                   href="javascript:;"
                                                   data-spin="up">+</a>
                                            </div>
                                        </td>
                                        <td>
                                            @if($value->price != 0)
                                                <span class="product-price total-price">{{number_format($value->price * $value->total)}}</span>
                                                @php $total = $total + ($value->price * $value->total); @endphp
                                            @else
                                                <span class="product-price total-price">Giá liên hệ</span>
                                            @endif
                                        </td>
                                        <td class="text-left">
                                            <a onclick="return remove({{$value->id}});" href="javascript:void(0)"
                                               class="btn btn-remove"><i class="fa fa-trash fa-lg"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>


                @if(count($item) > 0)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="b-total-table clearfix">
                            <table class="table table-condensed">
                                <tbody>
                                <tr class="total">
                                    <td style="padding-top: 50px;">Tổng đơn giá:</td>
                                    <td style="padding-top: 50px;">{{number_format($total)}}đ</td>
                                    <td width="27%">
                                        <a href="{{URL::action('FontEnd_Controller\CartController@thanhtoan')}}"
                                           class="btn btn-primary-color2 btn-sm">Thanh toán
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        {{--</form>--}}
    </section>

@endsection
@section('script')
    <script src="{{asset('themes/js/jquery-1.11.2.min.js')}}"></script>

    <script>
        function remove(id) {
            if (confirm("Bạn có muốn xóa sản phẩm!")) {
                $.ajax({
                    url: "{{URL::action('FontEnd_Controller\CartController@removeProduct')}}",
                    type: "post",
                    dateType: "json",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,

                    },
                    success: function (result) {
                        location.reload();
                        alert(result.mess);
                    }
                });
            } else {
                return false;
            }
        }

        function plus(id) {
            var quality = (+$("#quality-" + id).val() + 1);
            console.log(quality);
            $.ajax({
                url: "{{URL::action('FontEnd_Controller\CartController@plus')}}",
                type: "post",
                dateType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                    quality: quality,

                },
                success: function (result) {
                    location.reload();
                    console.log(result.mess);
                }
            });
        }

        function minus(id) {
            var quality = (+$("#quality-" + id).val() - 1);
            console.log(quality);
            $.ajax({
                url: "{{URL::action('FontEnd_Controller\CartController@minus')}}",
                type: "post",
                dateType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                    quality: quality,

                },
                success: function (result) {
                    location.reload();
                    console.log(result.mess);
                }
            });
        }
    </script>
@endsection