<header>
    <div class="b-top-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="b-top-options b-top-info pull-left">
                        <div class="nav">
                            <ul class="list-inline">
                                <li>
                                    <span><i class="fa fa-phone"></i>0964.791.791 - 0989.198.548</span>
                                </li>
                                <li>
                                    <span><i class="fa fa-envelope-o"></i>mocanhxnk@gmail.com</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="b-top-options b-top-info pull-right">
                        <div class="nav">
                            <ul class="list-inline">
                                <li>
                                    <span><a href="{{$facebook}}"><i class="fa fa-facebook"></i></a></span>
                                </li>
                                <li>
                                    <span><a href="{{$youtube}}"><i class="fa fa-youtube-play"></i></a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-header-main style-2">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                    <div class="b-logo">
                        <a href="/">
                            {{--<span>GiadungEUS</span>--}}
                            <img src="/themes/images/logo.png">
                        </a>
                    </div>
                </div>
                <div id="toggle-nav" class="col-xs-12 col-sm-10 col-md-8 col-lg-9 menu-wrapper clearfix">
                    <div class="toggle-nav-btn">
                        <button class="btn btn-menu"><i class="fa fa-bars fa-lg"></i></button>
                    </div>
                    <div class="b-header-menu pull-right">
                        <ul class="list-inline">
                            <li>
                                <a class="heading-line" href="/">Trang chủ</a>

                            </li>
                            @foreach($categories as $category)
                                <li>
                                    {{--<a class="heading-line" href="{{$category->getLinkByID['link']}}">{{ $category->name}}</a>--}}
                                    @if($category->type == 2)
                                        <a class="heading-line"
                                           href="{{URL::action('FontEnd_Controller\ProductController@getProductCategory',$category->getLinkByID['link'])}}">{{ $category->name}}</a>
                                    @elseif($category->type == 1)
                                        <a class="heading-line"
                                           href="{{$category->link}}">{{ $category->name}}</a>
                                    @elseif($category->type == 3)
                                        <a class="heading-line"
                                           href="{{route('newsDetail',$category->getSlugByID['slug'])}}">{{ $category->name}}</a>
                                    @endif

                                    @if(count($category->childs))
                                        @include('font-end.layout.manageChild',['childs' => $category->childs])
                                    @endif
                                </li>
                            @endforeach
                            <li class="search">
                                <a id="search-open" class="iconSearch" href="#"><i class="fa fa-search"></i></a>
                                <div id="search">
                                    <form method="get">
                                        <div class="form-group">
                                            <input id="searchQuery" type="search" name="home-search"
                                                   placeholder="Search...">
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="cart-wrapper" class="col-xs-6 col-sm-12 col-md-2 col-lg-1">
                    <div class="b-cart pull-right">
                        {{--<button id="cart" class="btn btn-default-color1 btn-sm">--}}
{{--                            <span class="price"><i class="fa fa-shopping-bag"></i> {{number_format($sub_money)}}</span>--}}
                            <span><i class="fa fa-shopping-cart fa-2x" style="cursor: pointer;"></i></span>
                            <span class="counter-wrapper"><span class="counter">{!! count($carts) !!}</span></span>
                        {{--</button>--}}
                        <div class="cart-products">
                            <div class="c-holder">
                                <h3 class="title">
                                    sản phẩm đã đặt
                                </h3>
                                <ul class="products-list list-unstyled">
                                    @foreach ($carts as $cart)
                                        <li>
                                            <div class="row b-cart-table">
                                                <a href="" class="image">
                                                    <img src="{{asset($cart->image)}}" alt="/"
                                                         style="width: 40px;height: 40px">
                                                </a>
                                                <div class="caption">
                                                    <a class="product-name" href=""
                                                       title="{{$cart->name}}">{{$cart->name}}</a>
                                                    <span class="product-price">{{$cart->total}}
                                                        x {{number_format($cart->price)}}</span>
                                                </div>
                                                {{--<div class="remove">--}}
                                                <a onclick="return remove({{$cart->id}});" href="javascript:void(0)"
                                                   class="btn btn-remove"><i class="fa fa-trash fa-lg"></i>
                                                </a>
                                                {{--</div>--}}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="products-buttons text-center">
                                    <a href="{{URL::action('FontEnd_Controller\CartController@getCart')}}"
                                       class="btn btn-primary-color2 btn-sm">Tới giỏ hàng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
    function remove(id) {
        if (confirm("Bạn có muốn xóa!")) {
            $.ajax({
                url: "{{URL::action('FontEnd_Controller\CartController@removeProduct')}}",
                type: "post",
                dateType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,

                },
                success: function (result) {
                    location.reload();
                    alert(result.mess);
                }
            });
        } else {
            return false;
        }
    }
</script>
