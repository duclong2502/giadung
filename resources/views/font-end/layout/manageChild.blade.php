<div class="b-all-homes">
    <ul class="list-unstyled">
        @foreach($childs as $child)
            <li>

                {{--<a class="heading-line"--}}
                   {{--href="{{URL::action('FontEnd_Controller\ProductController@getProductCategory',$child->getLinkByID['link'])}}">{{ $child->name }}</a>--}}

                @if($child->type == 2)
                    <a class="heading-line"
                       href="{{URL::action('FontEnd_Controller\ProductController@getProductCategory',$child->getLinkByID['link'])}}">{{ $child->name}}</a>
                @elseif($child->type == 1)
                    <a class="heading-line"
                       href="{{$child->link}}">{{ $child->name}}</a>
                @elseif($child->type == 3)
                    <a class="heading-line"
                       href="{{route('newsDetail',$child->getSlugByID['slug'])}}">{{ $child->name}}</a>
                @endif

                @if(count($child->childs))
                    @include('manageChild',['childs' => $child->childs])
                @endif
            </li>
        @endforeach
    </ul>
</div>
