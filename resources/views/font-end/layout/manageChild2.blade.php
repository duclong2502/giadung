<ul class="sub-menu">
    @foreach($childs as $child)
        <li>
            <a href="{{URL::action('FontEnd_Controller\ProductController@getProductCategory',$child->link)}}
                    ">{{ $child->name }}</a>

            @if(count($child->childs))
                @include('manageChild',['childs' => $child->childs])
            @endif
        </li>
    @endforeach
</ul>
