{{--<meta name="title" content="{{$title_seo ?: MAGHelper::getTitleSeoConfig($key = "title_seo")}}">--}}
<meta name="title" content="{{ $title_seo or MAGHelper::getTitleSeoConfig($key = "title_seo")}}">
<meta name="link" content="{{$link_seo or MAGHelper::getTitleSeoConfig($key = "link_seo")}}">
<meta name="primary_key_seo" content="{{$primary_key_seo or MAGHelper::getTitleSeoConfig($key = "primary_key_seo")}}">
<meta name="sub_key_seo" content="{{$sub_key_seo or MAGHelper::getTitleSeoConfig($key = "sub_key_seo")}}">
<meta name="description_seo" content="{{$description_seo or MAGHelper::getTitleSeoConfig($key = "description_seo")}}">
