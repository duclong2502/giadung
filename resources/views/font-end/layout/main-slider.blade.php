<section class="main-slider">
    <div class="slider-pro full-width-slider" id="main-slider" data-width="100%" data-height="570" data-fade="true"
         data-buttons="true" data-wait-for-layers="true" data-thumbnail-pointer="false" data-touch-swipe="false"
         data-autoplay="true" data-auto-scale-layers="true" data-visible-size="100%" data-force-size="fullWidth"
         data-autoplay-delay="5000">
        <div class="sp-slides">
            <div class="sp-slide">
                <img class="sp-image" src="/themes/media/slides/slide_1.jpg"
                     data-src="/themes/media/slides/slide_1.jpg"
                     data-retina="/themes/media/slides/slide_1.jpg" alt="slide1"/>
                <div class="sp-layer slider-discount"
                     data-horizontal="18.6%" data-vertical="22.5%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
                    <span>25% OFF</span>
                </div>
                <div class="sp-layer slide-tex-1"
                     data-horizontal="18.6%" data-vertical="33.5%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                    <span>smartphones<br><span class="border-line">on sale</span></span>
                </div>
                <div class="sp-layer slider-text-2"
                     data-horizontal="18.7%" data-vertical="54.4%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="800">
                    more faster. more powerful<br>
                    & even more bigger
                </div>
                <div class="sp-layer"
                     data-horizontal="18.9%" data-vertical="68.7%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="1000">
                    <a class="btn btn-primary-color1 btn-sm" href="#">SHOP NOW</a>
                </div>
            </div>

            <div class="sp-slide">
                <img class="sp-image" src="/themes/media/slides/slide_2.jpg"
                     data-src="/themes/media/slides/slide_2.jpg"
                     data-retina="/themes/media/slides/slide_2.jpg" alt="slide2"/>
                <div class="sp-layer ls-text"
                     data-horizontal="14.8%" data-vertical="27.5%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
                    <span>next is now</span>
                </div>
                <div class="sp-layer"
                     data-horizontal="15%" data-vertical="35.5%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                    <img src="/themes/media/slides/samsung.png" alt="samsung">

                </div>
                <div class="sp-layer"
                     data-horizontal="15%" data-vertical="41.7%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="800">
                    <img src="/themes/media/slides/galaxy.png" alt="samsung galaxy">
                </div>
                <div class="sp-layer text-highlight"
                     data-horizontal="15%" data-vertical="52%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="800">
                    <span class="highlight-primary">powerful. stunning. now even bigger</span>
                </div>
                <div class="sp-layer"
                     data-horizontal="15%" data-vertical="63.4%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="1000">
                    <button class="btn btn-default-color1 btn-sm" type="button">SHOP NOW</button>
                </div>
            </div>

            <div class="sp-slide">
                <img class="sp-image" src="/themes/media/slides/slide_3.jpg"
                     data-src="/themes/media/slides/slide_3.jpg"
                     data-retina="/themes/media/slides/slide_3.jpg" alt="slide3"/>
                <div class="sp-layer s3-text"
                     data-horizontal="18.7%" data-vertical="22.3%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
                    <span>microsoft</span>
                </div>
                <div class="sp-layer s3-name"
                     data-horizontal="18.4%" data-vertical="25%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                    <span>lumia</span>
                </div>
                <div class="sp-layer s3-discount"
                     data-horizontal="18.7%" data-vertical="36.4%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="800">
                    <span>smartphones<br>10% OFF</span>
                </div>
                <div class="sp-layer s3-text2"
                     data-horizontal="18.7%" data-vertical="53%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="800">
                    <span>get discount on latest<br>& the best tech</span>
                </div>
                <div class="sp-layer sl3"
                     data-horizontal="18.8%" data-vertical="69.4%"
                     data-show-transition="left" data-hide-transition="up" data-show-delay="1000">
                    <button class="btn btn-default-color1 btn-sm" type="button">SHOP NOW</button>
                </div>
            </div>

        </div>
    </div>
</section>