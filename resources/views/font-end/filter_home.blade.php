<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 wow fadeInUp">
    @foreach($products as $product)
        <div class="b-item-card">
            <div class="image">
                <a href="{{asset($product->image)}}">
                    <img src="{{asset($product->image)}}"
                         class="img-responsive center-block" alt="{{$product->name}}">
                </a>
                <div class="image-add-mod">
                    <div class="add-description">
                        <div>
                            {{$product->description}}
                            <br>
                            <a href="{{asset($product->image)}}"
                               data-gal="prettyPhoto" title="{{$product->name}}"
                               class="btn btn-lightbox btn-default-color1 btn-sm">
                                <i class="fa fa-search-plus fa-lg"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-info">
                <div class="caption">
                    <p class="name-item">
                        <a class="product-name" href="{{route('productDetail',['cat'=>MAGHelper::getSlugCategory($product->category_id),'slug'=>$product->slug])}}">{{$product->name}}</a>
                    </p>
                    <span class="product-price">{{number_format($product->price_sale)}}</span>
                </div>
                <div class="add-buttons">
                    <button type="button" class="btn btn-add btn-add-cart"><i
                                class="fa fa-shopping-cart"></i></button>
                </div>
            </div>
        </div>
    @endforeach
</div>