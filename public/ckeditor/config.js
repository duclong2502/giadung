/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'vi';
    // config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = 'http://giadungducmag.com/ckfinder/ckfinder.html',
    config.filebrowserImageBrowseUrl = 'http://giadungducmag.com/ckfinder/ckfinder.html?type=Images',
    config.filebrowserFlashBrowseUrl = 'http://giadungducmag.com/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = 'http://giadungducmag.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = 'http://giadungducmag.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = 'http://giadungducmag.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    config.extraPlugins = 'lineheight,youtube';
    // config.filebrowserBrowseUrl = 'http://antoanasean.com.vn/ckfinder/ckfinder.html',
    //     config.filebrowserImageBrowseUrl = 'http://antoanasean.com.vn/ckfinder/ckfinder.html?type=Images',
    //     config.filebrowserFlashBrowseUrl = 'http://antoanasean.com.vn/ckfinder/ckfinder.html?type=Flash';
    // config.filebrowserUploadUrl = 'http://antoanasean.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    // config.filebrowserImageUploadUrl = 'http://antoanasean.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    // config.filebrowserFlashUploadUrl = 'http://antoanasean.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
